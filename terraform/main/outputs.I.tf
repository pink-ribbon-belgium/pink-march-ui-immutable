output "I-ui_bucket" {
  value = {
    id                          = aws_s3_bucket.ui.id
    name                        = aws_s3_bucket.ui.bucket
    arn                         = aws_s3_bucket.ui.arn
    bucket_regional_domain_name = aws_s3_bucket.ui.bucket_regional_domain_name
  }
}

output "I-ui_bucket-cloudfront_origin_access_identity" {
  value = {
    path = aws_cloudfront_origin_access_identity.ui.cloudfront_access_identity_path
    arn  = aws_cloudfront_origin_access_identity.ui.iam_arn
  }
}

output "I-ui_ci" {
  value = {
    name              = aws_iam_user.ci.name
    path              = aws_iam_user.ci.path
    arn               = aws_iam_user.ci.arn
    aws_access_key_id = aws_iam_access_key.ci.id
  }
}

output "I-ui_ci-aws_secret_access_key" {
  value     = aws_iam_access_key.ci.secret
  sensitive = true
}

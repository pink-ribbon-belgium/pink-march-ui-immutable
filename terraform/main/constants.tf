locals {
  region  = "eu-west-1"
  profile = "pink-ribbon-belgium-dev"
  repo    = "pink-march-ui-immutable"
  tags = {
    name = "Pink March UI builds"
    env  = "production"
    repo = "${local.repo}//terraform/main"
  }

}

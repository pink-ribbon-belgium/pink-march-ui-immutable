# pink-march-ui Terraform Infrastructure Definition - main

The infrastructure necessary for the Pink March UI.

CI deploys every successful build in an S3 bucket (`pink-march-ui-bookmarkable` deploys in the same bucket).

Furthermore, roles and privileges are defined here for integration testing.

## Getting started

The infrastructure is defined using [Terraform].
See [Getting started with a Terraform configuration].

You need to have [AWS credentials] set up in `~/.aws/credentials` for profile `pink-ribbon-belgium-dev`,
which should be able to assume the role defined in [`infrastructure_role/`](infrastructure_role).

[terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[getting started with a terraform configuration]: https://peopleware.atlassian.net/wiki/x/p4zhC
[aws credentials]: https://peopleware.atlassian.net/wiki/x/RoAWBg

# CI can write versions to s3 bucket

resource "aws_iam_user" "ci" {
  name = data.terraform_remote_state.infrastructure_role.outputs.I-ui_ci["name"]
  path = data.terraform_remote_state.infrastructure_role.outputs.I-ui_ci["path"]
  tags = local.tags
}

resource "aws_iam_access_key" "ci" {
  user = aws_iam_user.ci.name
}

data "aws_iam_policy_document" "ci" {
  statement {
    actions = [
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.ui.arn
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.ui.arn}/ui/immutable/*"
    ]
  }
}

resource "aws_iam_policy" "ci" {
  name        = data.terraform_remote_state.infrastructure_role.outputs.I-ui_ci["name"]
  path        = data.terraform_remote_state.infrastructure_role.outputs.I-ui_ci["path"]
  policy      = data.aws_iam_policy_document.ci.json
  description = "Allow ${local.repo} CI to upload"
}

resource "aws_iam_user_policy_attachment" "ci" {
  user       = aws_iam_user.ci.name
  policy_arn = aws_iam_policy.ci.arn
}

resource "aws_s3_bucket" "ui" {
  bucket = data.terraform_remote_state.infrastructure_role.outputs.I-ui_bucket["name"]
  region = local.region

  tags = local.tags

  versioning {
    enabled = false
  }

  lifecycle {
    prevent_destroy = true
  }
}

# only this identity can access the bucket from cloudfront, and the devops group
resource "aws_cloudfront_origin_access_identity" "ui" {}

data "aws_iam_policy_document" "bucket_policy-ui" {
  # cloudfront: extra access on top of IAM access
  statement {
    actions = ["s3:GetObject"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.ui.iam_arn]
    }

    resources = ["${aws_s3_bucket.ui.arn}/*"]
  }
}

resource "aws_s3_bucket_policy" "ui" {
  bucket = aws_s3_bucket.ui.id
  policy = data.aws_iam_policy_document.bucket_policy-ui.json
}

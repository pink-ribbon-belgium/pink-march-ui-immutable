data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
    condition {
      test = "StringEquals"
      values = [
        "devsecops"
      ]
      variable = "aws:PrincipalTag/canAssumeRole"
    }
  }
}

resource "aws_iam_role" "pink-march-ui-immutable-infrastructure" {
  name               = local.infrastructure-prefix
  path               = "/devsecops/"
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
  tags = {
    repo = local.repo-immutable
  }
}

# Attach policy to role
resource "aws_iam_role_policy_attachment" "pink-march-ui-immutable-infrastructure-role-policy" {
  policy_arn = aws_iam_policy.role-policy.arn
  role       = aws_iam_role.pink-march-ui-immutable-infrastructure.name
}

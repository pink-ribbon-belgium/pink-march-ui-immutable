locals {
  region                = "eu-west-1"
  profile               = "pink-ribbon-belgium-dev"
  repo-basename         = "pink-march-ui"
  repo-immutable        = "${local.repo-basename}-immutable"
  infrastructure-prefix = "${local.repo-immutable}-infrastructure"
  bucket_postfix        = "pink-ribbon-belgium.org"
  ui_bucket-name        = "${local.repo-basename}.${local.bucket_postfix}"
  ui_bucket-arn         = "arn:aws:s3:::${local.ui_bucket-name}"
  ui_ci-name            = local.repo-immutable
  ui_ci-path            = "/ci/"
  ui_ci-arn-user        = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user${local.ui_ci-path}${local.ui_ci-name}"
  ui_ci-arn-policy      = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.ui_ci-path}${local.ui_ci-name}"
}

data "aws_caller_identity" "current" {}

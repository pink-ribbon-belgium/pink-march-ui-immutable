/*
 *     Copyright (C) 2020 PeopleWare NV
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const cmd = require('node-cmd');

function generateNgswConfig() {
  let buildNumber = addLeadingZeros(process.env.BITBUCKET_BUILD_NUMBER);
  cmd.run('ngsw-config dist ngsw-config.json /ui/immutable/' + buildNumber);
}

function addLeadingZeros(number) {
  if (number === undefined) {
    number = '00000';
  }

  while (number.length < 5) {
    number = '0' + number;
  }
  return number;
}

generateNgswConfig();

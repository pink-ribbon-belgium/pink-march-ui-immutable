/*
 *     Copyright (C) 2020 PeopleWare NV
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';
/**
 * The problem: ngsw.json isn't prefixed correctly and there is no angular way to configure these files.
 * Solution: This script will override the assetGroup content of ngsw.json with our 'servePath' (found in angular.json)  + 'filename'.
 * We use this script every time we build our angular project.
 */
const path = require('path');
const fs = require('fs');
const angularJsonPath = path.join(__dirname, '..', '..', '..', '..', 'angular.json');
const angularJson = require(angularJsonPath);
const ngswPath = path.join(__dirname, '..', '..', '..', '..', 'dist', 'ngsw.json');
const ngsw = require(ngswPath);

const applyDynamicBase = () => {
  const indexHtml = '/index.html';

  if (ngsw.index === indexHtml) {
    ngsw.index = '.' + indexHtml;
  }
  for (const assetGroup of ngsw.assetGroups) {
    const indexHtmlIndex = assetGroup.urls.findIndex(url => url.includes(indexHtml));
    if (indexHtmlIndex > -1) {
      assetGroup.urls[indexHtmlIndex] = '.' + indexHtml;
    }
    const patternIndex = assetGroup.patterns.findIndex(pattern => pattern.includes('index\\.html'));
    assetGroup.patterns[patternIndex] = '\\/(?:.+\\/)?index\\.html';
  }

  fs.writeFile(ngswPath, JSON.stringify(ngsw, null, 4), err => {
    if (err) {
      console.error('File could not be written!', err);
      throw err;
    }
  });
};

const buildNumber = process.env.BITBUCKET_BUILD_NUMBER;
const dynBaseBase = '/' + angularJson.projects['pink-march-ui-immutable'].architect.serve.options.servePath;
const dynBase = buildNumber !== undefined ? dynBaseBase.replace('00000', buildNumber.padStart(5, '0')) : dynBaseBase;

applyDynamicBase(dynBase);

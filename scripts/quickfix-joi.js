#!/usr/bin/env node

/**
 * `joi-browser.min.js` is build with UMD, which in this case refers to `window`.
 * That doesn't work in web workers (because there is no `window` global).
 *
 * However, the UMD path where the `window` reference is not used in our project.
 *
 * This quick fix replaces the reference to `window` in `joi-browser.min.js` with `undefined`.
 * It is to be executed in `postinstall`.
 *
 * See [#2251@hapi/joi](https://github.com/hapijs/joi/issues/2251).
 */
const fs = require('fs').promises;

const joiBrowserPath = './node_modules/@hapi/joi/dist/joi-browser.min.js';
const options = { encoding: 'utf8' };

async function doIt() {
  const joiSrc = await fs.readFile(joiBrowserPath, options);
  // Replace the window object, which doesn't exist in a worker, with undefined
  // This parameter isn't used in our project anyway.
  const result = joiSrc.replace('window', 'undefined');
  await fs.writeFile(joiBrowserPath, result, options);
  console.log(`Doctored \`${joiBrowserPath}\` successfully.`);
}

console.log(
  `Doctoring \`${joiBrowserPath}\`.
It uses \`window\` in the UMD preliminary, but we don't use that. We replace it with \`undefined\`
…`
);
doIt();

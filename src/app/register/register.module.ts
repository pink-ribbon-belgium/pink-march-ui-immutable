//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AccountModule } from '../account/account.module';
import { CompanyModule } from '../company/company.module';
import { SharedModule } from '../shared/shared.module';
import { CompanyProfileExistsComponent } from './company-profile-exists/company-profile-exists.component';
import { CompanyProfileComponent } from './company-profile/company-profile.component';
import { PaymentPreparationComponent } from './payment-preparation/payment-preparation.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { PersonalProfileExistsComponent } from './personal-profile-exists/personal-profile-exists.component';
import { PersonalProfileComponent } from './personal-profile/personal-profile.component';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { RegistrationSuccessComponent } from './registration-success/registration-success.component';

@NgModule({
  declarations: [
    RegisterComponent,
    PersonalProfileComponent,
    PersonalProfileExistsComponent,
    PaymentPreparationComponent,
    PaymentSuccessComponent,
    RegistrationSuccessComponent,
    CompanyProfileComponent,
    CompanyProfileExistsComponent
  ],
  imports: [CommonModule, SharedModule, RegisterRoutingModule, CompanyModule, AccountModule]
})
export class RegisterModule {}

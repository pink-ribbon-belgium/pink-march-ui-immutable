//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { of } from 'rxjs';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { sharedModules } from '../../shared/constant/shared-modules';
import { RegistrationSuccessComponent } from './registration-success.component';

interface Fixture {
  component: RegistrationSuccessComponent;
  fixture: ComponentFixture<RegistrationSuccessComponent>;
}

describe('RegistrationSuccessComponent', () => {
  beforeEach(() => {
    void TestBed.configureTestingModule({
      declarations: [RegistrationSuccessComponent],
      imports: [RouterTestingModule.withRoutes([]), BrowserAnimationsModule, ...appModules, ...sharedModules],
      providers: [
        {
          provide: AuthService,
          useValue: {
            userProfile$: of(null)
          }
        }
      ]
    });
  });

  beforeEach(function(this: Fixture): void {
    this.fixture = TestBed.createComponent(RegistrationSuccessComponent);
    this.component = this.fixture.componentInstance;
  });

  it('should create', function(this: Fixture): void {
    expect(this.component).toBeTruthy();
  });
});

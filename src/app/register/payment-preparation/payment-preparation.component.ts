//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

// tslint:disable:template-cyclomatic-complexity //

import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, merge, Observable } from 'rxjs';
import { debounceTime, filter, finalize, startWith, switchMap, tap } from 'rxjs/operators';
import { PaymentService } from '../../api/payment/payment.service';
import { TeamService } from '../../api/team/team.service';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { generalConstant } from '../../shared/constant/general.constants';
import { localStorageConstant } from '../../shared/constant/local-storage.constants';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';
import { GroupProfile } from '../../types/group/group-profile';
import { CalculationParams } from '../../types/payment/calculation-params';
import { CalculationResult } from '../../types/payment/calculation-result';

@Component({
  selector: 'app-payment-preparation',
  templateUrl: './payment-preparation.component.html',
  styleUrls: ['./payment-preparation.component.scss']
})
export class PaymentPreparationComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  groupId: string;
  groupType: string;
  teamProfile: GroupProfile;
  calculationResult: CalculationResult;
  defaultTeamName: string;
  hasVoucherControl = new FormControl(false);
  isBusy = false;
  formGroup: FormGroup;
  isTeam = false;
  flowType: string;

  constructor(
    private readonly route: ActivatedRoute,
    public loc: Location,
    private readonly translateService: TranslateService,
    private readonly snackbarService: DefaultSnackbarService,
    private readonly teamService: TeamService,
    private readonly paymentService: PaymentService,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.loadLocalStorageData();
    this.createForm();
    this.checkPaymentError();
    localStorage.removeItem(localStorageConstant.paymentId);
    if (this.groupType === 'Team' && this.groupId) {
      this.loadTeamData(this.groupId);
    }
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      groupId: this.groupId,
      voucherCode: null,
      teamName: null,
      numberOfSlots: [1, [Validators.min(1), Validators.max(1000), Validators.required]],
      groupType: this.groupType === 'Company' ? generalConstant.companyGroupType : generalConstant.teamGroupType,
      locale: `${this.translateService.currentLang}-BE`,
      structureVersion: 1
    });
    this.subscriptions.add(
      merge(this.formGroup.get('numberOfSlots').valueChanges, this.formGroup.get('voucherCode').valueChanges)
        .pipe(
          startWith(null),
          tap(() => {
            if (this.groupType === 'Team') {
              if (this.formGroup.get('numberOfSlots').value > 1 && !this.isTeam) {
                this.isTeam = true;
                this.formGroup.get('teamName').setValidators(Validators.required);
              }
              if (this.formGroup.get('numberOfSlots').value === 1 && this.isTeam) {
                this.isTeam = false;
                this.formGroup.get('teamName').clearValidators();
              }
            }
            this.isBusy = true;
          }),
          debounceTime(500),
          filter(() => this.formGroup.get('numberOfSlots').valid),
          switchMap(() => this.recalculatePayment())
        )
        .subscribe()
    );
  }

  onBackClick(): void {
    this.loc.back();
  }

  recalculatePayment(): Observable<CalculationResult> {
    const calculationParams: CalculationParams = {
      numberOfParticipants: this.formGroup.get('numberOfSlots').value,
      groupId: this.formGroup.get('groupId').value,
      voucherCode: this.formGroup.get('voucherCode').value
        ? this.formGroup.get('voucherCode').value.toUpperCase()
        : undefined
    };

    return this.paymentService.calculatePayment(calculationParams).pipe(
      tap((calculationResult: CalculationResult) => {
        this.isBusy = false;
        this.calculationResult = calculationResult;
        this.checkVoucherValidity(this.calculationResult.voucherIsValid);
      }),
      finalize(() => (this.isBusy = false))
    );
  }

  onHaveVoucherChanged(event: MatCheckboxChange): void {
    const voucherControl = this.formGroup.get('voucherCode');
    if (!event.checked) {
      voucherControl.clearValidators();
      voucherControl.setValue(null);
    } else {
      voucherControl.setValidators(Validators.required);
    }
  }

  checkPaymentError(): void {
    this.route.params.subscribe(url => {
      if (url[generalConstant.paymentErrorPath]) {
        this.translateService.get('REGISTER.PAYMENT_PREPARATION.ERROR_PAYMENT').subscribe((translation: string) => {
          this.loc.replaceState(`/register/${this.groupType.toLowerCase()}/payment-preparation/${this.groupId}`);
          this.snackbarService.openSnackBar(translation);
        });
      }
    });
  }

  loadLocalStorageData(): void {
    this.flowType = localStorage.getItem(localStorageConstant.flow);
    this.groupType = localStorage.getItem(localStorageConstant.profileType);
    this.groupId = localStorage.getItem(localStorageConstant.groupId);
  }

  loadTeamData(teamId: string): void {
    this.isBusy = true;
    this.subscriptions.add(
      this.teamService.getTeamProfileById(teamId).subscribe(
        teamProf => {
          this.isBusy = false;
          this.defaultTeamName = teamProf.name;
          this.teamProfile = {
            structureVersion: teamProf.structureVersion,
            id: teamProf.id,
            name: null
          };
          this.formGroup.get('teamName').setValue(this.defaultTeamName);
        },
        () => {
          this.isBusy = false;
        }
      )
    );
  }
  onSubmitFormClick(): void {
    this.isBusy = true;
    const formData = this.formGroup.getRawValue();
    const paymentDetails = {
      ...formData,
      voucherCode: formData.voucherCode ? formData.voucherCode.toUpperCase() : undefined
    };
    if (this.teamProfile && this.flowType === 'registration') {
      this.teamProfile.name = paymentDetails.numberOfSlots === 1 ? this.defaultTeamName : paymentDetails.teamName;
      this.subscriptions.add(
        forkJoin(
          this.paymentService.initializePayment(paymentDetails),
          this.teamService.saveTeamProfile(this.teamProfile)
        ).subscribe(
          results => {
            localStorage.setItem(localStorageConstant.paymentId, results[0].paymentId);
            this.calculationResult.total > 0
              ? (location = results[0].postUrl)
              : this.router.navigateByUrl('/register/payment-success');
          },
          () => {
            this.isBusy = false;
          }
        )
      );
    } else {
      this.subscriptions.add(
        this.paymentService.initializePayment(paymentDetails).subscribe(
          resultData => {
            localStorage.setItem(localStorageConstant.paymentId, resultData.paymentId);
            this.calculationResult.total > 0
              ? (location = resultData.postUrl)
              : this.router.navigateByUrl('/register/payment-success');
          },
          () => {
            this.isBusy = false;
          }
        )
      );
    }
  }

  checkVoucherValidity(validVoucher: boolean): void {
    if (this.hasVoucherControl.value && !validVoucher) {
      this.translateService
        .get('REGISTER.PAYMENT_PREPARATION.ERROR_INVALID_VOUCHER')
        .subscribe((translation: string) => {
          this.snackbarService.openSnackBar(translation);
        });
    }
  }
}

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Location } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { Router } from '@angular/router';
import { of } from 'rxjs';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { sharedModules } from '../../shared/constant/shared-modules';
import { PaymentPreparationComponent } from './payment-preparation.component';

interface Fixture {
  component: PaymentPreparationComponent;
  fixture: ComponentFixture<PaymentPreparationComponent>;
  location: Location;
  router: Router;
}

describe('PaymentPreparationComponent', () => {
  beforeEach(() => {
    void TestBed.configureTestingModule({
      declarations: [PaymentPreparationComponent],
      imports: [RouterTestingModule.withRoutes([]), BrowserAnimationsModule, ...appModules, ...sharedModules],
      providers: [
        {
          provide: AuthService,
          useValue: {
            userProfile$: of(null)
          }
        }
      ]
    });
  });

  beforeEach(function(this: Fixture): void {
    this.fixture = TestBed.createComponent(PaymentPreparationComponent);
    this.component = this.fixture.componentInstance;
    this.location = TestBed.get(Location);
    this.router = TestBed.get(Router);
  });

  it('should create', function(this: Fixture): void {
    expect(this.component).toBeTruthy();
  });

  it('should navigate back when onBackClick is triggered', function(this: Fixture): void {
    this.fixture.detectChanges();
    spyOn(this.location, 'back');
    this.component.onBackClick();
    expect(this.location.back).toHaveBeenCalledTimes(1);
  });

  // it('should execute onSubmitClicked after submit form button click', async function(this: Fixture): Promise<void> {
  //   spyOn(this.component, 'onSubmitFormClick');
  //   const button = this.fixture.debugElement.nativeElement.querySelector('#submit');
  //   button.click();
  //   this.fixture.detectChanges();
  //   await this.fixture.whenStable();
  //   expect(this.component.onSubmitFormClick).toHaveBeenCalled();
  // });

  // it('should navigate when onSubmitClicked called', function(this: Fixture): void {
  //   spyOn(this.router, 'navigate');
  //   this.component.onSubmitFormClick();
  //   expect(this.router.navigate).toHaveBeenCalledWith(['/register/payment-success']);
  // });
});

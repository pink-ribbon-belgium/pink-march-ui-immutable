//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { sharedModules } from '../../shared/constant/shared-modules';
import { PaymentSuccessComponent } from './payment-success.component';

describe('PaymentSuccessComponent', () => {
  let component: PaymentSuccessComponent;
  let fixture: ComponentFixture<PaymentSuccessComponent>;

  interface Fixture {
    translateService: TranslateService;
    authService: AuthService;
  }

  beforeEach(() => {
    void TestBed.configureTestingModule({
      declarations: [PaymentSuccessComponent],
      imports: [RouterTestingModule.withRoutes([]), ...appModules, ...sharedModules],
      providers: [
        {
          provide: AuthService,
          useValue: {
            userProfile$: of(null)
          }
        }
      ]
    });
  });

  beforeEach(function(this: Fixture): void {
    fixture = TestBed.createComponent(PaymentSuccessComponent);
    component = fixture.componentInstance;
    this.translateService = TestBed.get<TranslateService>(TranslateService);
    this.authService = TestBed.get<AuthService>(AuthService);
    this.translateService.use('nl');
  });

  it('should create', function(this: Fixture): void {
    expect(component).toBeTruthy();
  });
});

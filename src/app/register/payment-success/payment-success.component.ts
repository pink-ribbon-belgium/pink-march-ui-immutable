//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

// tslint:disable:template-cyclomatic-complexity //

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { first, flatMap } from 'rxjs/operators';
import { AccountService } from '../../api/account/account.service';
import { PaymentService } from '../../api/payment/payment.service';
import { AuthService } from '../../auth/auth.service';
import * as navActions from '../../navigation/store/actions/nav.action';
import { NavState } from '../../navigation/store/state/nav.state';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../shared/constant/auth0.constants';
import { localStorageConstant } from '../../shared/constant/local-storage.constants';
import { PaymentSuccess } from '../../types/payment/payment-success';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.scss']
})
export class PaymentSuccessComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  paymentId: string;
  accountId: string;
  profileType: string;
  groupId: string;
  paymentSuccess: PaymentSuccess;
  flowTYpe: string;
  displayTextType: string;

  constructor(
    private readonly router: Router,
    private readonly paymentService: PaymentService,
    public auth: AuthService,
    public accountService: AccountService,
    private readonly store: Store<NavState>
  ) {
    super();
  }

  ngOnInit(): void {
    this.loadLocalStorageData();
    this.subscriptions.add(
      this.auth.userProfile$.subscribe((userData: any) => {
        if (userData) {
          this.accountId = userData[auth0Constant.accountId];
          this.verifyPayment();
        }
        // MUDO RACE CONDITION:
        //      This code is called on a "fresh" load of the app, returning from Ogone.
        //      - IF the auth service has user data before this triggers (we have a token on this fresh load),
        //        we call verify payment.
        //      - IF the auth service does NOT have user data when this triggers, we eat that condition and do
        //        nothing.
        //      - WHAT IT SHOULD BE: WAIT for the auth service to complete, then we WILL have user data and a
        //        token, and we MUST call verify */
      })
    );
  }

  loadLocalStorageData(): void {
    this.profileType = localStorage.getItem(localStorageConstant.profileType);
    this.groupId = localStorage.getItem(localStorageConstant.groupId);
    this.paymentId = localStorage.getItem(localStorageConstant.paymentId);
    this.flowTYpe = localStorage.getItem(localStorageConstant.flow);
  }

  getDisplayTextType(): string {
    if (this.profileType === 'Company') {
      return 'Company';
    }

    if (this.profileType === 'Team') {
      if (this.paymentSuccess.freeSlots > 0) {
        return 'Team';
      }

      return 'Individual';
    }
  }

  refreshStore(): void {
    this.subscriptions.add(
      this.auth.userProfile$
        .pipe(
          first(Boolean),
          flatMap(userProfile => this.accountService.getAdministratorOf(userProfile[auth0Constant.accountId]))
        )
        .subscribe(administrations => {
          this.store.dispatch(
            navActions.loadAdministrationSuccess({
              isAdmin: true,
              groupId: administrations[0].groupId,
              groupType: administrations[0].groupType
            })
          );
        })
    );
    this.subscriptions.add(
      this.auth.userProfile$
        .pipe(
          first(Boolean),
          flatMap(userProfile => this.accountService.uniqueGetSlot(userProfile[auth0Constant.accountId]))
        )
        .subscribe(
          slot => {
            let isMember = false;
            if (slot.subgroupId) {
              isMember = true;
            }
            this.store.dispatch(
              navActions.loadSlotSuccess({
                slot,
                isMember,
                isRegistered: true,
                groupId: slot.groupId,
                groupType: slot.groupType,
                subgroupId: slot.subgroupId
              })
            );
          },
          err => {
            if (err.status === 404) {
              this.store.dispatch(navActions.loadSlotFailed({ isRegistered: false }));
            }
          }
        )
    );
  }

  verifyPayment(): void {
    this.subscriptions.add(
      this.paymentService.processPayment(this.profileType, this.groupId, this.accountId, this.paymentId).subscribe(
        (result: PaymentSuccess) => {
          if (this.profileType === 'Team' && this.flowTYpe === 'additional') {
            void this.router.navigate([`/dashboard/team/${this.groupId}`]);
          } else if (this.flowTYpe === 'registration') {
            this.refreshStore();
          }
          this.store.dispatch(navActions.paymentSuccess({ paymentCompleted: true }));
          this.paymentSuccess = result;
          this.displayTextType = this.getDisplayTextType();
        },
        () => {
          // MUDO were does this navigation lead too?
          void this.router.navigate(['/register/payment-preparation/payment-error']);
        }
      )
    );
  }
}

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { AccountModule } from '../../account/account.module';
import { AccountPreferencesService } from '../../api/account-preferences/account-preferences.service';
import { AccountProfileService } from '../../api/account-profile/account-profile.service';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { sharedModules } from '../../shared/constant/shared-modules';
import { Language } from '../../shared/enum/language';
import { AccountPreferences } from '../../types/account/account-preferences';
import { AccountProfile } from '../../types/account/account-profile';
import { KnownFromEnum } from '../../types/account/known-from-enum';
import { PersonalProfileComponent } from './personal-profile.component';

interface Fixture {
  component: PersonalProfileComponent;
  fixture: ComponentFixture<PersonalProfileComponent>;
  authService: AuthService;
  accountProfileService: AccountProfileService;
  accountPreferenceService: AccountPreferencesService;
  cdref: ChangeDetectorRef;
  router: Router;
  accountProfile: AccountProfile;
  accountPreferences: AccountPreferences;
  selectedLanguage: Language;
  userData: {};
  accountId: string;
}

describe('PersonalProfileComponent', () => {
  beforeEach(() => {
    void TestBed.configureTestingModule({
      declarations: [PersonalProfileComponent],
      providers: [
        ChangeDetectorRef,
        {
          provide: AuthService,
          useValue: {
            userProfile$: of(null)
          }
        }
      ],
      imports: [
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
        ...appModules,
        ...sharedModules,
        AccountModule
      ]
    });
  });

  beforeEach(inject([AuthService], function(this: Fixture): void {
    this.fixture = TestBed.createComponent(PersonalProfileComponent);
    this.component = this.fixture.componentInstance;
    this.authService = TestBed.get(AuthService);
    this.accountProfileService = TestBed.get(AccountProfileService);
    this.accountPreferenceService = TestBed.get(AccountPreferencesService);
    this.cdref = TestBed.get(ChangeDetectorRef);
    this.router = TestBed.get(Router);
    this.accountId = 'id-OF-Eric';

    this.accountProfile = {
      accountId: 'id-OF-Eric',
      firstName: 'Eric',
      lastName: 'DoeRustig',
      gender: 'M',
      dateOfBirth: '1984-01-23T15:22:39.212Z',
      zip: '3600',
      structureVersion: 1
    };

    this.userData = {
      given_name: 'Max',
      family_name: 'Hunter',
      email: 'test@test.com',
      sub: '123456ABC'
    };

    this.accountPreferences = {
      accountId: 'preference-id',
      language: Language.nl,
      structureVersion: 2,
      verifiedEmail: {
        email: 'snelle@eddy.be',
        isVerified: false
      },
      newsletter: false,
      knownFromType: KnownFromEnum.FRIENDS
    };

    this.selectedLanguage = Language.nl;
  }));

  afterEach(function(this: Fixture): void {
    TestBed.resetTestingModule();
  });

  it('should create', function(this: Fixture): void {
    expect(this.component).toBeTruthy();
  });

  it('should set email if userData is available', async function(this: Fixture): Promise<void> {
    const httpClient = TestBed.get(HttpClient);
    spyOn(httpClient, 'get').and.returnValue(throwError(null));
    this.authService.userProfile$ = of(this.userData);
    this.fixture.detectChanges();
    await this.fixture.whenStable();
    expect(this.component.formGroup.controls.email.value).toBe('test@test.com');
  });

  it('should not call initialiseFormData if no userData', async function(this: Fixture): Promise<void> {
    this.authService.userProfile$ = of(null);
    spyOn(this.component, 'initialiseFormData');
    this.fixture.detectChanges();
    await this.fixture.whenStable();
    expect(this.component.initialiseFormData).not.toHaveBeenCalled();
  });

  it('should initialise accountData when userData is available', function(this: Fixture): void {
    this.component.createFormGroup();
    this.component.initialiseFormData(this.userData);

    expect(this.component.formGroup.controls.firstName.value).toBe('Max');
    expect(this.component.formGroup.controls.lastName.value).toBe('Hunter');
  });

  // it('should set accountProfile if data is available', async function(this: Fixture): Promise<void> {
  //   this.authService.userProfile$ = of({});
  //   spyOn(this.accountProfileService, 'getAccountProfileByAccountId').and.returnValue(of(this.accountProfile));
  //
  //   this.fixture.detectChanges();
  //   await this.fixture.whenStable();
  //
  //   expect(this.component.accountProfile.firstName).toBe('Eric');
  //   expect(this.component.accountProfile.lastName).toBe('DoeRustig');
  // });

  // it('should call initialiseFormData if no accountProfile can be found', async function(this: Fixture): Promise<void> {
  //   this.authService.userProfile$ = of({});
  //   spyOn(this.component, 'initialiseFormData');
  //   spyOn(this.accountProfileService, 'getAccountProfileByAccountId').and.returnValue(throwError({ Status: 404 }));
  //
  //   this.fixture.detectChanges();
  //   await this.fixture.whenStable();
  //   expect(this.component.initialiseFormData).toHaveBeenCalled();
  // });

  it('should not initialise accountData when userData is null', function(this: Fixture): void {
    const userData = {
      given_name: null,
      family_name: null,
      sub: null
    };
    this.component.createFormGroup();
    this.component.initialiseFormData(userData);

    expect(this.component.formGroup.controls.firstName.value).toBe(null);
    expect(this.component.formGroup.controls.lastName.value).toBe(null);
  });

  // it('should navigate when profile is saved successfully', function(this: Fixture): void {
  //   spyOn(this.router, 'navigate');
  //   spyOn(this.accountProfileService, 'saveAccountProfile').and.returnValue(of(this.accountProfile));
  //   spyOn(this.accountPreferenceService, 'saveAccountPreferences').and.returnValue(of(this.accountPreferences));
  //   this.component.initialiseFormData(this.accountProfile);
  //   this.component.onSubmitFormClick();
  //   expect(this.router.navigate).toHaveBeenCalledWith(['/register/payment-preparation']);
  // });

  // it('should not navigate when profile is saved unsuccessfully', function(this: Fixture): void {
  //   spyOn(this.router, 'navigate');
  //   spyOn(this.accountProfileService, 'saveAccountProfile').and.returnValue(throwError({ Status: 400 }));
  //   spyOn(this.accountPreferenceService, 'saveAccountPreferences').and.returnValue(of(this.accountPreferences));
  //   this.component.initialiseFormData(this.accountProfile);
  //   this.component.onSubmitFormClick();
  //   expect(this.router.navigate).not.toHaveBeenCalled();
  // });
});

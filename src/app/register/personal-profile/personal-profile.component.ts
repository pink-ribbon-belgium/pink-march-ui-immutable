//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.
// tslint:disable:template-cyclomatic-complexity //
import { AfterContentChecked, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { EMPTY, forkJoin, merge } from 'rxjs';
import { catchError, filter, finalize, flatMap } from 'rxjs/operators';
import * as uuidv4 from 'uuid/v4';
import { AccountPreferencesService } from '../../api/account-preferences/account-preferences.service';
import { AccountProfileService } from '../../api/account-profile/account-profile.service';
import { AccountService } from '../../api/account/account.service';
import { CompanyService } from '../../api/company/company.service';
import { PaymentService } from '../../api/payment/payment.service';
import { TeamService } from '../../api/team/team.service';
import { AuthService } from '../../auth/auth.service';
import * as navActions from '../../navigation/store/actions/nav.action';
import { NavState } from '../../navigation/store/state/nav.state';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../shared/constant/auth0.constants';
import { generalConstant } from '../../shared/constant/general.constants';
import { localStorageConstant } from '../../shared/constant/local-storage.constants';
import { Language } from '../../shared/enum/language';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';
import { AccountPreferences } from '../../types/account/account-preferences';
import { AccountProfile } from '../../types/account/account-profile';
import { KnownFromEnum } from '../../types/account/known-from-enum';
import { GroupAdministration } from '../../types/group/group-administration';
import { GroupProfile } from '../../types/group/group-profile';
import { Team } from '../../types/team/team';

@Component({
  selector: 'app-personal-profile',
  templateUrl: './personal-profile.component.html',
  styleUrls: ['./personal-profile.component.scss']
})
export class PersonalProfileComponent extends UnsubscribeOnDestroyAdapter
  implements OnInit, OnDestroy, AfterContentChecked {
  accountId: string;
  team: Team;
  groupProfile: GroupProfile;
  email: string;
  generatedId: string;
  registrationGroupType: string;
  registrationGroupId: string;
  languageSelections = Object.keys(Language);
  isSaving = false;
  isGroupAdmin = false;
  accountPreferences: AccountPreferences;
  formGroup: FormGroup;
  accountProfile: AccountProfile;
  knownFromTypes: Array<string>;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly accountService: AccountService,
    private readonly accountProfileService: AccountProfileService,
    private readonly accountPreferencesService: AccountPreferencesService,
    private readonly teamService: TeamService,
    private readonly companyService: CompanyService,
    private readonly snackbarService: DefaultSnackbarService,
    private readonly cdref: ChangeDetectorRef,
    private readonly paymentService: PaymentService,
    private readonly fb: FormBuilder,
    public translateService: TranslateService,
    public auth: AuthService,
    private readonly store: Store<NavState>
  ) {
    super();
  }

  ngOnInit(): void {
    this.knownFromTypes = Object.keys(KnownFromEnum);
    this.createFormGroup();
    this.route.params.subscribe(url => {
      this.registrationGroupType = url[generalConstant.groupType];
      this.registrationGroupId = url[generalConstant.groupId];
    });
    this.generatedId = uuidv4(); // MUDO: ID should not be generated, but backend still requires it (although the backend generates a new one)
    this.subscriptions.add(
      this.auth.userProfile$.subscribe((userData: any) => {
        if (userData) {
          this.accountId = userData[auth0Constant.accountId];
          this.initializePreferences(userData);
          this.subscriptions.add(
            this.accountService.uniqueGetSlot(this.accountId).subscribe(
              () => {
                void this.router.navigate(['/register/personal-profile-exists']);
              },
              () => {
                this.processNewProfile(userData);
              }
            )
          );
        }
      })
    );
    this.formGroupListeners();
  }

  createFormGroup(): void {
    this.formGroup = this.fb.group({
      language: [this.translateService.currentLang, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      gender: [null, Validators.required],
      dateOfBirth: [null, Validators.required],
      zip: [null, [Validators.required, Validators.pattern('^\\d{4}$')]],
      email: [null, [Validators.required, Validators.pattern('^\\b[+\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b$')]],
      disclaimer: [false, Validators.requiredTrue],
      newsletter: [false],
      knownFromType: [null, Validators.required]
    });
  }

  formGroupListeners(): void {
    this.subscriptions.add(
      this.formGroup
        .get('knownFromType')
        .valueChanges.pipe(filter(Boolean))
        .subscribe((value: string) => {
          if (value === KnownFromEnum.OTHER) {
            this.setOtherTextInput();
          } else {
            if (this.formGroup.get('otherText')) {
              this.formGroup.removeControl('otherText');
            }
          }
        })
    );
  }

  setOtherTextInput(): void {
    this.formGroup.addControl('otherText', this.fb.control(null, Validators.required));
  }

  processNewProfile(userData: any): void {
    this.subscriptions.add(
      this.accountProfileService.getAccountProfileByAccountId(this.accountId).subscribe(
        accountProfile => {
          this.initialiseFormData(accountProfile);
        },
        () => {
          this.initialiseFormData(userData);
        }
      )
    );
    if (!this.registrationGroupId || !this.registrationGroupType) {
      this.fetchTeamData(userData);
    }
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  onSubmitFormClick(): void {
    this.isSaving = true;
    if (!this.registrationGroupId || !this.registrationGroupType) {
      this.submitAllData();
    } else {
      this.registrationGroupType = this.registrationGroupType === 'team' ? 'Team' : 'Company';
      this.claimSlot();
    }
  }

  claimSlot(): void {
    this.subscriptions.add(
      forkJoin(
        this.accountProfileService.saveAccountProfile(this.getAccountProfileDto()),
        this.accountPreferencesService.saveAccountPreferences(
          {
            ...this.accountPreferences,
            verifiedEmail: { ...this.accountPreferences.verifiedEmail, email: this.formGroup.get('email').value },
            language: this.formGroup.get('language').value,
            newsletter: this.formGroup.get('newsletter').value,
            knownFromType: this.formGroup.get('knownFromType').value,
            otherText: this.formGroup.get('otherText') ? this.formGroup.get('otherText').value : undefined
          },
          this.accountId
        )
      )
        .pipe(
          flatMap(() =>
            this.registrationGroupType === generalConstant.companyGroupType
              ? this.companyService.claimSlot(this.registrationGroupId, this.accountId)
              : this.teamService.claimSlot(this.registrationGroupId, this.accountId)
          )
        )
        .subscribe(
          () => {
            this.store.dispatch(
              navActions.saveProfileSuccess({ profileLoaded: true, accountProfile: this.accountProfile })
            );
            this.store.dispatch(
              navActions.claimSlotSuccess({
                groupId: this.registrationGroupId,
                groupType: this.registrationGroupType,
                isRegistered: true
              })
            );
            this.subscriptions.add(
              this.accountService.uniqueGetSlot(this.accountId).subscribe(result => {
                this.store.dispatch(
                  navActions.loadSlotSuccess({
                    slot: result,
                    isRegistered: true,
                    groupId: this.registrationGroupId,
                    groupType: this.registrationGroupType,
                    isMember: false,
                    subgroupId: undefined
                  })
                );
                this.isSaving = false;
                void this.router.navigate(['/register/registration-success']);
              })
            );
          },
          () => {
            this.showTeamFullSnackBar();
          }
        )
    );
  }

  showTeamFullSnackBar(): void {
    this.translateService.get('REGISTER.PERSONAL_PROFILE.ERROR_SLOTS').subscribe((translation: string) => {
      this.snackbarService.openSnackBar(translation);
    });
  }

  submitAllData(): void {
    this.isGroupAdmin ? this.saveExistingGroupAdmin() : this.saveNewGroupAdmin();
  }

  saveNewGroupAdmin(): void {
    this.subscriptions.add(
      forkJoin([
        this.accountProfileService.saveAccountProfile(this.getAccountProfileDto()),
        this.accountPreferencesService.saveAccountPreferences(
          {
            ...this.accountPreferences,
            verifiedEmail: { ...this.accountPreferences.verifiedEmail, email: this.formGroup.get('email').value },
            language: this.formGroup.get('language').value,
            newsletter: this.formGroup.get('newsletter').value,
            knownFromType: this.formGroup.get('knownFromType').value,
            otherText: this.formGroup.get('otherText') ? this.formGroup.get('otherText').value : undefined
          },
          this.accountId
        ),
        this.teamService.saveTeam({
          ...this.team,
          name: `${this.formGroup.get('firstName').value} ${this.formGroup.get('lastName').value}`,
          accountId: this.accountId
        })
      ]).subscribe(
        resultData => {
          this.team.id = resultData[2].teamId;
          this.subscriptions.add(
            this.auth.getTokenSilently$({ audience: auth0Constant.audience, ignoreCache: true }).subscribe(
              () => {
                this.processSaveSuccess();
              },
              () => {
                this.isSaving = false;
              }
            )
          );
        },
        () => {
          this.isSaving = false;
        }
      )
    );
  }

  saveExistingGroupAdmin(): void {
    this.subscriptions.add(
      forkJoin([
        this.accountProfileService.saveAccountProfile(this.getAccountProfileDto()),
        this.accountPreferencesService.saveAccountPreferences(
          {
            ...this.accountPreferences,
            verifiedEmail: { ...this.accountPreferences.verifiedEmail, email: this.formGroup.get('email').value },
            language: this.formGroup.get('language').value,
            newsletter: this.formGroup.get('newsletter').value,
            knownFromType: this.formGroup.get('knownFromType').value,
            otherText: this.formGroup.get('otherText') ? this.formGroup.get('otherText').value : undefined
          },
          this.accountId
        )
      ])
        .pipe(finalize(() => (this.isSaving = false)))
        .subscribe(() => {
          this.processSaveSuccess();
        })
    );
  }

  fetchTeamData(userData: any): void {
    this.subscriptions.add(
      this.accountService.getAdministratorOf(this.accountId).subscribe(
        groupsAdministration => {
          const groupAdministration: GroupAdministration = groupsAdministration[0];
          this.isGroupAdmin = true;
          this.subscriptions.add(
            forkJoin(
              this.teamService.getTeamById(groupAdministration.groupId),
              this.teamService.getTeamProfileById(groupAdministration.groupId)
            ).subscribe(
              ([teamData, groupProfile]) => {
                this.initialiseTeamData(teamData);
                this.checkTeamPayments(groupProfile, groupAdministration.groupId);
              },
              () => {
                this.initialiseGroupProfileData(userData);
                this.initialiseTeamData(userData);
              }
            )
          );
        },
        () => {
          this.initialiseGroupProfileData(userData);
          this.initialiseTeamData(userData);
        }
      )
    );
  }

  processSaveSuccess(): void {
    this.isSaving = false;
    this.store.dispatch(navActions.saveProfileSuccess({ profileLoaded: true, accountProfile: this.accountProfile }));
    localStorage.setItem(localStorageConstant.profileType, 'Team');
    localStorage.setItem(localStorageConstant.groupId, this.team.id);
    localStorage.setItem(localStorageConstant.flow, 'registration');
    void this.router.navigate(['/register/payment-preparation']);
  }

  initialiseFormData(userData: any): void {
    this.formGroup.setValue({
      ...this.formGroup.getRawValue(),
      firstName: userData.firstName ? userData.firstName : userData.given_name ? userData.given_name : null,
      lastName: userData.lastName ? userData.lastName : userData.family_name ? userData.family_name : null,
      dateOfBirth: userData.dateOfBirth ? userData.dateOfBirth : null,
      gender: userData.gender ? userData.gender : null,
      zip: userData.zip ? userData.zip : null
    });
  }

  initialiseTeamData(teamData: any): void {
    this.team = {
      id: teamData.id ? teamData.id : this.generatedId,
      code: 'FKFG-KDKK-KSKK-LWSK',
      groupType: generalConstant.teamGroupType,
      structureVersion: teamData.structureVersion ? teamData.structureVersion : 1
    };
  }

  initialiseGroupProfileData(groupProfile: any): void {
    this.groupProfile = {
      id: groupProfile.id,
      name: groupProfile.name ? groupProfile.name : 'Demo', // TODO: Is this needed?
      structureVersion: 1
    };
  }

  initializePreferences(userData: any): void {
    const emailControl = this.formGroup.get('email');
    this.accountPreferencesService.getAccountPreferencesByAccountId(this.accountId).subscribe(
      (preferences: AccountPreferences) => {
        this.accountPreferences = preferences;
        delete this.accountPreferences.links;
        if (preferences.verifiedEmail) {
          emailControl.setValue(preferences.verifiedEmail.email);
          emailControl.disable();
        }
        if (preferences.knownFromType === KnownFromEnum.OTHER) {
          this.setOtherTextInput();
          this.formGroup.get('otherText').setValue(preferences.otherText);
        }
        this.formGroup.get('language').setValue(preferences.language);
        this.formGroup.get('newsletter').setValue(preferences.newsletter);
        this.formGroup.get('knownFromType').setValue(preferences.knownFromType);
        this.translateService.use(preferences.language);
      },
      () => {
        this.accountPreferences = {
          structureVersion: 2,
          accountId: this.accountId,
          language: this.translateService.currentLang as Language,
          verifiedEmail: {
            email: userData.email,
            isVerified: userData.email_verified
          },
          newsletter: false,
          knownFromType: null
        };
        emailControl.setValue(userData.email);
        if (emailControl.value) {
          emailControl.disable();
        }
      }
    );
  }

  checkTeamPayments(groupProfile: GroupProfile, groupId: string): void {
    this.subscriptions.add(
      this.paymentService.getPaymentsForGroup('team', groupId).subscribe(
        paymentData => {
          if (paymentData.some(p => p.paymentStatus === '9')) {
            void this.router.navigate(['/register/personal-profile-exists']);

            return;
          }
          merge(
            ...paymentData.map(payment =>
              this.paymentService
                .processPayment('team', groupId, this.accountId, payment.paymentId)
                .pipe(catchError(() => EMPTY))
            )
          )
            .pipe(
              finalize(() => {
                this.initialiseGroupProfileData(groupProfile);
              })
            )
            .subscribe(() => {
              void this.router.navigate(['/register/personal-profile-exists']);
            });
        },
        () => {
          this.initialiseGroupProfileData(groupProfile);
        }
      )
    );
  }

  getAccountProfileDto(): AccountProfile {
    const formValue = this.formGroup.getRawValue();

    this.accountProfile = {
      firstName: formValue.firstName,
      lastName: formValue.lastName,
      dateOfBirth: formValue.dateOfBirth,
      gender: formValue.gender,
      zip: formValue.zip,
      accountId: this.accountId,
      structureVersion: 2
    };

    return this.accountProfile;
  }

  trackByFn(index): number {
    return index;
  }

  getEnumTranslationKey(type: string): string {
    return `REGISTER.KNOWN_FROM_ENUM.${type}`;
  }

  // tslint:disable-next-line:max-file-line-count
}

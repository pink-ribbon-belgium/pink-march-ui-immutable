//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyProfileExistsComponent } from './company-profile-exists/company-profile-exists.component';
import { CompanyProfileComponent } from './company-profile/company-profile.component';
import { PaymentPreparationComponent } from './payment-preparation/payment-preparation.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { PersonalProfileExistsComponent } from './personal-profile-exists/personal-profile-exists.component';
import { PersonalProfileComponent } from './personal-profile/personal-profile.component';
import { RegisterComponent } from './register.component';
import { RegistrationSuccessComponent } from './registration-success/registration-success.component';

const registerRoutes: Routes = [
  {
    path: '',
    component: RegisterComponent,
    pathMatch: 'full'
  },
  {
    path: 'personal-profile',
    component: PersonalProfileComponent
  },
  {
    path: 'personal-profile/:groupType/:groupId',
    component: PersonalProfileComponent
  },
  {
    path: 'personal-profile-exists',
    component: PersonalProfileExistsComponent
  },
  {
    path: 'company-profile',
    component: CompanyProfileComponent
  },
  {
    path: 'company-profile/:unitCode',
    component: CompanyProfileComponent
  },
  {
    path: 'company-profile-exists',
    component: CompanyProfileExistsComponent
  },
  {
    path: `payment-preparation`,
    component: PaymentPreparationComponent
  },
  {
    path: 'payment-preparation/:payment-error',
    component: PaymentPreparationComponent
  },
  {
    path: 'registration-success',
    component: RegistrationSuccessComponent
  },
  {
    path: 'payment-success',
    component: PaymentSuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(registerRoutes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule {}

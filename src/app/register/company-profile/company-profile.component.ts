//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

// tslint:disable:template-cyclomatic-complexity //

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { EMPTY, forkJoin, merge } from 'rxjs';
import { catchError, filter, finalize } from 'rxjs/operators';
import * as uuidv4 from 'uuid/v4';
import { AccountPreferencesService } from '../../api/account-preferences/account-preferences.service';
import { AccountService } from '../../api/account/account.service';
import { CompanyService } from '../../api/company/company.service';
import { PaymentService } from '../../api/payment/payment.service';
import { AuthService } from '../../auth/auth.service';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../shared/constant/auth0.constants';
import { generalConstant } from '../../shared/constant/general.constants';
import { localStorageConstant } from '../../shared/constant/local-storage.constants';
import { Language } from '../../shared/enum/language';
import { AccountPreferences } from '../../types/account/account-preferences';
import { KnownFromEnum } from '../../types/account/known-from-enum';
import { Company } from '../../types/company/company';
import { GroupAdministration } from '../../types/group/group-administration';
import { GroupProfile } from '../../types/group/group-profile';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.scss']
})
export class CompanyProfileComponent extends UnsubscribeOnDestroyAdapter implements OnInit, OnDestroy {
  groupProfile: GroupProfile;
  company: Company;
  groupAdministration: GroupAdministration;
  accountId: string;
  generatedId: string;
  unitCode: string;
  isSaving = false;
  isGroupAdmin = false;
  verifyingPayments = false;
  accountPreferences: AccountPreferences;
  formGroup: FormGroup;
  knownFromTypes: Array<string>;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    public auth: AuthService,
    private readonly accountService: AccountService,
    private readonly companyService: CompanyService,
    private readonly paymentService: PaymentService,
    private readonly accountPreferencesService: AccountPreferencesService,
    private readonly fb: FormBuilder,
    public translate: TranslateService
  ) {
    super();
  }

  ngOnInit(): void {
    this.knownFromTypes = Object.keys(KnownFromEnum);
    this.route.params.subscribe(url => {
      this.unitCode = url[generalConstant.unitCode];
    });
    localStorage.setItem(localStorageConstant.flow, 'registration');

    this.generatedId = uuidv4();
    this.subscriptions.add(
      this.auth.userProfile$.subscribe((userData: any) => {
        if (userData) {
          this.accountId = userData[auth0Constant.accountId];
          this.initializePreferences(userData);
          this.subscriptions.add(
            this.accountService.uniqueGetSlot(this.accountId).subscribe(
              () => {
                void this.router.navigate(['/register/personal-profile-exists']);
              },
              () => {
                this.processNewProfile(userData);
              }
            )
          );
        }
      })
    );
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      name: [this.groupProfile.name, Validators.required],
      address: [this.company.address, Validators.required],
      zip: [this.company.zip, [Validators.required, Validators.pattern('^\\d{4}$')]],
      city: [this.company.city, Validators.required],
      vat: [this.company.vat, [Validators.required, Validators.pattern('^BE\\d{10}$')]],
      reference: [],
      contactFirstName: [this.company.contactFirstName, Validators.required],
      contactLastName: [this.company.contactLastName, Validators.required],
      contactTelephone: [this.company.contactTelephone, [Validators.required, Validators.pattern('^\\+\\d{10,20}$')]],
      contactEmail: [
        { value: this.company.contactEmail, disabled: !!this.company.contactEmail },
        [Validators.required, Validators.pattern('^\\b[+\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b$')]
      ],
      disclaimer: [false, Validators.requiredTrue],
      newsletter: [false],
      knownFromType: [null, Validators.required]
    });
    this.formGroupListeners();
  }

  formGroupListeners(): void {
    this.subscriptions.add(
      this.formGroup
        .get('knownFromType')
        .valueChanges.pipe(filter(Boolean))
        .subscribe((value: string) => {
          if (value === KnownFromEnum.OTHER) {
            this.setOtherTextInput();
          } else {
            if (this.formGroup.get('otherText')) {
              this.formGroup.removeControl('otherText');
            }
          }
        })
    );
  }

  setOtherTextInput(): void {
    this.formGroup.addControl('otherText', this.fb.control(null, Validators.required));
  }

  initializePreferences(userData: any): void {
    this.accountPreferencesService.getAccountPreferencesByAccountId(this.accountId).subscribe(
      (preferences: AccountPreferences) => {
        this.accountPreferences = preferences;
        delete this.accountPreferences.links;
      },
      () => {
        this.accountPreferences = {
          structureVersion: 2,
          accountId: this.accountId,
          language: this.translate.currentLang as Language,
          verifiedEmail: {
            email: userData.email,
            isVerified: userData.email_verified
          },
          newsletter: false,
          knownFromType: null
        };
      }
    );
  }

  processNewProfile(userData: any): void {
    this.subscriptions.add(
      this.accountService.getAdministratorOf(this.accountId).subscribe(
        groupsAdministration => {
          const groupAdministration: GroupAdministration = groupsAdministration[0];
          this.isGroupAdmin = true;
          forkJoin(
            this.companyService.getCompanyProfileById(groupAdministration.groupId),
            this.companyService.getCompanyById(groupAdministration.groupId)
          ).subscribe(
            resultData => {
              this.checkCompanyForPayment(resultData, groupAdministration.groupId);
            },
            () => {
              this.initialiseCompanyFormData(userData);
              this.initialiseGroupProfileFormData(userData);
              this.createForm();
            }
          );
        },
        () => {
          this.initialiseCompanyFormData(userData);
          this.initialiseGroupProfileFormData(userData);
          this.createForm();
        }
      )
    );
  }

  checkCompanyForPayment(resultData: Array<any>, groupId: string): void {
    this.subscriptions.add(
      this.paymentService.getPaymentsForGroup('company', resultData[1].id).subscribe(
        paymentData => {
          if (paymentData.some(p => p.paymentStatus === '9')) {
            void this.router.navigate(['/register/company-profile-exists']);

            return;
          }
          merge(
            ...paymentData.map(payment =>
              this.paymentService
                .processPayment('company', groupId, this.accountId, payment.paymentId)
                .pipe(catchError(() => EMPTY))
            )
          )
            .pipe(
              finalize(() => {
                this.initialiseGroupProfileFormData(resultData[0]);
                this.initialiseCompanyFormData(resultData[1]);
                this.createForm();
              })
            )
            .subscribe(() => {
              void this.router.navigate(['/register/company-profile-exists']);
            });
        },
        () => {
          this.initialiseGroupProfileFormData(resultData[0]);
          this.initialiseCompanyFormData(resultData[1]);
          this.createForm();
        }
      )
    );
  }

  onSubmitFormClick(): void {
    this.isSaving = true;
    if (this.isGroupAdmin) {
      this.saveExistingGroupAdmin();
    } else {
      this.saveNewGroupAdmin();
    }
  }

  saveNewGroupAdmin(): void {
    const companyToSave = { ...this.formGroup.getRawValue() };
    delete companyToSave.disclaimer;
    delete companyToSave.newsletter;
    delete companyToSave.knownFromType;
    if (this.formGroup.get('otherText')) {
      delete companyToSave.otherText;
    }
    forkJoin([
      this.accountPreferencesService.saveAccountPreferences(
        {
          ...this.accountPreferences,
          newsletter: this.formGroup.get('newsletter').value,
          knownFromType: this.formGroup.get('knownFromType').value,
          otherText: this.formGroup.get('otherText') ? this.formGroup.get('otherText').value : undefined
        },
        this.accountId
      ),
      this.companyService.createCompany({
        ...this.company,
        ...companyToSave,
        accountId: this.accountId
      })
    ]).subscribe(
      resultData => {
        this.company.id = resultData[1].companyId;
        this.subscriptions.add(
          this.auth.getTokenSilently$({ audience: auth0Constant.audience, ignoreCache: true }).subscribe(
            () => {
              this.processSaveSuccess();
            },
            () => {
              this.isSaving = false;
            }
          )
        );
      },
      () => {
        this.isSaving = false;
      }
    );
  }

  saveExistingGroupAdmin(): void {
    forkJoin([
      this.accountPreferencesService.saveAccountPreferences(
        {
          ...this.accountPreferences,
          newsletter: this.formGroup.get('newsletter').value,
          knownFromType: this.formGroup.get('knownFromType').value,
          otherText: this.formGroup.get('otherText') ? this.formGroup.get('otherText').value : undefined
        },
        this.accountId
      ),
      this.companyService.updateCompany(this.company),
      this.companyService.saveCompanyProfile(this.groupProfile)
    ]).subscribe(
      () => {
        this.processSaveSuccess();
      },
      () => {
        this.isSaving = false;
      }
    );
  }

  processSaveSuccess(): void {
    this.isSaving = false;
    localStorage.setItem(localStorageConstant.profileType, 'Company');
    localStorage.setItem(localStorageConstant.groupId, this.company.id);
    void this.router.navigate(['/register/payment-preparation']);
  }

  initialiseCompanyFormData(companyData: any): void {
    this.company = {
      id: companyData.id ? companyData.id : this.generatedId,
      address: companyData.address ? companyData.address : null,
      city: companyData.city ? companyData.city : null,
      vat: companyData.vat ? companyData.vat : null,
      reference: companyData.reference ? companyData.reference : null,
      logo: null,
      contactFirstName: companyData.contactFirstName
        ? companyData.contactFirstName
        : companyData.given_name
        ? companyData.given_name
        : null,
      contactLastName: companyData.contactLastName
        ? companyData.contactLastName
        : companyData.family_name
        ? companyData.family_name
        : null,
      contactEmail: companyData.contactEmail ? companyData.contactEmail : companyData.email ? companyData.email : null,
      contactTelephone: companyData.contactTelephone ? companyData.contactTelephone : null,
      zip: companyData.zip ? companyData.zip : null,
      code: 'FKFG-KDKK-KSKK-LWSK',
      groupType: generalConstant.companyGroupType,
      structureVersion: companyData.structureVersion ? companyData.structureVersion : 1,
      unit: this.unitCode ? this.unitCode : null
    };
  }

  initialiseGroupProfileFormData(groupProfile: any): void {
    this.groupProfile = {
      id: groupProfile.id ? groupProfile.id : this.generatedId,
      name: groupProfile.name && !groupProfile.nickname ? groupProfile.name : null,
      structureVersion: 1
    };
  }

  trackByFn(index): number {
    return index;
  }

  getEnumTranslationKey(type: string): string {
    return `REGISTER.KNOWN_FROM_ENUM.${type}`;
  }
}

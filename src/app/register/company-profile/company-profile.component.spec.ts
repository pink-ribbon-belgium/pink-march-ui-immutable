//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { Router } from '@angular/router';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { CompanyContactComponent } from '../../company/company-contact/company-contact.component';
import { CompanyDetailsComponent } from '../../company/company-details/company-details.component';
import { UploadImageComponent } from '../../group/upload-image/upload-image.component';
import { sharedModules } from '../../shared/constant/shared-modules';
import { Language } from '../../shared/enum/language';
import { CompanyProfileComponent } from './company-profile.component';

interface Fixture {
  component: CompanyProfileComponent;
  fixture: ComponentFixture<CompanyProfileComponent>;
  authService: AuthService;
  router: Router;
  selectedLanguage: Language;
}

describe('CompanyProfileComponent', () => {
  beforeEach(() => {
    void TestBed.configureTestingModule({
      declarations: [CompanyProfileComponent, CompanyDetailsComponent, CompanyContactComponent, UploadImageComponent],
      imports: [RouterTestingModule.withRoutes([]), BrowserAnimationsModule, ...appModules, ...sharedModules],
      providers: [
        {
          provide: AuthService,
          useValue: {
            userProfile$: of(null)
          }
        }
      ]
    });
  });

  beforeEach(inject([AuthService], function(this: Fixture): void {
    this.fixture = TestBed.createComponent(CompanyProfileComponent);
    this.component = this.fixture.componentInstance;
    this.authService = TestBed.get(AuthService);
    this.router = TestBed.get(Router);

    this.selectedLanguage = Language.nl;
  }));

  it('should create', function(this: Fixture): void {
    expect(this.component).toBeTruthy();
  });

  it('should not call initialiseFormData if no userData', async function(this: Fixture): Promise<void> {
    spyOn(this.component, 'initialiseCompanyFormData');
    this.fixture.detectChanges();
    await this.fixture.whenStable();
    expect(this.component.initialiseCompanyFormData).not.toHaveBeenCalled();
  });

  // it('should call initialiseFormData if userData', async function(this: Fixture): Promise<void> {
  //   this.authService.userProfile$ = of({});
  //   spyOn(this.component, 'initialiseCompanyFormData');
  //   this.fixture.detectChanges();
  //   await this.fixture.whenStable();
  //   expect(this.component.initialiseCompanyFormData).toHaveBeenCalled();
  // });

  // it('should initialise accountData when userData is available', function(this: Fixture): void {
  //   const userData = {
  //     given_name: 'Max',
  //     family_name: 'Hunter',
  //     email: 'test@test.com',
  //     sub: '123456ABC'
  //   };
  //   this.component.initialiseCompanyFormData(userData);
  //   this.fixture.detectChanges();
  //   expect(this.component.company.contactFirstName).toBe('Max');
  //   expect(this.component.company.contactLastName).toBe('Hunter');
  //   expect(this.component.company.contactEmail).toBe('test@test.com');
  //   expect(this.component.accountId).toBe('123456ABC');
  // });

  // it('should not initialise accountData when userData is null', function(this: Fixture): void {
  //   const userData = {
  //     given_name: null,
  //     family_name: null,
  //     email: null,
  //     sub: null
  //   };
  //   this.component.initialiseCompanyFormData(userData);
  //   this.fixture.detectChanges();
  //   expect(this.component.company.contactFirstName).toBe(null);
  //   expect(this.component.company.contactLastName).toBe(null);
  //   expect(this.component.company.contactEmail).toBe(null);
  //   expect(this.component.accountId).toBe(null);
  // });

  // it('should execute onSubmitClicked after submit form button click', async function(this: Fixture): Promise<void> {
  //   spyOn(this.component, 'onSubmitFormClick');
  //   const button = this.fixture.debugElement.nativeElement.querySelector('#submit');
  //   button.click();
  //   this.fixture.detectChanges();
  //   await this.fixture.whenStable();
  //   expect(this.component.onSubmitFormClick).toHaveBeenCalled();
  // });

  // it('should navigate when onSubmitClicked called', function(this: Fixture): void {
  //   spyOn(this.router, 'navigate');
  //   this.component.onSubmitFormClick();
  //   expect(this.router.navigate).toHaveBeenCalledWith(['/register/payment-preparation']);
  // });
});

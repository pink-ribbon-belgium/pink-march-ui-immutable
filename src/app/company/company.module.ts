import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UploadImageComponent } from '../group/upload-image/upload-image.component';
import { SharedModule } from '../shared/shared.module';
import { CompanyContactComponent } from './company-contact/company-contact.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';

@NgModule({
  declarations: [CompanyDetailsComponent, CompanyContactComponent, UploadImageComponent],
  imports: [CommonModule, SharedModule],
  exports: [CompanyDetailsComponent, CompanyContactComponent, UploadImageComponent]
})
export class CompanyModule {}

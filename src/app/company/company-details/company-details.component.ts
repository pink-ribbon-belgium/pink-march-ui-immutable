// tslint:disable:template-cyclomatic-complexity //

import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { localStorageConstant } from '../../shared/constant/local-storage.constants';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {
  @Input() formGroup: FormGroup;
  flowType: string;
  isRegistration = false;

  ngOnInit(): void {
    this.flowType = localStorage.getItem(localStorageConstant.flow);
    if (this.flowType === 'registration') {
      this.isRegistration = true;
    }
  }
}

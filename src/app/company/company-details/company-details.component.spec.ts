import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { UploadImageComponent } from '../../group/upload-image/upload-image.component';
import { sharedModules } from '../../shared/constant/shared-modules';
import { CompanyDetailsComponent } from './company-details.component';

describe('CompanyDetailsComponent', () => {
  let component: CompanyDetailsComponent;
  let fixture: ComponentFixture<CompanyDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyDetailsComponent, UploadImageComponent],
      imports: [...appModules, ...sharedModules],
      providers: [
        {
          provide: AuthService,
          useValue: {}
        }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyDetailsComponent);
    component = fixture.componentInstance;
    const store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => (key in store ? store[key] : null),
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      }
    };

    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
    localStorage.setItem('flow', 'registration');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set isRegistration to true', async () => {
    fixture.detectChanges();
    await fixture.whenStable();
    expect(component.isRegistration).toBe(true);
  });
});

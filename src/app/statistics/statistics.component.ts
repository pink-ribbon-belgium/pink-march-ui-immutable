import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../api/statistics/statistics.service';
import { UnsubscribeOnDestroyAdapter } from '../shared/adapter/unsubscribe-on-destroy-adapter';
import { Statistics } from '../types/statistics/statistics';

// tslint:disable:template-cyclomatic-complexity //

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  statistics: Statistics;
  constructor(private readonly statisticsService: StatisticsService) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.statisticsService.getStatistics().subscribe(result => {
        this.statistics = result;
      })
    );
  }
}

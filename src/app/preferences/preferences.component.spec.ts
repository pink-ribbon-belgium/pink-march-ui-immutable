import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { of, throwError } from 'rxjs';
import { AccountPreferencesService } from '../api/account-preferences/account-preferences.service';
import { appModules } from '../app-modules.constant';
import { AuthService } from '../auth/auth.service';
import { sharedModules } from '../shared/constant/shared-modules';
import { Language } from '../shared/enum/language';
import { KnownFromEnum } from '../types/account/known-from-enum';
import { PreferencesComponent } from './preferences.component';

describe('PreferencesComponent', () => {
  let component: PreferencesComponent;
  let fixture: ComponentFixture<PreferencesComponent>;

  interface Fixture {
    translateService: TranslateService;
    authService: AuthService;
    accountPreferencesService: AccountPreferencesService;
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PreferencesComponent],
      imports: [...appModules, ...sharedModules, RouterTestingModule],
      providers: [
        {
          provide: AuthService,
          useValue: {}
        }
      ]
    });
  });

  beforeEach(function(this: Fixture): void {
    fixture = TestBed.createComponent(PreferencesComponent);
    component = fixture.componentInstance;
    this.translateService = TestBed.get<TranslateService>(TranslateService);
    this.authService = TestBed.get<AuthService>(AuthService);
    this.accountPreferencesService = TestBed.get<AccountPreferencesService>(AccountPreferencesService);
    this.translateService.use('nl');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call translateService use when value changes', async function(this: Fixture): Promise<void> {
    spyOn(this.translateService, 'use');
    component.createForm();
    component.formGroup.get('language').setValue('fr');
    await fixture.whenStable();
    expect(this.translateService.use).toHaveBeenCalledWith('fr');
  });

  it('should set form value to preferences when available', async function(this: Fixture): Promise<void> {
    this.authService.userProfile$ = of({});
    const accountPreferences = {
      structureVersion: 2,
      language: Language.nl,
      verifiedEmail: {
        email: 'ditiseenemail@email.be',
        isVerified: false
      },
      newsletter: false,
      knownFromType: KnownFromEnum.FRIENDS,
      accountId: 'nahana'
    };
    spyOn(this.accountPreferencesService, 'getAccountPreferencesByAccountId').and.returnValue(of(accountPreferences));
    fixture.detectChanges();
    await fixture.whenStable();
    const formGroupValue = component.formGroup.getRawValue();
    expect(formGroupValue.language).toEqual(accountPreferences.language);
    expect(formGroupValue.verifiedEmail).toBeTruthy();
    expect(formGroupValue.verifiedEmail.email).toEqual(accountPreferences.verifiedEmail.email);
    expect(formGroupValue.verifiedEmail.isVerified).toEqual(accountPreferences.verifiedEmail.isVerified);
  });

  it('should enable email when userPreferences and userProfile have none', async function(this: Fixture): Promise<
    void
  > {
    this.authService.userProfile$ = of({ email: null, email_verified: null });
    const accountPreferences = {
      structureVersion: 2,
      language: Language.nl,
      accountId: 'nahana',
      newsletter: false,
      knownFromType: KnownFromEnum.FRIENDS
    };
    spyOn(this.accountPreferencesService, 'getAccountPreferencesByAccountId').and.returnValue(of(accountPreferences));
    fixture.detectChanges();
    await fixture.whenStable();
    expect(component.formGroup.get('verifiedEmail.email').enabled).toBe(true);
  });

  it('should enable email when userProfile has none', async function(this: Fixture): Promise<void> {
    this.authService.userProfile$ = of({ email: null, email_verified: null });
    spyOn(this.accountPreferencesService, 'getAccountPreferencesByAccountId').and.returnValue(throwError(null));
    fixture.detectChanges();
    await fixture.whenStable();
    expect(component.formGroup.get('verifiedEmail.email').enabled).toBe(true);
  });

  it('should use userProfile when there are no preferences', async function(this: Fixture): Promise<void> {
    const userProfile = {
      email: 'usermail@mail.com',
      email_verified: false
    };
    this.authService.userProfile$ = of(userProfile);
    spyOn(this.accountPreferencesService, 'getAccountPreferencesByAccountId').and.returnValue(throwError(null));
    fixture.detectChanges();
    await fixture.whenStable();
    const formValue = component.formGroup.getRawValue();
    expect(formValue.verifiedEmail).toBeTruthy();
    expect(formValue.verifiedEmail.email).toEqual(userProfile.email);
    expect(formValue.verifiedEmail.isVerified).toEqual(userProfile.email_verified);
  });

  it('should keep email disabled when it already was', async function(this: Fixture): Promise<void> {
    component.accountPreferences = {
      structureVersion: 2,
      accountId: 'nahana',
      language: Language.nl,
      verifiedEmail: {
        email: 'email@email.com',
        isVerified: false
      },
      newsletter: false,
      knownFromType: KnownFromEnum.FRIENDS
    };
    component.createForm();
    spyOn(this.accountPreferencesService, 'saveAccountPreferences').and.returnValue(of(null));
    spyOn(this.translateService, 'get').and.returnValue(of(''));
    component.saveAccountPreferences();
    await fixture.whenStable();
    expect(component.formGroup.get('verifiedEmail.email').disabled).toBe(true);
  });

  it('should disable email when it was enabled', async function(this: Fixture): Promise<void> {
    component.accountPreferences = {
      structureVersion: 2,
      accountId: 'nahana',
      language: Language.nl,
      verifiedEmail: {
        email: 'email@email.com',
        isVerified: false
      },
      newsletter: false,
      knownFromType: KnownFromEnum.FRIENDS
    };
    component.createForm();
    component.formGroup.get('verifiedEmail.email').enable();
    spyOn(this.accountPreferencesService, 'saveAccountPreferences').and.returnValue(of(null));
    spyOn(this.translateService, 'get').and.returnValue(of(''));
    component.saveAccountPreferences();
    await fixture.whenStable();
    expect(component.formGroup.get('verifiedEmail.email').disabled).toBe(true);
  });
});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { filter, finalize, first, flatMap, tap } from 'rxjs/operators';
import { AccountPreferencesService } from '../api/account-preferences/account-preferences.service';
import { AuthService } from '../auth/auth.service';
import { UnsubscribeOnDestroyAdapter } from '../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../shared/constant/auth0.constants';
import { Language } from '../shared/enum/language';
import { DefaultSnackbarService } from '../shared/services/default-snackbar/default-snackbar.service';
import { AccountPreferences } from '../types/account/account-preferences';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  languageSelections = Object.keys(Language);
  formGroup: FormGroup;
  accountId: string;
  isLoading: boolean;
  accountPreferences: AccountPreferences;
  isSaving: boolean;
  userProfile;

  constructor(
    private readonly fb: FormBuilder,
    private readonly auth: AuthService,
    private readonly accountPreferencesService: AccountPreferencesService,
    private readonly translateService: TranslateService,
    private readonly snackbar: DefaultSnackbarService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.getAccountPreferences();
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      language: [null, Validators.required],
      verifiedEmail: this.fb.group({
        email: [
          { value: null, disabled: true },
          [Validators.required, Validators.pattern('^\\b[+\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b$')]
        ],
        isVerified: null
      }),
      newsletter: [false]
    });
    this.subscriptions.add(
      this.formGroup
        .get('language')
        .valueChanges.pipe(filter(Boolean))
        .subscribe((value: string) => this.translateService.use(value))
    );
  }

  getAccountPreferences(): void {
    this.isLoading = true;
    this.subscriptions.add(
      this.auth.userProfile$
        .pipe(
          first(Boolean),
          tap(userProfile => {
            this.accountId = userProfile[auth0Constant.accountId];
            this.userProfile = userProfile;
          }),
          flatMap(() => this.accountPreferencesService.getAccountPreferencesByAccountId(this.accountId)),
          finalize(() => (this.isLoading = false))
        )
        .subscribe(
          accountPreferences => {
            this.accountPreferences = accountPreferences;
            this.formGroup.setValue({
              language: accountPreferences.language,
              verifiedEmail: accountPreferences.verifiedEmail || {
                email: this.userProfile.email,
                isVerified: this.userProfile.email_verified
              },
              newsletter: this.accountPreferences.newsletter
            });
            if (!this.formGroup.get('verifiedEmail.email').value) {
              this.formGroup.get('verifiedEmail.email').enable();
            }
          },
          () => {
            this.accountPreferences = {
              structureVersion: 2,
              accountId: this.accountId,
              language: this.translateService.currentLang as Language,
              verifiedEmail: {
                email: this.userProfile.email,
                isVerified: this.userProfile.email_verified
              },
              newsletter: false,
              knownFromType: null
            };
            this.formGroup.setValue({
              language: this.accountPreferences.language,
              verifiedEmail: this.accountPreferences.verifiedEmail,
              newsletter: false
            });
            if (!this.userProfile.email) {
              this.formGroup.get('verifiedEmail.email').enable();
            }
          }
        )
    );
  }

  saveAccountPreferences(): void {
    this.isSaving = true;
    const savableData = { ...this.accountPreferences, ...this.formGroup.getRawValue() };
    delete savableData.links;
    this.subscriptions.add(
      this.accountPreferencesService
        .saveAccountPreferences(savableData, this.accountId)
        .pipe(
          flatMap(() => this.translateService.get('PREFERENCES.SNACK_BAR.SAVED')),
          finalize(() => (this.isSaving = false))
        )
        .subscribe(successMessage => {
          this.snackbar.openSnackBar(successMessage);
          this.formGroup.reset({
            language: savableData.language,
            verifiedEmail: savableData.verifiedEmail,
            newsletter: savableData.newsletter
          });
          if (this.formGroup.get('verifiedEmail.email').enabled) {
            this.formGroup.get('verifiedEmail.email').disable();
          }
        })
    );
  }

  trackByFn(index): number {
    return index;
  }
}

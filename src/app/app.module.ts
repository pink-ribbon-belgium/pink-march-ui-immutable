//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import {
  MAT_DATE_LOCALE,
  MatDialogModule,
  MatGridListModule,
  MatPaginatorModule,
  MatTableModule,
  MatTooltipModule
} from '@angular/material';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ChartsModule } from 'ng2-charts';
import { environment } from '../environments/environment';
import { AboutComponent } from './about/about.component';
import { AccountModule } from './account/account.module';
import { AuthInterceptor } from './api/interceptors/auth-interceptor';
import { BaseInterceptor } from './api/interceptors/base.interceptor';
import { appModules } from './app-modules.constant';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './auth/auth.service';
import { CompanyModule } from './company/company.module';
import { AccountProfileComponent } from './dashboard/account-profile/account-profile.component';
import { ActivityDialogComponent } from './dashboard/activities/activity-dialog/activity-dialog.component';
import { MyActivitiesComponent } from './dashboard/activities/my-activities/my-activities.component';
import { CompanyProfileComponent } from './dashboard/company-profile/company-profile.component';
import { ParticipantsComponent } from './dashboard/company-profile/participants/participants.component';
import { CompanyTeamComponent } from './dashboard/company-team/company-team.component';
import { DevicesComponent } from './dashboard/devices/devices.component';
import { CompanyParticipantsRankingComponent } from './dashboard/rankings/company-rankings/company-participants-ranking/company-participants-ranking.component';
import { CompanyRankingsComponent } from './dashboard/rankings/company-rankings/company-rankings.component';
import { CompanyUnitRankingComponent } from './dashboard/rankings/company-rankings/company-unit-ranking/company-unit-ranking.component';
import { GlobalCompanyRankingComponent } from './dashboard/rankings/company-rankings/global-company-ranking/global-company-ranking.component';
import { IndividualCompanyRankingComponent } from './dashboard/rankings/company-rankings/individual-company-ranking/individual-company-ranking.component';
import { CompanyTeamRankingComponent } from './dashboard/rankings/company-team-ranking/company-team-ranking.component';
import { IndividualRankingComponent } from './dashboard/rankings/individual-ranking/individual-ranking.component';
import { RankingComponent } from './dashboard/rankings/ranking/ranking.component';
import { TeamRankingComponent } from './dashboard/rankings/team-ranking/team-ranking.component';
import { ManageTeamComponent } from './dashboard/team/manage-team/manage-team.component';
import { TeamFormComponent } from './dashboard/team/team-form/team-form.component';
import { JoinLinkComponent } from './group/join-link/join-link.component';
import { LayoutComponent } from './layout/layout.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { SharedModule } from './shared/shared.module';
import { StatisticsComponent } from './statistics/statistics.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    SharedModule,
    ...appModules
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'nl' },
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
  }
}

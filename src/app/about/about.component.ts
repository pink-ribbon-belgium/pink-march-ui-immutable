import { Component } from '@angular/core';
import { ConfigService } from '../shared/services/config/config.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  config = window.env;

  constructor(public configService: ConfigService) {}
}

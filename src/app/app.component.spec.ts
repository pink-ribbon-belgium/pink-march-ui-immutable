//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';

import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { AccountPreferencesService } from './api/account-preferences/account-preferences.service';
import { AccountProfileService } from './api/account-profile/account-profile.service';
import { AccountService } from './api/account/account.service';
import { appModules } from './app-modules.constant';
import { AppComponent } from './app.component';
import { AuthService } from './auth/auth.service';
import { AuthState, initialAuthState } from './auth/store/state/auth.state';
import createSpy = jasmine.createSpy;
import { initialNavState, NavState } from './navigation/store/state/nav.state';
import { sharedModules } from './shared/constant/shared-modules';
import { Language } from './shared/enum/language';
import { KnownFromEnum } from './types/account/known-from-enum';

interface Fixture {
  router: Router;
  component: AppComponent;
  fixture: ComponentFixture<AppComponent>;
  translateService: TranslateService;
  authService: AuthService;
  accountPreferencesService: AccountPreferencesService;
  accountProfileService: AccountProfileService;
  store: MockStore<AuthState>;
  navStore: MockStore<NavState>;
  accountService: AccountService;
}

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [RouterTestingModule.withRoutes([]), BrowserAnimationsModule, ...appModules, ...sharedModules],
      providers: [
        provideMockStore({ initialState: { auth: initialAuthState, nav: initialNavState } }),
        {
          provide: AuthService,
          useValue: {
            userProfile$: of({}),
            getTokenSilently$: createSpy('getTokenSilently$')
          }
        }
      ]
    });
  });

  beforeEach(inject([TranslateService], function(this: Fixture): void {
    this.router = TestBed.get(Router);
    this.fixture = TestBed.createComponent(AppComponent);
    this.translateService = TestBed.get(TranslateService);
    this.component = this.fixture.componentInstance;
    this.authService = TestBed.get<AuthService>(AuthService);
    this.accountPreferencesService = TestBed.get<AccountPreferencesService>(AccountPreferencesService);
    this.accountProfileService = TestBed.get<AccountProfileService>(AccountProfileService);
    this.store = TestBed.get<Store<AuthState>>(Store);
    this.navStore = TestBed.get<Store<NavState>>(Store);
    this.accountService = TestBed.get<AccountService>(AccountService);
    spyOn(localStorage, 'setItem');
    spyOn(this.accountPreferencesService, 'getAccountPreferencesByAccountId').and.returnValue(
      of({
        accountId: 'test',
        language: Language.fr,
        verifiedEmail: { email: '', isVerified: false },
        newsletter: false,
        knownFromType: KnownFromEnum.FRIENDS,
        structureVersion: 2
      })
    );
    spyOn(this.accountService, 'getAdministratorOf').and.returnValue(
      of([
        {
          accountId: '72bd9328-3e3e-46da-93da-bff6a66d275d',
          groupId: 'group-id',
          deleted: false,
          structureVersion: 2,
          groupType: 'Team'
        }
      ])
    );
    spyOn(this.accountProfileService, 'getAccountProfileByAccountId');
  }));

  afterEach(function(this: Fixture): void {
    TestBed.resetTestingModule();
  });

  it('should compile', function(this: Fixture): void {
    this.fixture.detectChanges();
    expect(this.component).toBeTruthy();
  });
});

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { AccountProfile } from '../../../types/account/account-profile';
import { Slot } from '../../../types/slot/slot';
import {
  claimSlotSuccess,
  joinSubgroupSuccess,
  loadAdministrationSuccess,
  loadProfileFail,
  loadProfileSuccess,
  loadSlotFailed,
  loadSlotSuccess,
  paymentSuccess,
  saveProfileSuccess,
  saveSubgroupSuccess
} from '../actions/nav.action';

export interface NavState {
  profileLoaded: boolean;
  accountProfile: AccountProfile;
  isAdmin: boolean;
  groupId: string;
  subgroupId: string;
  groupType: string;
  slot: Slot;
  isMember: boolean;
  isRegistered: boolean;
  paymentCompleted: boolean;
}

export const initialState: NavState = {
  profileLoaded: false,
  accountProfile: undefined,
  isAdmin: false,
  groupId: undefined,
  subgroupId: undefined,
  groupType: undefined,
  slot: undefined,
  isMember: false,
  isRegistered: false,
  paymentCompleted: false
};

const _navReducer = createReducer(
  initialState,
  on(loadProfileSuccess, (state, { profileLoaded, accountProfile }) => ({ ...state, profileLoaded, accountProfile })),
  on(loadProfileFail, (state, { profileLoaded }) => ({ ...state, profileLoaded })),
  on(saveProfileSuccess, (state, { profileLoaded, accountProfile }) => ({ ...state, profileLoaded, accountProfile })),
  on(loadAdministrationSuccess, (state, { isAdmin, groupId, groupType }) => ({
    ...state,
    isAdmin,
    groupId,
    groupType
  })),
  on(loadSlotSuccess, (state, { slot, isMember, isRegistered, groupId, groupType, subgroupId }) => ({
    ...state,
    slot,
    isMember,
    isRegistered,
    groupId,
    groupType,
    subgroupId
  })),
  on(loadSlotFailed, (state, { isRegistered }) => ({ ...state, isRegistered })),
  on(claimSlotSuccess, (state, { groupId, groupType, isRegistered }) => ({
    ...state,
    groupId,
    groupType,
    isRegistered
  })),
  on(saveSubgroupSuccess, (state, { subgroupId }) => ({ ...state, subgroupId })),
  on(joinSubgroupSuccess, (state, { subgroupId }) => ({ ...state, subgroupId })),
  on(paymentSuccess, (state, { paymentCompleted }) => ({ ...state, paymentCompleted }))
);

// NOTE: tslint says we should not wrap, because it is not needed; and it says only to use arrow functions
//       angular build says we should: https://stackoverflow.com/questions/56993101/error-building-angularngrx-8-for-production-when-using-createreducer-function/56993402
//       both wrap and not use a function expression, but a regular function
// tslint:disable-next-line
export function navReducer(state, action) {
  return _navReducer(state, action);
}

export const selectNavState = createFeatureSelector<NavState>('nav');
export const selectProfileLoaded = createSelector(selectNavState, navState => navState.profileLoaded);
export const selectAccountId = createSelector(selectNavState, navState => navState.accountProfile.accountId);
export const selectAccountProfile = createSelector(selectNavState, navState => navState.accountProfile);
export const selectIsAdmin = createSelector(selectNavState, navState => navState.isAdmin);
export const selectGroupId = createSelector(selectNavState, navState => navState.groupId);
export const selectGroupType = createSelector(selectNavState, navState => navState.groupType);
export const selectIsMember = createSelector(selectNavState, navState => navState.isMember);
export const selectIsRegistered = createSelector(selectNavState, navState => navState.isRegistered);
export const selectSubgroupId = createSelector(selectNavState, navState => navState.subgroupId);
export const selectSlot = createSelector(selectNavState, navState => navState.slot);
export const selectPaymentCompleted = createSelector(selectNavState, navState => navState.paymentCompleted);

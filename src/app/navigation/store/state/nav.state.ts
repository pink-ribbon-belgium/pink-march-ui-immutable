//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { AccountProfile } from '../../../types/account/account-profile';
import { Slot } from '../../../types/slot/slot';

export interface NavState {
  profileLoaded: boolean;
  accountProfile: AccountProfile;
  isAdmin: boolean;
  groupId: string;
  subgroupId: string;
  groupType: string;
  slot: Slot;
  isMember: boolean;
  isRegistered: boolean;
  paymentCompleted: boolean;
}

export const initialNavState: NavState = {
  profileLoaded: false,
  accountProfile: undefined,
  isAdmin: false,
  groupId: undefined,
  subgroupId: undefined,
  groupType: undefined,
  slot: undefined,
  isMember: false,
  isRegistered: false,
  paymentCompleted: false
};

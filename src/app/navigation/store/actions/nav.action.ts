//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { createAction, props } from '@ngrx/store';
import { AccountProfile } from '../../../types/account/account-profile';
import { Slot } from '../../../types/slot/slot';

export enum NavActionTypes {
  LoadProfileSuccess = '[Nav list] Load Profile Success',
  LoadProfileFail = '[Nav list] Load Profile Fail',
  SaveProfileSuccess = '[Nav list] Save Profile Success',
  LoadAdministrationSuccess = '[Nav list] Load GroupAdministration Success',
  LoadSlotSuccess = '[Nav list] Load Slot Success',
  LoadSlotFail = '[Nav list] Load Slot Fail',
  ClaimSlotSuccess = '[Nav list] Claim Slot Success',
  SaveSubgroupSuccess = '[Nav list] Save Subgroup Success',
  JoinSubgroupSuccess = '[Nav list] Save Subgroup Success',
  PaymentSuccess = '[Nav list] Payment Success'
}

export const loadProfileSuccess = createAction(
  NavActionTypes.LoadProfileSuccess,
  props<{ profileLoaded: boolean; accountProfile: AccountProfile }>()
);

export const loadProfileFail = createAction(NavActionTypes.LoadProfileFail, props<{ profileLoaded: boolean }>());

export const saveProfileSuccess = createAction(
  NavActionTypes.SaveProfileSuccess,
  props<{ profileLoaded: boolean; accountProfile: AccountProfile }>()
);

export const loadAdministrationSuccess = createAction(
  NavActionTypes.LoadAdministrationSuccess,
  props<{ isAdmin: boolean; groupId: string; groupType: string }>()
);

export const loadSlotSuccess = createAction(
  NavActionTypes.LoadSlotSuccess,
  props<{
    slot: Slot;
    isMember: boolean;
    isRegistered: boolean;
    groupId: string;
    groupType: string;
    subgroupId: string;
  }>()
);

export const loadSlotFailed = createAction(NavActionTypes.LoadSlotFail, props<{ isRegistered: boolean }>());
export const claimSlotSuccess = createAction(
  NavActionTypes.ClaimSlotSuccess,
  props<{ groupId: string; groupType: string; isRegistered: boolean }>()
);

export const saveSubgroupSuccess = createAction(NavActionTypes.SaveSubgroupSuccess, props<{ subgroupId: string }>());
export const joinSubgroupSuccess = createAction(NavActionTypes.JoinSubgroupSuccess, props<{ subgroupId: string }>());
export const paymentSuccess = createAction(NavActionTypes.PaymentSuccess, props<{ paymentCompleted: boolean }>());

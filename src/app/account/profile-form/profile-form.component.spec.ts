import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AccountPreferencesService } from '../../api/account-preferences/account-preferences.service';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { sharedModules } from '../../shared/constant/shared-modules';
import { ProfileFormComponent } from './profile-form.component';

describe('ProfileFormComponent', () => {
  let component: ProfileFormComponent;
  let fixture: ComponentFixture<ProfileFormComponent>;

  interface Fixture {
    translateService: TranslateService;
    authService: AuthService;
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileFormComponent],
      imports: [...appModules, ...sharedModules],
      providers: [
        {
          provide: AuthService,
          useValue: {}
        }
      ]
    });
  });

  beforeEach(function(this: Fixture): void {
    fixture = TestBed.createComponent(ProfileFormComponent);
    component = fixture.componentInstance;
    this.translateService = TestBed.get(TranslateService);
    this.authService = TestBed.get(AuthService);
    component.formGroup = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      dateOfBirth: new FormControl('', Validators.required),
      zip: new FormControl('', Validators.required)
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

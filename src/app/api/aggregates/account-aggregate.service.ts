import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountAggregate } from '../../types/aggregates/account-aggregate';

@Injectable({
  providedIn: 'root'
})
export class AccountAggregateService {
  readonly baseUrl = '/I/accountaggregate/';

  constructor(private readonly http: HttpClient) {}

  getAccountAggregate(accountId): Observable<AccountAggregate> {
    return this.http.get<AccountAggregate>(`${this.baseUrl}${accountId}?doesnotexist=${new Date().getTime()}`);
  }

  putAccountAggregate(accountAggregate): Observable<AccountAggregate> {
    return this.http.put<AccountAggregate>(
      `${this.baseUrl}${accountAggregate.accountId}?doesnotexist=${new Date().getTime()}`,
      accountAggregate
    );
  }

  getAccountAggregatesForGroup(groupId): Observable<Array<AccountAggregate>> {
    return this.http.get<Array<AccountAggregate>>(`${this.baseUrl}group/${groupId}`);
  }

  getAccountAggregatesForSubgroup(subgroupId): Observable<Array<AccountAggregate>> {
    return this.http.get<Array<AccountAggregate>>(`${this.baseUrl}subgroup/${subgroupId}`);
  }
}

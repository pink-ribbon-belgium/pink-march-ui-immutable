import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TeamAggregate } from '../../types/aggregates/team-aggregate';

@Injectable({
  providedIn: 'root'
})
export class TeamAggregateService {
  readonly baseUrl = '/I/teamaggregate/';

  constructor(private readonly http: HttpClient) {}

  getTeamAggregate(teamAggregateId): Observable<TeamAggregate> {
    return this.http.get<TeamAggregate>(`${this.baseUrl}${teamAggregateId}`);
  }

  getAllTeamAggregates(): Observable<Array<TeamAggregate>> {
    return this.http.get<Array<TeamAggregate>>(`${this.baseUrl}all`);
  }

  getTeamAggregatesForSubgroups(companyId): Observable<Array<TeamAggregate>> {
    return this.http.get<Array<TeamAggregate>>(`${this.baseUrl}company/${companyId}`);
  }
}

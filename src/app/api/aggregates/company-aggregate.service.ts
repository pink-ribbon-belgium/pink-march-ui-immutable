import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CompanyAggregate } from '../../types/aggregates/company-aggregate';

@Injectable({
  providedIn: 'root'
})
export class CompanyAggregateService {
  readonly baseUrl = '/I/companyaggregate/';

  constructor(private readonly http: HttpClient) {}

  getCompanyAggregate(companyId): Observable<CompanyAggregate> {
    return this.http.get<CompanyAggregate>(`${this.baseUrl}${companyId}`);
  }

  getAllCompanyAggregates(): Observable<Array<CompanyAggregate>> {
    return this.http.get<Array<CompanyAggregate>>(`${this.baseUrl}all`);
  }

  getCompanyAggregatesForUnit(unit): Observable<Array<CompanyAggregate>> {
    return this.http.get<Array<CompanyAggregate>>(`${this.baseUrl}unit/${unit}`);
  }
}

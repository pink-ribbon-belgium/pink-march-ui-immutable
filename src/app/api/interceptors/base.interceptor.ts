import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

let baseUrl: string;

@Injectable()
export class BaseInterceptor implements HttpInterceptor {
  constructor() {
    baseUrl = `/${window.env.apiStage}`;
  }

  intercept(req: HttpRequest<Object>, next: HttpHandler): Observable<HttpEvent<Object>> {
    let transformedReq = req;
    if (req.url.startsWith('/I/')) {
      transformedReq = req.clone({ url: baseUrl + req.url });
      if (transformedReq.body) {
        Object.keys(transformedReq.body).forEach(k => {
          if (transformedReq.body[k] === null) {
            transformedReq.body[k] = undefined;
          }
        });
      }
    }

    return next.handle(transformedReq);
  }
}

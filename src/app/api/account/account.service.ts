//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from '../../types/account/account';
import { Activity } from '../../types/activity/activity';
import { CalculationParams } from '../../types/activity/calculation-params';
import { CalculationResult } from '../../types/activity/calculation-result';
import { GroupAdministration } from '../../types/group/group-administration';
import { Slot } from '../../types/slot/slot';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  readonly baseUrl = '/I/account/';

  constructor(private readonly http: HttpClient) {}

  getAccountById(accountId: string): Observable<Account> {
    return this.http.get<Account>(`${this.baseUrl}${accountId}`);
  }

  saveAccount(account: Account): Observable<Account> {
    return this.http.put<Account>(`${this.baseUrl}${account.id}`, account);
  }

  getAdministratorOf(accountId): Observable<Array<GroupAdministration>> {
    return this.http.get<Array<GroupAdministration>>(`${this.baseUrl}${accountId}/administratorOf`);
  }

  uniqueGetSlot(accountId): Observable<Slot> {
    return this.http.get<any>(`${this.baseUrl}${accountId}/slot?doesnotexist=${new Date().getTime()}`);
  }

  putSlot(slot): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}${slot.accountId}/slot`, slot);
  }

  getActivities(accountId): Observable<Array<Activity>> {
    return this.http.get<Array<Activity>>(
      `${this.baseUrl}${accountId}/activities?doesnotexist=${new Date().getTime()}`
    );
  }

  updateActivity(accountId: string, activity: Activity): Observable<Activity> {
    return this.http.put<Activity>(
      `${this.baseUrl}${accountId}/activity/${activity.activityDate}?doesnotexist=${new Date().getTime()}`,
      activity
    );
  }

  calculateDistance(params: CalculationParams): Observable<CalculationResult> {
    return this.http.post<CalculationResult>(`/I/activity/calculation`, params);
  }
}

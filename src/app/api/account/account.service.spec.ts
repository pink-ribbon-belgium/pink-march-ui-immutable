//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { TestBed } from '@angular/core/testing';

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SubSink } from 'subsink';
import { Account } from '../../types/account/account';
import { GroupAdministration } from '../../types/group/group-administration';
import { AccountService } from './account.service';

describe('AccountService', () => {
  let httpTestingController: HttpTestingController;
  let accountService: AccountService;
  let subscriptions: SubSink;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AccountService]
    });

    subscriptions = new SubSink();
    httpTestingController = TestBed.get(HttpTestingController);
    accountService = TestBed.get(AccountService);
  });

  let account: Account;
  let groupAdministrations: Array<GroupAdministration>;

  beforeEach(() => {
    account = {
      id: 'id-OF-THIS-account',
      email: 'email@example.com',
      structureVersion: 1
    };

    groupAdministrations = [
      {
        accountId: '72bd9328-3e3e-46da-93da-bff6a66d275d',
        groupId: '72bd9328-3e3e-46da-93da-bff6a66d275d',
        deleted: false,
        structureVersion: 2,
        groupType: 'Team'
      }
    ];
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(accountService).toBeTruthy();
  });

  it('should return expected account', () => {
    const requestUrl = `${accountService.baseUrl}${account.id}`;

    subscriptions.add(
      accountService
        .getAccountById(account.id)
        .subscribe(profile => expect(profile).toEqual(account, 'should return expected account'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(account);
  });

  it('should update an account and return it', () => {
    const requestUrl = `${accountService.baseUrl}${account.id}`;

    subscriptions.add(
      accountService
        .saveAccount(account)
        .subscribe(data => expect(data).toEqual(account, 'should return the account'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(account);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: account });
    req.event(expectedResponse);
  });

  it('should return array of groupAdministration', () => {
    const requestUrl = `${accountService.baseUrl}${account.id}/administratorOf`;

    subscriptions.add(
      accountService
        .getAdministratorOf(account.id)
        .subscribe(administrations =>
          expect(administrations).toEqual(groupAdministrations, 'should return expected groupAdministrations')
        )
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(groupAdministrations);
  });
});

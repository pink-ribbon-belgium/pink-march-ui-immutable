//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Company } from '../../types/company/company';
import { Subgroup } from '../../types/company/subgroup';
import { GroupAdministration } from '../../types/group/group-administration';
import { GroupProfile } from '../../types/group/group-profile';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  readonly baseUrl = '/I/company';

  constructor(private readonly http: HttpClient) {}

  getCompanyById(companyId: string): Observable<Company> {
    return this.http.get<Company>(`${this.baseUrl}/${companyId}`);
  }

  updateCompany(company: Company): Observable<Company> {
    return this.http.put<Company>(`${this.baseUrl}/${company.id}`, company);
  }

  createCompany(company: Company): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}`, company);
  }

  getCompanyProfileById(companyId: string): Observable<GroupProfile> {
    return this.http.get<GroupProfile>(`${this.baseUrl}/${companyId}/publicProfile`);
  }

  saveCompanyProfile(groupProfile: GroupProfile): Observable<GroupProfile> {
    return this.http.put<GroupProfile>(`${this.baseUrl}/${groupProfile.id}/publicProfile`, groupProfile);
  }

  saveGroupAdministration(groupAdministration: GroupAdministration): Observable<GroupAdministration> {
    return this.http.put<GroupAdministration>(
      `${this.baseUrl}/${groupAdministration.groupId}/administrators/${groupAdministration.accountId}`,
      groupAdministration
    );
  }

  getCompanyTeams(companyId: string): Observable<Array<Subgroup>> {
    return this.http.get<Array<Subgroup>>(`${this.baseUrl}/${companyId}/subgroups`);
  }

  claimSlot(teamId: string, accountId: string): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/${teamId}/claimSlot/${accountId}`, null);
  }

  deleteCompanyMember(companyId, accountId): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/${companyId}/slot/${accountId}`, null);
  }
}

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { TestBed } from '@angular/core/testing';

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SubSink } from 'subsink';
import { generalConstant } from '../../shared/constant/general.constants';
import { Company } from '../../types/company/company';
import { GroupAdministration } from '../../types/group/group-administration';
import { GroupProfile } from '../../types/group/group-profile';
import { CompanyService } from './company.service';

describe('CompanyService', () => {
  let httpTestingController: HttpTestingController;
  let companyService: CompanyService;
  let subscriptions: SubSink;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CompanyService]
    });

    subscriptions = new SubSink();
    httpTestingController = TestBed.get(HttpTestingController);
    companyService = TestBed.get(CompanyService);
  });

  let company: Company;
  let companyProfile: GroupProfile;
  let groupAdministration: GroupAdministration;

  beforeEach(() => {
    company = {
      id: '72bd9328-3e3e-46da-93da-bff6a66d275d',
      address: 'this is an address 12',
      zip: '2500',
      city: 'Lier',
      vat: 'this-is-a-vat',
      logo: '',
      contactFirstName: 'Jos',
      contactLastName: 'Pros',
      contactEmail: 'email@example.com',
      contactTelephone: '+32498765432',
      code: 'CODE',
      groupType: generalConstant.companyGroupType,
      structureVersion: 1
    };

    companyProfile = {
      id: '72bd9328-3e3e-46da-93da-bff6a66d275d',
      name: 'My Company',
      structureVersion: 1
    };

    groupAdministration = {
      accountId: '72bd9328-3e3e-46da-93da-bff6a66d275d',
      groupId: '72bd9328-3e3e-46da-93da-bff6a66d275d',
      deleted: false,
      structureVersion: 2,
      groupType: 'Company'
    };
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(companyService).toBeTruthy();
  });

  it('should return expected company', () => {
    const requestUrl = `${companyService.baseUrl}/${company.id}`;

    subscriptions.add(
      companyService
        .getCompanyById(company.id)
        .subscribe(com => expect(com).toEqual(company, 'should return expected company'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(company);
  });

  it('should update a company and return it', () => {
    const requestUrl = `${companyService.baseUrl}/${company.id}`;

    subscriptions.add(
      companyService
        .updateCompany(company)
        .subscribe(data => expect(data).toEqual(company, 'should return the company'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(company);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: company });
    req.event(expectedResponse);
  });

  it('should post a company', () => {
    const requestUrl = `${companyService.baseUrl}`;

    subscriptions.add(
      companyService
        .createCompany(company)
        .subscribe(data => expect(data).toEqual(company, 'should return the companyId'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(company);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: company });
    req.event(expectedResponse);
  });

  it('should return expected companyProfile', () => {
    const requestUrl = `${companyService.baseUrl}/${company.id}/publicProfile`;

    subscriptions.add(
      companyService.getCompanyProfileById(company.id).subscribe(comProfile => {
        expect(comProfile).toEqual(companyProfile, 'should return expected companyProfile');
      }, fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(companyProfile);
  });

  it('should update a companyProfile and return it', () => {
    const requestUrl = `${companyService.baseUrl}/${companyProfile.id}/publicProfile`;

    subscriptions.add(
      companyService
        .saveCompanyProfile(companyProfile)
        .subscribe(data => expect(data).toEqual(companyProfile, 'should return the companyProfile'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(companyProfile);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: companyProfile });
    req.event(expectedResponse);
  });

  it('should update a groupAdministration and return it', () => {
    const requestUrl = `${companyService.baseUrl}/${groupAdministration.groupId}/administrators/${groupAdministration.accountId}`;

    subscriptions.add(
      companyService
        .saveGroupAdministration(groupAdministration)
        .subscribe(data => expect(data).toEqual(groupAdministration, 'should return the groupAdministration'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(groupAdministration);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: groupAdministration });
    req.event(expectedResponse);
  });

  it('should claim a slot', () => {
    const requestUrl = `${companyService.baseUrl}/${company.id}/claimSlot/${groupAdministration.accountId}`;

    subscriptions.add(companyService.claimSlot(company.id, groupAdministration.accountId).subscribe());

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(null);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: null });
    req.event(expectedResponse);
  });
});

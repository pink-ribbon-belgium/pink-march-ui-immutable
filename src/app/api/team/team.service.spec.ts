//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { TestBed } from '@angular/core/testing';

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SubSink } from 'subsink';
import { generalConstant } from '../../shared/constant/general.constants';
import { GroupAdministration } from '../../types/group/group-administration';
import { GroupProfile } from '../../types/group/group-profile';
import { Team } from '../../types/team/team';
import { TeamService } from './team.service';

describe('TeamService', () => {
  let httpTestingController: HttpTestingController;
  let teamService: TeamService;
  let subscriptions: SubSink;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TeamService]
    });

    subscriptions = new SubSink();
    httpTestingController = TestBed.get(HttpTestingController);
    teamService = TestBed.get(TeamService);
  });

  let team: Team;
  let teamProfile: GroupProfile;
  let groupAdministration: GroupAdministration;

  beforeEach(() => {
    team = {
      id: 'eb82a9cc-68d6-400e-9c85-c2302d7e7207',
      code: 'FKFG-KDKK-KSKK-LWSK',
      groupType: generalConstant.teamGroupType,
      structureVersion: 1
    };

    teamProfile = {
      id: '72bd9328-3e3e-46da-93da-bff6a66d275d',
      name: 'My Company',
      structureVersion: 1
    };

    groupAdministration = {
      accountId: '72bd9328-3e3e-46da-93da-bff6a66d275d',
      groupId: '72bd9328-3e3e-46da-93da-bff6a66d275d',
      deleted: false,
      structureVersion: 2,
      groupType: 'Team'
    };
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(teamService).toBeTruthy();
  });

  it('should return expected team', () => {
    const requestUrl = `${teamService.baseUrl}/${team.id}`;

    subscriptions.add(
      teamService
        .getTeamById(team.id)
        .subscribe(data => expect(data).toEqual(team, 'should return expected team'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(team);
  });

  it('should update a team and return it', () => {
    const requestUrl = `${teamService.baseUrl}/${team.id}`;

    subscriptions.add(
      teamService.updateTeam(team).subscribe(data => expect(data).toEqual(team, 'should return the team'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(team);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: team });
    req.event(expectedResponse);
  });

  it('should post a team and return teamId', () => {
    const requestUrl = `${teamService.baseUrl}`;

    subscriptions.add(
      teamService.saveTeam(team).subscribe(data => expect(data).toEqual(team, 'should return the teamId'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(team);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: team });
    req.event(expectedResponse);
  });

  it('should return expected teamProfile', () => {
    const requestUrl = `${teamService.baseUrl}/${team.id}/publicProfile`;

    subscriptions.add(
      teamService
        .getTeamProfileById(team.id)
        .subscribe(tProfile => expect(tProfile).toEqual(teamProfile, 'should return expected teamProfile'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(teamProfile);
  });

  it('should update a teamProfile and return it', () => {
    const requestUrl = `${teamService.baseUrl}/${teamProfile.id}/publicProfile`;

    subscriptions.add(
      teamService
        .saveTeamProfile(teamProfile)
        .subscribe(data => expect(data).toEqual(teamProfile, 'should return the teamProfile'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(teamProfile);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: teamProfile });
    req.event(expectedResponse);
  });

  it('should update a groupAdministration and return it', () => {
    const requestUrl = `${teamService.baseUrl}/${groupAdministration.groupId}/administrators/${groupAdministration.accountId}`;

    subscriptions.add(
      teamService
        .saveGroupAdministration(groupAdministration)
        .subscribe(data => expect(data).toEqual(groupAdministration, 'should return the groupAdministration'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(groupAdministration);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: groupAdministration });
    req.event(expectedResponse);
  });

  it('should claim a slot', () => {
    const requestUrl = `${teamService.baseUrl}/${team.id}/claimSlot/${groupAdministration.accountId}`;

    subscriptions.add(teamService.claimSlot(team.id, groupAdministration.accountId).subscribe());

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(null);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: null });
    req.event(expectedResponse);
  });
});

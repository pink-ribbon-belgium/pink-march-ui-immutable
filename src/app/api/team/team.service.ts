//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GroupAdministration } from '../../types/group/group-administration';
import { GroupProfile } from '../../types/group/group-profile';
import { Team } from '../../types/team/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  readonly baseUrl = '/I/team';

  constructor(private readonly http: HttpClient) {}

  getTeamById(teamId: string): Observable<Team> {
    return this.http.get<Team>(`${this.baseUrl}/${teamId}`);
  }

  updateTeam(team: Team): Observable<Team> {
    return this.http.put<Team>(`${this.baseUrl}/${team.id}`, team);
  }

  saveTeam(team: Team): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}`, team);
  }

  getTeamProfileById(teamId: string): Observable<GroupProfile> {
    return this.http.get<GroupProfile>(`${this.baseUrl}/${teamId}/publicProfile`);
  }

  saveTeamProfile(teamProfile: GroupProfile): Observable<GroupProfile> {
    return this.http.put<GroupProfile>(`${this.baseUrl}/${teamProfile.id}/publicProfile`, teamProfile);
  }

  saveGroupAdministration(groupAdministration: GroupAdministration): Observable<GroupAdministration> {
    return this.http.put<GroupAdministration>(
      `${this.baseUrl}/${groupAdministration.groupId}/administrators/${groupAdministration.accountId}`,
      groupAdministration
    );
  }

  claimSlot(teamId: string, accountId: string): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/${teamId}/claimSlot/${accountId}`, null);
  }

  getMembers(groupId): Observable<any> {
    return this.http.get<any>(`/I/group/${groupId}/members`);
  }

  deleteTeamMember(teamId, accountId): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/${teamId}/slot/${accountId}`, null);
  }
}

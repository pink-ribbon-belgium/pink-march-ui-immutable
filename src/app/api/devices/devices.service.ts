import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TrackerConnection } from '../../types/account/tracker-connection';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {
  readonly baseUrl = '/I/account';

  constructor(private readonly http: HttpClient) {}

  putTrackerConnection(accountId: string, connectionData: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${accountId}/trackerConnection`, connectionData);
  }

  getTrackerConnection(accountId: string): Observable<TrackerConnection> {
    return this.http.get<TrackerConnection>(`${this.baseUrl}/${accountId}/trackerConnection`);
  }

  deleteTrackerConnection(accountId: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${accountId}/trackerConnection`);
  }
}

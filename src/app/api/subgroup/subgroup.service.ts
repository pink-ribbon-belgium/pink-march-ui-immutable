import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subgroup } from '../../types/company/subgroup';

@Injectable({
  providedIn: 'root'
})
export class SubgroupService {
  readonly baseUrl = '/I/subgroup';

  constructor(private readonly http: HttpClient) {}

  getSubgroupById(companyId: string, subgroupId: string): Observable<Subgroup> {
    return this.http.get<Subgroup>(`${this.baseUrl}/${subgroupId}/company/${companyId}`);
  }

  saveSubgroup(subgroup: Subgroup, companyId: string): Observable<Subgroup> {
    return this.http.put<Subgroup>(`${this.baseUrl}/${subgroup.id}/company/${companyId}`, subgroup);
  }

  createSubgroup(subgroup): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}`, subgroup);
  }

  getSubgroupMembers(companyId, subgroupId): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/${subgroupId}/company/${companyId}/members`);
  }

  deleteSubgroupMember(companyId, subgroupId, accountId): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/${subgroupId}/company/${companyId}/slot/${accountId}`, null);
  }

  deleteSubgroup(companyId, subgroupId): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/${subgroupId}/company/${companyId}`);
  }
}

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Language } from '../../shared/enum/language';
import { AccountPreferences } from '../../types/account/account-preferences';

@Injectable({
  providedIn: 'root'
})
export class AccountPreferencesService {
  readonly baseUrl = '/I/account/';

  constructor(private readonly http: HttpClient) {}

  getAccountPreferencesByAccountId(accountId: string): Observable<AccountPreferences> {
    return this.http.get<AccountPreferences>(`${this.baseUrl}${accountId}/preferences`);
  }

  saveAccountPreferences(accountPreferences: AccountPreferences, accountId: string): Observable<AccountPreferences> {
    return this.http.put<AccountPreferences>(`${this.baseUrl}${accountId}/preferences`, accountPreferences);
  }
}

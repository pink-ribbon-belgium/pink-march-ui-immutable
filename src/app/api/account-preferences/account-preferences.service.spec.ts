//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SubSink } from 'subsink';
import { Language } from '../../shared/enum/language';
import { AccountPreferences } from '../../types/account/account-preferences';
import { KnownFromEnum } from '../../types/account/known-from-enum';
import { AccountPreferencesService } from './account-preferences.service';

describe('AccountPreferencesService', () => {
  let httpTestingController: HttpTestingController;
  let accountPreferencesService: AccountPreferencesService;
  let subscriptions: SubSink;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AccountPreferencesService]
    });

    subscriptions = new SubSink();
    httpTestingController = TestBed.get(HttpTestingController);
    accountPreferencesService = TestBed.get(AccountPreferencesService);
  });

  let accountPreferences: AccountPreferences;

  beforeEach(() => {
    accountPreferences = {
      accountId: 'preference-id',
      language: Language.nl,
      structureVersion: 2,
      verifiedEmail: {
        email: 'hola@chicka.esp',
        isVerified: false
      },
      newsletter: false,
      knownFromType: KnownFromEnum.FRIENDS
    };
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(accountPreferencesService).toBeTruthy();
  });

  it('should return expected accountPreferences', () => {
    const requestUrl = `${accountPreferencesService.baseUrl}${accountPreferences.accountId}/preferences`;

    subscriptions.add(
      accountPreferencesService
        .getAccountPreferencesByAccountId(accountPreferences.accountId)
        .subscribe(profile => expect(profile).toEqual(accountPreferences, 'should return expected preferences'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(accountPreferences);
  });

  it('should update an accountProfile and return it', () => {
    const requestUrl = `${accountPreferencesService.baseUrl}${accountPreferences.accountId}/preferences`;

    subscriptions.add(
      accountPreferencesService
        .saveAccountPreferences(accountPreferences, accountPreferences.accountId)
        .subscribe(data => expect(data).toEqual(accountPreferences, 'should return the preferences'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(accountPreferences);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: accountPreferences });
    req.event(expectedResponse);
  });
});

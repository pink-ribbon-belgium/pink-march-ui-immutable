//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GroupProfile } from '../../../types/group/group-profile';

@Injectable({
  providedIn: 'root'
})
export class GroupProfileService {
  readonly baseUrl = '/I/group/';

  constructor(private readonly http: HttpClient) {}

  getGroupProfileById(groupId: string): Observable<GroupProfile> {
    return this.http.get<GroupProfile>(`${this.baseUrl}${groupId}/publicProfile`);
  }

  saveGroupProfile(groupProfile: GroupProfile): Observable<GroupProfile> {
    return this.http.put<GroupProfile>(`${this.baseUrl}${groupProfile.id}/publicProfile`, groupProfile);
  }
}

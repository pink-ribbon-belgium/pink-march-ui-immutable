//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { TestBed } from '@angular/core/testing';

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SubSink } from 'subsink';
import { GroupProfile } from '../../../types/group/group-profile';
import { GroupProfileService } from './group-profile.service';

describe('GroupProfileService', () => {
  let httpTestingController: HttpTestingController;
  let groupProfileService: GroupProfileService;
  let subscriptions: SubSink;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GroupProfileService]
    });

    subscriptions = new SubSink();
    httpTestingController = TestBed.get(HttpTestingController);
    groupProfileService = TestBed.get(GroupProfileService);
  });

  let groupProfile: GroupProfile;

  beforeEach(() => {
    groupProfile = {
      id: '38c5c784-f601-4328-a900-66666c56fd6a',
      name: 'test-groupProfile',
      structureVersion: 1
    };
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(groupProfileService).toBeTruthy();
  });

  it('should return expected company', () => {
    const requestUrl = `${groupProfileService.baseUrl}${groupProfile.id}/publicProfile`;

    subscriptions.add(
      groupProfileService
        .getGroupProfileById(groupProfile.id)
        .subscribe(profile => expect(profile).toEqual(groupProfile, 'should return expected groupProfile'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(groupProfile);
  });

  it('should update a groupProfile and return it', () => {
    const requestUrl = `${groupProfileService.baseUrl}${groupProfile.id}/publicProfile`;

    subscriptions.add(
      groupProfileService
        .saveGroupProfile(groupProfile)
        .subscribe(data => expect(data).toEqual(groupProfile, 'should return the groupProfile'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(groupProfile);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: groupProfile });
    req.event(expectedResponse);
  });
});

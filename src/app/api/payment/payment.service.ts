//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../../shared/services/config/config.service';
import { CalculationParams } from '../../types/payment/calculation-params';
import { CalculationResult } from '../../types/payment/calculation-result';
import { Payment } from '../../types/payment/payment';
import { PaymentDetails } from '../../types/payment/payment-details';
import { PaymentSuccess } from '../../types/payment/payment-success';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  readonly baseUrl = '/I/payment/';

  constructor(private readonly http: HttpClient, private readonly config: ConfigService) {}

  initializePayment(paymentDetails: PaymentDetails): Observable<any> {
    return this.http.post<any>(`/I/${paymentDetails.groupType.toLowerCase()}/${paymentDetails.groupId}/payment`, {
      ...paymentDetails,
      redirectUrl: this.config.ogoneRedirectUrl
    });
  }

  processPayment(profileType: string, groupId: string, accountId: string, orderId: string): Observable<PaymentSuccess> {
    return this.http.put<PaymentSuccess>(
      `/I/${profileType.toLowerCase()}/${groupId}/payment/${orderId}/process/${accountId}`,
      null
    );
  }

  calculatePayment(calculationParams: CalculationParams): Observable<CalculationResult> {
    return this.http.post<CalculationResult>(
      `${this.baseUrl}calculation?doesnotexist=${new Date().getTime()}`,
      calculationParams
    );
  }

  getPaymentsForGroup(groupType: string, groupId: string): Observable<Array<Payment>> {
    return this.http.get<Array<Payment>>(`/I/${groupType}/${groupId}/payments`);
  }
}

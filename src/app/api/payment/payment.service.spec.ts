//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { TestBed } from '@angular/core/testing';

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SubSink } from 'subsink';
import { generalConstant } from '../../shared/constant/general.constants';
import { ConfigService } from '../../shared/services/config/config.service';
import { CalculationParams } from '../../types/payment/calculation-params';
import { CalculationResult } from '../../types/payment/calculation-result';
import { Payment } from '../../types/payment/payment';
import { PaymentDetails } from '../../types/payment/payment-details';
import { PaymentSuccess } from '../../types/payment/payment-success';
import { PaymentService } from './payment.service';

describe('PaymentService', () => {
  let paymentService: PaymentService;
  let httpTestingController: HttpTestingController;
  let subscriptions: SubSink;
  let config: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PaymentService]
    });

    subscriptions = new SubSink();
    paymentService = TestBed.get(PaymentService);
    httpTestingController = TestBed.get(HttpTestingController);
    config = TestBed.get<ConfigService>(ConfigService);
  });

  let payment: Payment;
  let calculationParams: CalculationParams;
  let calculationResult: CalculationResult;
  let paymentDetails: PaymentDetails;
  let accountId: string;
  let paymentSuccess: PaymentSuccess;

  beforeEach(() => {
    payment = {
      paymentId: '72bd9328-3e3e-46da-93da-bff6a66d275d',
      paymentInitiatedAt: '2020-02-02T15:00:39.212Z',
      groupId: 'e88a6e1a-87c5-42fc-a804-2211d6bed387',
      groupType: 'Company',
      voucherCode: 'PV2020',
      numberOfSlots: 15,
      amount: 46.25,
      paymentStatus: 'processing',
      structureVersion: 1
    };

    calculationParams = {
      numberOfParticipants: 5,
      groupId: '96ee57d1-8c49-40c3-b742-d8344a55d77b',
      voucherCode: 'DISCOUNT'
    };

    calculationResult = {
      unitPrice: 6,
      numberOfParticipants: 5,
      subTotal: 30,
      discount: 5,
      total: 25,
      voucherIsValid: true
    };

    paymentDetails = {
      groupId: '7e28fbe6-45fe-4b93-8ce5-18963b12bc44',
      locale: 'nl-BE',
      numberOfSlots: 20,
      voucherCode: 'PPW20',
      groupType: generalConstant.teamGroupType,
      structureVersion: 1
    };

    paymentSuccess = {
      joinLink: 'join/team/3425',
      freeSlots: 5
    };

    accountId = '72bd9328-3e3e-46da-93da-bff6a66d275d';
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(paymentService).toBeTruthy();
  });

  it('should put a paymentDetails and receive an url', () => {
    const requestUrl = `/I/${paymentDetails.groupType.toLowerCase()}/${paymentDetails.groupId}/payment`;

    subscriptions.add(
      paymentService
        .initializePayment(paymentDetails)
        .subscribe(data => expect(data).toEqual('https://secure.ogone.com/Ncol/Test/', 'should return url'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual({ ...paymentDetails, redirectUrl: config.ogoneRedirectUrl });

    const expectedResponse = new HttpResponse({
      status: 200,
      statusText: 'OK',
      body: 'https://secure.ogone.com/Ncol/Test/'
    });
    req.event(expectedResponse);
  });

  it('should retrieve a paymentSuccess', () => {
    const requestUrl = `/I/${paymentDetails.groupType.toLowerCase()}/${paymentDetails.groupId}/payment/${
      payment.paymentId
    }/process/${accountId}`;

    subscriptions.add(
      paymentService
        .processPayment(paymentDetails.groupType, paymentDetails.groupId, accountId, payment.paymentId)
        .subscribe(data => expect(data).toEqual(paymentSuccess, 'should return paymentSuccess'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
  });

  // it('should post a calculateParams and receive calculationResult', () => {
  //   const requestUrl = `${paymentService.baseUrl}calculation?doesnotexist=${new Date().getTime()}`;
  //
  //   subscriptions.add(
  //     paymentService
  //       .calculatePayment(calculationParams)
  //       .subscribe(data => expect(data).toEqual(calculationResult, 'should return calculationResult'), fail)
  //   );
  //
  //   const req = httpTestingController.expectOne(requestUrl);
  //   expect(req.request.method).toEqual('POST');
  //   expect(req.request.body).toEqual(calculationParams);
  //
  //   const expectedResponse = new HttpResponse({
  //     status: 200,
  //     statusText: 'OK',
  //     body: calculationResult
  //   });
  //   req.event(expectedResponse);
  // });

  it('should return expected payment', () => {
    const requestUrl = `/I/company/${payment.groupId}/payments`;

    subscriptions.add(
      paymentService
        .getPaymentsForGroup('company', payment.groupId)
        .subscribe(pay => expect(pay).toEqual([payment], 'should return expected payment array'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush([payment]);
  });
});

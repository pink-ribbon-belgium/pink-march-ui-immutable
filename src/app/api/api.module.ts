//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { NgModule } from '@angular/core';
import { AccountPreferencesService } from './account-preferences/account-preferences.service';
import { AccountProfileService } from './account-profile/account-profile.service';
import { AccountService } from './account/account.service';
import { AccountAggregateService } from './aggregates/account-aggregate.service';
import { TeamAggregateService } from './aggregates/team-aggregate.service';
import { CompanyService } from './company/company.service';
import { GroupProfileService } from './group/profile/group-profile.service';
import { SubgroupService } from './subgroup/subgroup.service';

@NgModule({
  providers: [
    AccountService,
    AccountPreferencesService,
    AccountProfileService,
    CompanyService,
    GroupProfileService,
    SubgroupService,
    AccountAggregateService,
    TeamAggregateService
  ]
})
export class ApiModule {}

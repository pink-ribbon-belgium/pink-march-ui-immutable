//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SubSink } from 'subsink';
import { AccountProfile } from '../../types/account/account-profile';
import { AccountProfileService } from './account-profile.service';

describe('AccountProfileService', () => {
  let httpTestingController: HttpTestingController;
  let accountProfileService: AccountProfileService;
  let subscriptions: SubSink;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AccountProfileService]
    });

    subscriptions = new SubSink();
    httpTestingController = TestBed.get(HttpTestingController);
    accountProfileService = TestBed.get(AccountProfileService);
  });

  let accountProfile: AccountProfile;

  beforeEach(() => {
    accountProfile = {
      accountId: 'id-OF-Eric',
      firstName: 'Eric',
      lastName: 'DoeRustig',
      gender: 'M',
      dateOfBirth: '1984-01-23T15:22:39.212Z',
      zip: '3600',
      structureVersion: 1
    };
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(accountProfileService).toBeTruthy();
  });

  it('should return expected accountProfile', () => {
    const requestUrl = `${accountProfileService.baseUrl}${accountProfile.accountId}/publicProfile`;

    subscriptions.add(
      accountProfileService
        .getAccountProfileByAccountId(accountProfile.accountId)
        .subscribe(profile => expect(profile).toEqual(accountProfile, 'should return expected profile'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(accountProfile);
  });

  it('should update an accountProfile and return it', () => {
    const requestUrl = `${accountProfileService.baseUrl}${accountProfile.accountId}/publicProfile`;

    subscriptions.add(
      accountProfileService
        .saveAccountProfile(accountProfile)
        .subscribe(data => expect(data).toEqual(accountProfile, 'should return the profile'), fail)
    );

    const req = httpTestingController.expectOne(requestUrl);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(accountProfile);

    const expectedResponse = new HttpResponse({ status: 200, statusText: 'OK', body: accountProfile });
    req.event(expectedResponse);
  });
});

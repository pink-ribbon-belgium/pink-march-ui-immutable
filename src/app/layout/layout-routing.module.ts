//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from '../about/about.component';
import { AuthGuard } from '../auth/auth.guard';
import { AccountProfileComponent } from '../dashboard/account-profile/account-profile.component';
import { MyActivitiesComponent } from '../dashboard/activities/my-activities/my-activities.component';
import { CompanyProfileComponent } from '../dashboard/company-profile/company-profile.component';
import { CompanyTeamOverviewComponent } from '../dashboard/company-team-overview/company-team-overview.component';
import { CompanyTeamComponent } from '../dashboard/company-team/company-team.component';
import { DevicesComponent } from '../dashboard/devices/devices.component';
import { CompanyRankingsComponent } from '../dashboard/rankings/company-rankings/company-rankings.component';
import { RankingComponent } from '../dashboard/rankings/ranking/ranking.component';
import { ManageTeamComponent } from '../dashboard/team/manage-team/manage-team.component';
import { GroupOverviewComponent } from '../group/group-overview/group-overview.component';
import { PreferencesComponent } from '../preferences/preferences.component';
import { LayoutComponent } from './layout.component';

const layoutRoutes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'register',
        canActivate: [AuthGuard],
        loadChildren: () => import('../register/register.module').then(m => m.RegisterModule)
      },
      {
        path: 'join/unit/:unitCode',
        redirectTo: 'register/company-profile/:unitCode'
      },
      {
        path: 'join/:groupType/:groupId',
        redirectTo: 'register/personal-profile/:groupType/:groupId'
      },
      {
        path: 'about',
        component: AboutComponent,
        pathMatch: 'full'
      },
      {
        path: 'preferences',
        canActivate: [AuthGuard],
        component: PreferencesComponent,
        pathMatch: 'full'
      },
      {
        path: 'accountProfile/:accountId',
        canActivate: [AuthGuard],
        component: AccountProfileComponent,
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        children: [
          {
            path: 'company/:companyId',
            component: CompanyProfileComponent,
            pathMatch: 'full'
          },
          {
            path: 'team/:teamId',
            component: ManageTeamComponent,
            pathMatch: 'full'
          },
          {
            path: 'company/:groupId/company-team',
            component: CompanyTeamComponent,
            pathMatch: 'full'
          },
          {
            path: 'company/:groupId/company-team/:subgroupId',
            component: CompanyTeamComponent,
            pathMatch: 'full'
          },
          {
            path: 'devices',
            component: DevicesComponent,
            pathMatch: 'full'
          },
          {
            path: 'activities',
            component: MyActivitiesComponent,
            pathMatch: 'full'
          },
          {
            path: 'ranking',
            component: RankingComponent,
            pathMatch: 'full'
          },
          {
            path: 'ranking/:groupType/:groupId',
            component: RankingComponent,
            pathMatch: 'full'
          },
          {
            path: 'company/rankings/:groupId',
            component: CompanyRankingsComponent,
            pathMatch: 'full'
          },
          {
            path: ':groupType/:groupId/overview',
            component: GroupOverviewComponent,
            pathMatch: 'full'
          },
          {
            path: ':groupType/:groupId/company-team/:subgroupId/overview',
            component: GroupOverviewComponent,
            pathMatch: 'full'
          },
          {
            path: 'company/:groupId/company-teams',
            component: CompanyTeamOverviewComponent,
            pathMatch: 'full'
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(layoutRoutes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {}

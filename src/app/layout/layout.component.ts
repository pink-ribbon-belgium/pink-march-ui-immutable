//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

// tslint:disable:template-cyclomatic-complexity //
// tslint:disable:template-conditional-complexity //

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { first, flatMap, map, shareReplay } from 'rxjs/operators';
import { AccountPreferencesService } from '../api/account-preferences/account-preferences.service';
import { AccountProfileService } from '../api/account-profile/account-profile.service';
import { AccountService } from '../api/account/account.service';
import { AccountAggregateService } from '../api/aggregates/account-aggregate.service';
import { AuthService } from '../auth/auth.service';
import * as authActions from '../auth/store/actions/auth.action';
import { selectIsLoggedIn } from '../auth/store/reducers/auth.reducer';
import { AuthState } from '../auth/store/state/auth.state';
import * as navActions from '../navigation/store/actions/nav.action';
import {
  selectAccountId,
  selectGroupId,
  selectGroupType,
  selectIsAdmin,
  selectIsMember,
  selectIsRegistered,
  selectPaymentCompleted,
  selectProfileLoaded,
  selectSubgroupId
} from '../navigation/store/reducers/nav.reducer';
import { NavState } from '../navigation/store/state/nav.state';
import { UnsubscribeOnDestroyAdapter } from '../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../shared/constant/auth0.constants';
import { localStorageConstant } from '../shared/constant/local-storage.constants';
import { Language } from '../shared/enum/language';
import { ConfigService } from '../shared/services/config/config.service';
import { AccountProfile } from '../types/account/account-profile';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent extends UnsubscribeOnDestroyAdapter implements OnInit, OnDestroy {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe([Breakpoints.Handset, Breakpoints.Small, Breakpoints.Medium])
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  isLoggedIn$: Observable<boolean>;
  groupId$: Observable<string>;
  accountId$: Observable<string>;
  loadedProfile$: Observable<boolean>;
  accountProfile: AccountProfile;
  groupType$: Observable<string>;
  subgroupId$: Observable<string>;
  isRegistered$: Observable<boolean>;
  isAdministrator$: Observable<boolean>;
  isMember$: Observable<boolean>;
  paymentCompleted$: Observable<boolean>;

  constructor(
    private readonly translate: TranslateService,
    private readonly accountProfileService: AccountProfileService,
    private readonly accountPreferencesService: AccountPreferencesService,
    private readonly accountAggregateService: AccountAggregateService,
    private readonly accountService: AccountService,
    private readonly breakpointObserver: BreakpointObserver,
    private readonly store: Store<AuthState>,
    public authService: AuthService,
    public config: ConfigService,
    private readonly navStateStore: Store<NavState>,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar
  ) {
    super();
  }

  ngOnInit(): void {
    this.storeListeners();
    this.initTranslate();
    this.checkAccountProfile();
    this.checkLevel();
    this.subscriptions.add(
      this.authService.userProfile$
        .pipe(
          first(Boolean),
          flatMap(userProfile => this.accountService.getAdministratorOf(userProfile[auth0Constant.accountId]))
        )
        .subscribe(administrations => {
          this.navStateStore.dispatch(
            navActions.loadAdministrationSuccess({
              isAdmin: true,
              groupId: administrations[0].groupId,
              groupType: administrations[0].groupType
            })
          );
        })
    );
    this.subscriptions.add(
      this.authService.userProfile$
        .pipe(
          first(Boolean),
          flatMap(userProfile => this.accountService.uniqueGetSlot(userProfile[auth0Constant.accountId]))
        )
        .subscribe(
          slot => {
            let isMember = false;
            if (slot.subgroupId) {
              isMember = true;
            }
            this.navStateStore.dispatch(
              navActions.loadSlotSuccess({
                slot,
                isMember,
                isRegistered: true,
                groupId: slot.groupId,
                groupType: slot.groupType,
                subgroupId: slot.subgroupId
              })
            );
          },
          err => {
            if (err.status === 404) {
              this.store.dispatch(navActions.loadSlotFailed({ isRegistered: false }));
            }
          }
        )
    );
  }

  login(): void {
    this.store.dispatch(authActions.login({}));
  }

  logout(): void {
    this.store.dispatch(authActions.logout());
  }

  storeListeners(): void {
    this.loadedProfile$ = this.navStateStore.pipe(select(selectProfileLoaded));
    this.accountId$ = this.navStateStore.pipe(select(selectAccountId));
    this.groupId$ = this.navStateStore.pipe(select(selectGroupId));
    this.groupType$ = this.navStateStore.pipe(select(selectGroupType));
    this.isAdministrator$ = this.navStateStore.pipe(select(selectIsAdmin));
    this.isRegistered$ = this.navStateStore.pipe(select(selectIsRegistered));
    this.isMember$ = this.navStateStore.pipe(select(selectIsMember));
    this.subgroupId$ = this.navStateStore.pipe(select(selectSubgroupId));
    this.paymentCompleted$ = this.navStateStore.pipe(select(selectPaymentCompleted));
    this.isLoggedIn$ = this.navStateStore.pipe(select(selectIsLoggedIn));
  }

  registerIndividual(): void {
    void this.router.navigate(['/register/personal-profile']);
  }
  registerCompany(): void {
    void this.router.navigate(['/register/company-profile']);
  }

  private initTranslate(): void {
    this.translate.setDefaultLang(Language.nl);

    if (this.getInitialLanguage() === 'fr') {
      this.translate.use(Language.fr);
    } else {
      this.translate.use(Language.nl);
    }

    this.subscriptions.add(
      this.translate.onLangChange.subscribe((langChange: LangChangeEvent) => {
        localStorage.setItem(localStorageConstant.language, langChange.lang);
      })
    );

    this.getPreferenceLanguage();
  }

  private getInitialLanguage(): string {
    return localStorage.getItem(localStorageConstant.language) || this.translate.getBrowserLang();
  }

  private getPreferenceLanguage(): void {
    this.subscriptions.add(
      this.authService.userProfile$
        .pipe(
          first(Boolean),
          flatMap(userData =>
            this.accountPreferencesService.getAccountPreferencesByAccountId(userData[auth0Constant.accountId])
          )
        )
        .subscribe(preferences => {
          this.translate.use(preferences.language);
        })
    );
  }

  private checkAccountProfile(): void {
    this.subscriptions.add(
      this.authService.userProfile$
        .pipe(
          first(Boolean),
          flatMap(userData =>
            this.accountProfileService.getAccountProfileByAccountId(userData[auth0Constant.accountId])
          )
        )
        .subscribe(
          profile => {
            this.navStateStore.dispatch(
              navActions.loadProfileSuccess({ profileLoaded: true, accountProfile: profile })
            );
          },
          err => {
            if (err.status === 404) {
              this.navStateStore.dispatch(navActions.loadProfileFail({ profileLoaded: false }));
            }
          }
        )
    );
  }

  private checkLevel(): void {
    this.subscriptions.add(
      this.authService.userProfile$
        .pipe(
          first(Boolean),
          flatMap(userProfile => this.accountAggregateService.getAccountAggregate(userProfile[auth0Constant.accountId]))
        )
        .subscribe(result => {
          if (result.newLevel && !result.acknowledged) {
            this.translate.get('COMMON.NEW_LEVEL').subscribe(translation => {
              this.snackBar
                .open(translation, 'OK', {
                  verticalPosition: 'top'
                })
                .afterDismissed()
                .subscribe(dismissedByAction => {
                  if (dismissedByAction) {
                    const updatedAggregate = {
                      ...result,
                      previousLevel: result.level,
                      acknowledged: true
                    };
                    delete updatedAggregate.links;
                    this.accountAggregateService.putAccountAggregate(updatedAggregate).subscribe();
                  }
                });
            });
          }
        })
    );
  }
}

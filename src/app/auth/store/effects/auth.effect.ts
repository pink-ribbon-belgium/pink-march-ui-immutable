//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { flatMap, map, tap } from 'rxjs/operators';
import { UserProfile } from '../../../types/auth/user-profile';
import { AuthService } from '../../auth.service';
import * as authActions from '../actions/auth.action';

@Injectable()
export class AuthEffects {
  login$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.login),
        tap((action: { url?: string }) => {
          this.authService.login(action.url);
        })
      ),
    { dispatch: false }
  );

  loginComplete$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.loginComplete),
      flatMap(action => {
        if (action.loggedIn) {
          return this.authService.userProfile$.pipe(
            map((userProfile: UserProfile) => authActions.loginSuccess({ userProfile }))
          );
        }

        return of(authActions.loginFailure());
      })
    )
  );

  logout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.logout),
        tap(() => {
          this.authService.logout();
        })
      ),
    { dispatch: false }
  );

  constructor(private readonly actions$: Actions, private readonly authService: AuthService) {}
}

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import createSpy = jasmine.createSpy;
import { UserProfile } from '../../../types/auth/user-profile';
import { AuthService } from '../../auth.service';
import * as authActions from '../actions/auth.action';
import { AuthEffects } from './auth.effect';

let actions$: Observable<Action>;

describe('AuthEffects', () => {
  let effects: AuthEffects;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthEffects,
        provideMockActions(() => actions$),
        {
          provide: AuthService,
          useValue: {
            login: createSpy('login'),
            logout: createSpy('logout'),
            userProfile$: of(null)
          }
        }
      ]
    });
  });

  beforeEach(() => {
    effects = TestBed.get<AuthEffects>(AuthEffects);
    authService = TestBed.get<AuthService>(AuthService);
  });

  it('should call login on authservice', (done: DoneFn) => {
    actions$ = of(authActions.login({}));
    effects.login$.subscribe(() => {
      expect(authService.login).toHaveBeenCalled();
      done();
    });
  });

  it('should call logout on authservice', (done: DoneFn) => {
    actions$ = of(authActions.logout());
    effects.logout$.subscribe(() => {
      expect(authService.logout).toHaveBeenCalled();
      done();
    });
  });

  it('should return loginSuccess', (done: DoneFn) => {
    actions$ = of(authActions.loginComplete({ loggedIn: true }));
    effects.loginComplete$.subscribe(action => {
      expect(action.type).toEqual(authActions.AuthActionTypes.LoginSuccess);
      done();
    });
  });

  it('should return loginFailure', (done: DoneFn) => {
    actions$ = of(authActions.loginComplete({ loggedIn: false }));
    effects.loginComplete$.subscribe(action => {
      expect(action.type).toEqual(authActions.AuthActionTypes.LoginFailure);
      done();
    });
  });
});

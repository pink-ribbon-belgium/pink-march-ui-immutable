//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import * as AuthActions from '../actions/auth.action';
import * as fromAuth from './auth.reducer';

describe('AuthReducers', () => {
  it('should return expected state ', () => {
    const userProfile = {
      'https://pink-ribbon-belgium.org/accountId': 'test-Id',
      'https://pink-ribbon-belgium.org/mode': 'test',
      nickname: 'eric',
      name: 'eric',
      picture: 'nice-picture',
      updated_at: 'today',
      email: 'eric@test.com',
      email_verified: true,
      sub: 'insane-sub'
    };
    const expectedState = {
      isLoggedIn: true,
      userProfile
    };
    const state = fromAuth.authReducer({ isLoggedIn: true, userProfile }, AuthActions.loginSuccess({ userProfile }));
    expect(state).toEqual(expectedState);
  });

  it('should return expected state when loginComplete was called', () => {
    const expectedState = {
      isLoggedIn: true,
      userProfile: undefined
    };
    const state = fromAuth.authReducer(
      { isLoggedIn: false, userProfile: undefined },
      AuthActions.loginComplete({ loggedIn: true })
    );
    expect(state).toEqual(expectedState);
  });

  it('should return expected userProfile', () => {
    const expectedUserProfile = {
      'https://pink-ribbon-belgium.org/accountId': 'test-Id',
      'https://pink-ribbon-belgium.org/mode': 'test',
      nickname: 'eric',
      name: 'eric',
      picture: 'nice-picture',
      updated_at: 'today',
      email: 'eric@test.com',
      email_verified: true,
      sub: 'insane-sub'
    };

    const expectedState = {
      isLoggedIn: false,
      userProfile: expectedUserProfile
    };

    const state = fromAuth.authReducer(
      { isLoggedIn: false, userProfile: undefined },
      AuthActions.loginSuccess({ userProfile: expectedUserProfile })
    );
    expect(state).toEqual(expectedState);
  });
});

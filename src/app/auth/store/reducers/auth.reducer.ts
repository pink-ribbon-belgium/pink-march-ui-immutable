//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { loginComplete, loginSuccess } from '../actions/auth.action';

export interface AuthState {
  isLoggedIn: boolean;
  userProfile: {};
}

export const initialState: AuthState = {
  isLoggedIn: false,
  userProfile: undefined
};

const _authReducer = createReducer(
  initialState,
  on(loginSuccess, (state, { userProfile }) => ({ ...state, userProfile })),
  on(loginComplete, (state, { loggedIn }) => ({ ...state, isLoggedIn: loggedIn }))
);

// NOTE: tslint says we should not wrap, because it is not needed; and it says only to use arrow functions
//       angular build says we should: https://stackoverflow.com/questions/56993101/error-building-angularngrx-8-for-production-when-using-createreducer-function/56993402
//       both wrap and not use a function expression, but a regular function
// tslint:disable-next-line
export function authReducer(state, action) {
  return _authReducer(state, action);
}

export const selectAuthState = createFeatureSelector<AuthState>('auth');
export const selectIsLoggedIn = createSelector(selectAuthState, authState => authState.isLoggedIn);

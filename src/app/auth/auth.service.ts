//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import createAuth0Client from '@auth0/auth0-spa-js';
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, combineLatest, from, Observable, of } from 'rxjs';
import { concatMap, shareReplay, tap } from 'rxjs/operators';
import { auth0Constant } from '../shared/constant/auth0.constants';
import { Language } from '../shared/enum/language';
import { ConfigService } from '../shared/services/config/config.service';
import * as authActions from './store/actions/auth.action';
import { AuthState } from './store/state/auth.state';

@Injectable()
export class AuthService {
  // Create an observable of Auth0 instance of client
  auth0Client$ = from(
    createAuth0Client({
      domain: this.config.isProductionOrDemoMode
        ? 'auth.pink-march.pink-ribbon-belgium.org'
        : 'auth-dev.pink-march.pink-ribbon-belgium.org',
      client_id: this.config.isProductionOrDemoMode ? auth0Constant.prodClientId : auth0Constant.devClientId,
      redirect_uri: this.config.canonicalLocation,
      audience: auth0Constant.audience,
      mode: this.config.mode
    })
    // NOTE:
    // Re-authorizes the user in an iframe, gets a token from the returned code, and caches it
    // throws
    // - if authorize in the iframe times out (60s)
    // - if there is an error from the IdP (AuthenticationError), or
    // - the call to /oauth2/token fails for any reason (http, network, or STS auth error), or
    // - if the returned token verification fails
    // (also throws if setup is problematic)
    // *
    // An authentication error, or a wrong token, would be unexpected, since this only runs if we are logged in.
    // Network issues are possible. Finally, it is possible that the user's credentials are revoked in STS
    // between login and now.
  ).pipe(
    shareReplay(1) // Every subscription receives the same shared value
    // MUDO example has here:     catchError(err => throwError(err))
  );
  // Define observables for SDK methods that return promises by default
  // For each Auth0 SDK method, first ensure the client instance is ready
  // concatMap: Using the client instance, call SDK method; SDK returns a promise
  // from: Convert that resulting promise into an observable
  isAuthenticated$ = this.auth0Client$.pipe(
    concatMap((client: Auth0Client) => from(client.isAuthenticated())),
    // NOTE: no error expected from client.isAuthenticated()
    tap(res => (this.loggedIn = res))
  );
  handleRedirectCallback$ = this.auth0Client$.pipe(
    concatMap((client: Auth0Client) => from(client.handleRedirectCallback()))
    // NOTE:
    // client.handleRedirectCallback() throws
    // - if there are no query parameters, or
    // - they contain an error from the IdP (AuthenticationError), or
    // - the state referred to in the query parameters doesn't exist in local storage ('Invalid state'), or
    // - the call to /oauth2/token fails for any reason (http, network, or STS auth error), or
    // - if the returned token verification fails
    // *
    // wrong query parameters, no state, or token verification errors are unexpected; that indicates a programming error
    // an error from the IdP is possible: user can be not-authenticated
    // Network issues are possible. Finally, it is possible that the user's credentials are revoked in STS
    // between login and now.
  );
  // Create subject and public observable of user profile data
  userProfileSubject$ = new BehaviorSubject<any>(null);
  userProfile$ = this.userProfileSubject$.asObservable();
  // Create a local property for login status
  loggedIn: boolean = null;

  constructor(
    private readonly router: Router,
    private readonly config: ConfigService,
    private readonly translate: TranslateService,
    private readonly store: Store<AuthState>
  ) {
    // On initial load, check authentication state with authorization server
    // Set up local auth streams if user is already authenticated
    this.localAuthSetup();
    // Handle redirect from Auth0 login
    this.handleAuthCallback();
  }

  // When calling, options can be passed if desired
  // https://auth0.github.io/auth0-spa-js/classes/auth0client.html#getuser
  getUser$(options?): Observable<any> {
    return this.auth0Client$.pipe(
      concatMap((client: Auth0Client) => from(client.getUser(options))),
      // NOTE:
      // client.getUser only looks for the user in the local cache, based on audience and scope in options
      // it might return undefined, but does not throw
      tap(user => {
        this.userProfileSubject$.next(user);
      })
    );
  }

  login(redirectPath = '/'): void {
    // A desired redirect path can be passed to login method
    // (e.g., from a route guard)
    // Ensure Auth0 client instance exists
    this.auth0Client$.subscribe((client: Auth0Client) => {
      // Call method to log in
      void client.loginWithRedirect({
        redirect_uri: this.config.canonicalLocation,
        appState: { target: redirectPath, mode: this.config.mode }
      });
      // NOTE:
      //  this changes to location to IdP/authorize
      //  there is no meaningful error possible here
    });
  }

  logout(): void {
    // Ensure Auth0 client instance exists
    this.auth0Client$.subscribe((client: Auth0Client) => {
      localStorage.clear();
      // Call method to log out
      client.logout({
        client_id: this.config.isProductionOrDemoMode ? auth0Constant.prodClientId : auth0Constant.devClientId,
        returnTo:
          this.translate.currentLang === Language.fr ? 'https://www.derozemars.be/fr' : 'https://www.derozemars.be/'
      });
      // NOTE:
      //  this changes to location to IdP/logout
      //  there is no meaningful error possible here
    });
  }

  getTokenSilently$(options?): Observable<string> {
    return this.auth0Client$.pipe(concatMap((client: Auth0Client) => from(client.getTokenSilently(options))));
    // NOTE:
    // returns a cached token, if it is valid, for the options (and if options.ignoreCache is falsy)
    // otherwise, re-authorizes the user in an iframe, gets a token from the returned code, and caches it
    // throws
    // - if authorize in the iframe times out (60s)
    // - if there is an error from the IdP (AuthenticationError), or
    // - the call to /oauth2/token fails for any reason (http, network, or STS auth error), or
    // - if the returned token verification fails
    // *
    // An authentication error, or a wrong token, would be unexpected, since this only runs if we are logged in.
    // Network issues are possible. Finally, it is possible that the user's credentials are revoked in STS
    // between login and now.
  }

  private localAuthSetup(): void {
    // This should only be called on app initialization
    // Set up local authentication streams
    const checkAuth$ = this.isAuthenticated$.pipe(
      concatMap((loggedIn: boolean) => {
        if (loggedIn) {
          // If authenticated, get user and set in app
          // NOTE: you could pass options here if needed
          return this.getUser$();
        }

        // If not authenticated, return stream that emits 'false'
        return of(loggedIn);
      })
    );
    checkAuth$.subscribe();
  }

  private handleAuthCallback(): void {
    // Call when app reloads after user logs in with Auth0
    const params = window.location.search;
    if (params.includes('code=') && params.includes('state=')) {
      let targetRoute: string; // Path to redirect to after login processsed
      const authComplete$ = this.handleRedirectCallback$.pipe(
        // Have client, now call method to handle auth callback redirect
        tap(cbRes => {
          // Get and set target redirect route from callback results
          targetRoute = cbRes.appState && cbRes.appState.target ? cbRes.appState.target : '/';
        }),
        concatMap(() =>
          // Redirect callback complete; get user and login status
          combineLatest([this.getUser$(), this.isAuthenticated$])
        )
      );
      // Subscribe to authentication completion observable
      // Response will be an array of user and login status
      authComplete$.subscribe(([user, loggedIn]) => {
        // Redirect to target route after callback processing
        this.store.dispatch(authActions.loginComplete({ loggedIn }));
        window.history.replaceState({}, document.title, this.config.canonicalLocation);
        void this.router.navigate([targetRoute]);
      });
    }
  }
}

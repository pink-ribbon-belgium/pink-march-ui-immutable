//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TranslateModule } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import * as authActions from './store/actions/auth.action';
import { selectIsLoggedIn } from './store/reducers/auth.reducer';
import { AuthState, initialAuthState } from './store/state/auth.state';

describe('AuthGuard', () => {
  let authService: AuthService;
  let guard: AuthGuard;
  let mockStore: MockStore<AuthState>;
  const routeMock: any = { snapshot: {} };
  const routeStateMock: any = { snapshot: {}, url: '/register' };
  const routerMock = { navigate: jasmine.createSpy('navigate') };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        { provide: Router, useValue: routerMock },
        provideMockStore({ initialState: { auth: initialAuthState } }),
        {
          provide: AuthService,
          useValue: {}
        }
      ],
      imports: [HttpClientTestingModule, TranslateModule.forRoot()]
    });
    authService = TestBed.get(AuthService);
    guard = TestBed.get(AuthGuard);
    mockStore = TestBed.get<Store<AuthState>>(Store);
  });

  it('should be created', () => {
    const authGuard: AuthGuard = TestBed.get(AuthGuard);
    expect(authGuard).toBeTruthy();
  });

  it('should not allow and redirect an unauthenticated user', (done: DoneFn) => {
    const canDeactivate$ = guard.canActivate(routeMock, routeStateMock) as Observable<boolean>;
    spyOn(mockStore, 'dispatch');
    canDeactivate$.subscribe(deactivate => {
      expect(deactivate).toBeFalsy();
      expect(mockStore.dispatch).toHaveBeenCalledWith(authActions.login({ url: '/register' }));
      done();
    });
  });

  it('should allow the authenticated user to access app', (done: DoneFn) => {
    mockStore.overrideSelector(selectIsLoggedIn, true);
    const canDeactivate$ = guard.canActivate(routeMock, routeStateMock) as Observable<boolean>;
    canDeactivate$.subscribe(deactivate => {
      expect(deactivate).toBeTruthy();
      done();
    });
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { sharedModules } from '../../shared/constant/shared-modules';
import { JoinLinkComponent } from './join-link.component';

describe('JoinLinkComponent', () => {
  let component: JoinLinkComponent;
  let fixture: ComponentFixture<JoinLinkComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JoinLinkComponent],
      imports: [...appModules, ...sharedModules],
      providers: [
        {
          provide: AuthService,
          useValue: {}
        }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

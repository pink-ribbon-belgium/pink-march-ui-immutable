import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';

@Component({
  selector: 'app-join-link',
  templateUrl: './join-link.component.html',
  styleUrls: ['./join-link.component.scss']
})
export class JoinLinkComponent extends UnsubscribeOnDestroyAdapter {
  @Input() link: string;

  constructor(private readonly snackBar: DefaultSnackbarService, private readonly translate: TranslateService) {
    super();
  }

  // tslint:disable-next-line:comment-type
  /* istanbul ignore next : as this is not useful to test */
  copyJoinLink(): void {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.link;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    this.subscriptions.add(
      this.translate.get('COMPANY.SNACK_BAR.JOIN_LINK_COPIED').subscribe(message => {
        this.snackBar.openSnackBar(message);
      })
    );
  }
}

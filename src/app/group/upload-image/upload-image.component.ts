import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss']
})
export class UploadImageComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('imageUpload', { static: false }) imageUpload: ElementRef;
  @Input() formGroup: FormGroup;

  file: File;
  reader: FileReader;
  url: string;
  logo: string;
  formControl;
  disableDelete = false;

  constructor(
    private readonly snackBar: DefaultSnackbarService,
    private readonly sanitizer: DomSanitizer,
    private readonly translate: TranslateService
  ) {
    super();
  }

  ngOnInit(): void {
    this.formControl = this.formGroup.get('logo');
    this.logo = this.formControl.value;
    this.checkForAvatar();
  }

  onFileChange(event): void {
    if (event.target.files.length > 0 && event.target.files[0].size <= 390000) {
      this.setToBase64(event);
    } else {
      this.subscriptions.add(
        this.translate.get('UPLOAD_SNACKBAR.ERROR').subscribe(message => {
          this.snackBar.openSnackBar(message);
        })
      );
    }
  }

  // tslint:disable-next-line:comment-type
  /* istanbul ignore next : as this is not useful to test */
  setToBase64(inputValue): void {
    this.reader = new FileReader();
    this.reader.onloadend = (event: any) => {
      this.url = event.target.result;
      const urlSegments = event.target.result.split(',');
      const base64 = urlSegments[1];
      this.formControl.setValue(base64);
      this.formGroup.markAsDirty();
      this.disableDelete = false;
    };
    this.reader.readAsDataURL(inputValue.target.files[0]);
  }

  checkForAvatar(): void {
    if (!this.logo) {
      this.url = '';
      this.disableDelete = true;
    } else {
      this.url = `data:image/jpeg;base64,${this.logo}`;
      this.disableDelete = false;
    }
  }

  resetLogo(): void {
    this.formControl.setValue();
    this.formControl.markAsDirty();
    this.disableDelete = true;
    this.imageUpload.nativeElement.value = '';
    this.url = '';
  }

  sanitizeUrl(url): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, filter, finalize, flatMap, map, tap } from 'rxjs/operators';
import { CompanyService } from '../../api/company/company.service';
import { SubgroupService } from '../../api/subgroup/subgroup.service';
import { TeamService } from '../../api/team/team.service';
import { AuthService } from '../../auth/auth.service';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../shared/constant/auth0.constants';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';
import { ConfirmDeleteDialogComponent } from '../confirm-delete-dialog/confirm-delete-dialog.component';

// tslint:disable:template-cyclomatic-complexity //
@Component({
  selector: 'app-group-overview',
  templateUrl: './group-overview.component.html',
  styleUrls: ['./group-overview.component.scss']
})
export class GroupOverviewComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;
  isLoading = true;
  formGroup: FormGroup;
  groupId: string;
  subgroupId: string;
  groupType: string;
  subgroupName: string;
  accountId: string;
  members;
  showDelete = false;
  dataSource = new MatTableDataSource<any>(); // Add dto
  displayedColumns: Array<string> = ['name', 'actions'];

  constructor(
    private readonly fb: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly teamService: TeamService,
    private readonly companyService: CompanyService,
    private readonly subgroupService: SubgroupService,
    private readonly translate: TranslateService,
    public authService: AuthService,
    private readonly router: Router,
    private readonly dialog: MatDialog,
    private readonly snackBar: DefaultSnackbarService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.authService.userProfile$.subscribe(userProfile => (this.accountId = userProfile[auth0Constant.accountId]))
    );
    this.subscriptions.add(
      this.route.params
        .pipe(
          tap(params => {
            this.groupId = params.groupId;
            this.subgroupId = params.subgroupId;
            this.groupType = params.groupType;
          }),
          flatMap(() =>
            this.getData().pipe(
              finalize(() => {
                this.isLoading = false;
              })
            )
          )
        )
        .subscribe(([members, subgroup]) => {
          if (members.length > 0) {
            this.members = members;
          }
          this.dataSource.data = members;
          this.paginator.length = members.length;
          this.dataSource.paginator = this.paginator;
          if (subgroup) {
            this.subgroupName = subgroup.name;
            this.enableSubgroupDelete(members);
          }
        })
    );
  }

  getData(): Observable<Array<any>> {
    return !this.subgroupId
      ? forkJoin(this.teamService.getMembers(this.groupId))
      : forkJoin(
          this.subgroupService.getSubgroupMembers(this.groupId, this.subgroupId).pipe(
            map(res => res),
            catchError(err => of([]))
          ),
          this.subgroupService.getSubgroupById(this.groupId, this.subgroupId)
        );
  }

  deleteMember(member): void {
    switch (this.groupType) {
      case 'team': {
        const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
          width: '400px',
          disableClose: true,
          data: {
            confirmMessage: 'OVERVIEW.DIALOG.CONFIRM_DELETE_MEMBER_MESSAGE'
          }
        });
        dialogRef
          .afterClosed()
          .pipe(filter(Boolean))
          .subscribe(() => {
            this.isLoading = true;
            this.subscriptions.add(
              this.teamService
                .deleteTeamMember(this.groupId, member.accountId)
                .pipe(
                  flatMap(() => this.translate.get('COMPANY.SNACK_BAR.DELETE_MEMBER_SUCCESS')),
                  finalize(() => (this.isLoading = false))
                )
                .subscribe(successMessage => {
                  this.members.splice(this.members.indexOf(member), 1);
                  this.dataSource.data = this.members;
                  this.snackBar.openSnackBar(successMessage);
                })
            );
          });
        break;
      }
      case 'company': {
        if (!this.subgroupId) {
          const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
            width: '400px',
            disableClose: true,
            data: {
              confirmMessage: 'OVERVIEW.DIALOG.CONFIRM_DELETE_MEMBER_MESSAGE'
            }
          });
          dialogRef
            .afterClosed()
            .pipe(filter(Boolean))
            .subscribe(() => {
              this.isLoading = true;
              this.subscriptions.add(
                this.companyService
                  .deleteCompanyMember(this.groupId, member.accountId)
                  .pipe(
                    flatMap(() => this.translate.get('COMPANY.SNACK_BAR.DELETE_MEMBER_SUCCESS')),
                    finalize(() => (this.isLoading = false))
                  )
                  .subscribe(successMessage => {
                    this.members.splice(this.members.indexOf(member), 1);
                    this.dataSource.data = this.members;
                    this.snackBar.openSnackBar(successMessage);
                  })
              );
            });
        } else {
          const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
            width: '400px',
            disableClose: true,
            data: {
              confirmMessage: 'OVERVIEW.DIALOG.CONFIRM_DELETE_MEMBER_MESSAGE'
            }
          });
          dialogRef
            .afterClosed()
            .pipe(filter(Boolean))
            .subscribe(() => {
              this.isLoading = true;
              this.subscriptions.add(
                this.subgroupService
                  .deleteSubgroupMember(this.groupId, this.subgroupId, member.accountId)
                  .pipe(
                    flatMap(() => this.translate.get('COMPANY.SNACK_BAR.DELETE_MEMBER_SUCCESS')),
                    finalize(() => (this.isLoading = false))
                  )
                  .subscribe(successMessage => {
                    this.members.splice(this.members.indexOf(member), 1);
                    this.enableSubgroupDelete(this.members);
                    this.dataSource.data = this.members;
                    this.snackBar.openSnackBar(successMessage);
                  })
              );
            });
        }
        break;
      }
      default:
        break;
    }
  }

  enableDelete(member): boolean {
    // Enable delete when account is not the admin and when not in the last week of the activity period
    if (this.subgroupId) {
      return !(new Date().getTime() >= new Date('2021-05-24').getTime());
    }

    return (
      member.accountId !== this.accountId &&
      !(new Date().getTime() >= new Date('2021-05-24').getTime()) &&
      this.members.length > 1
    );
  }

  enableSubgroupDelete(members): void {
    this.showDelete = !(members.length > 0);
  }

  back(): void {
    if (this.subgroupId) {
      void this.router.navigate([`/dashboard/company/${this.groupId}/company-teams`]);
    } else {
      void this.router.navigate([`/dashboard/${this.groupType}/${this.groupId}`]);
    }
  }

  deleteSubgroup(subgroupId: string): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      width: '400px',
      disableClose: true,
      data: {
        confirmMessage: 'OVERVIEW.DIALOG.CONFIRM_DELETE_SUBGROUP_MESSAGE'
      }
    });
    dialogRef
      .afterClosed()
      .pipe(filter(Boolean))
      .subscribe(() => {
        this.isLoading = true;
        this.subscriptions.add(
          this.subgroupService
            .deleteSubgroup(this.groupId, subgroupId)
            .pipe(flatMap(() => this.translate.get('COMPANY.SNACK_BAR.DELETE_SUBGROUP_SUCCESS')))
            .subscribe(
              successMessage => {
                this.isLoading = false;
                this.snackBar.openSnackBar(successMessage);
                void this.router.navigate([`/dashboard/company/${this.groupId}/company-teams`]);
              },
              err => {
                this.isLoading = false;
                this.translate.get('COMPANY.SNACK_BAR.DELETE_SUBGROUP_ERROR').subscribe(message => {
                  this.snackBar.openSnackBar(message);
                });
              }
            )
        );
      });
  }
}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-title',
  templateUrl: './info-title.component.html',
  styleUrls: ['./info-title.component.scss']
})
export class InfoTitleComponent implements OnInit {
  @Input() title: string;
  @Input() url: string;
  @Input() hideIcon: boolean;

  // tslint:disable-next-line:no-empty
  ngOnInit(): void {}

  openInfoLink(): void {
    if (this.url) {
      window.open(this.url);
    }
  }
}

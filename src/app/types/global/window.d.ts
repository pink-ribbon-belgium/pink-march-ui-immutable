interface Window {
  env: {
    deployBaseUrl: string;
    deployPath: string;
    immutableBuild: string;
    bookmarkableBuild: string;
    disableAssertionChecks: boolean;
    enableServiceWorker: boolean;
    intervalVersionChecker: number; // expressed in seconds
    apiStage: string;
  };
}

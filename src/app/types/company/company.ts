//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Group } from '../group/group';

export interface Company extends Group {
  address: string;
  zip: string;
  city: string;
  vat: string;
  reference?: string;
  logo: string;
  contactFirstName: string;
  contactLastName: string;
  contactEmail: string;
  contactTelephone: string;
  unit?: string;
}

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { NgModule } from '@angular/core';
import { DateAdapter, ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { CustomDateAdapter } from './adapter/custom-date-adapter';
import { sharedModules } from './constant/shared-modules';
import { sharedComponents } from './constant/shared.components';

@NgModule({
  declarations: [...sharedComponents],
  imports: [...sharedModules],
  exports: [...sharedModules, ...sharedComponents],
  providers: [
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher },
    { provide: DateAdapter, useClass: CustomDateAdapter }
  ]
})
export class SharedModule {
  constructor(private readonly translate: TranslateService) {
    translate.addLangs(['nl', 'fr']);
    translate.setDefaultLang('nl');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/nl|fr/) ? browserLang : 'nl');
  }
}

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';

/**
 * A class that automatically unsubscribes all observables when
 * the object gets destroyed
 */
export class UnsubscribeOnDestroyAdapter implements OnDestroy {
  /** The subscription sink object that stores all subscriptions */
  subscriptions = new SubSink();

  /**
   * The lifecycle hook that unsubscribes all subscriptions
   * when the component / object gets destroyed
   */
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}

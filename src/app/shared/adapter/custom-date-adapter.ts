//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Injectable } from '@angular/core';
import { NativeDateAdapter } from '@angular/material';

@Injectable()
export class CustomDateAdapter extends NativeDateAdapter {
  parse(value: any): Date | null {
    let splitValue: Array<string> = [];
    let newValue = value;
    if (typeof value === 'string' && value.indexOf('/') > -1) {
      splitValue = value.split('/');
    } else if (typeof value === 'string' && value.indexOf('-') > -1) {
      splitValue = value.split('-');
    }

    if (splitValue.length > 0) {
      const year = Number(splitValue[2]);
      const month = Number(splitValue[1]) - 1;
      const date = Number(splitValue[0]);
      newValue = new Date(year, month, date);
    }

    const timestamp = typeof newValue === 'number' ? value : Date.parse(newValue);

    return isNaN(timestamp) ? null : new Date(timestamp);
  }

  format(date: Date, displayFormat: Object): string {
    const formattedDate = new Date(
      Date.UTC(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
        date.getMilliseconds()
      )
    );
    const displayFor = { ...displayFormat, timeZone: 'utc' };
    const dtf = new Intl.DateTimeFormat(this.locale, displayFor);

    return dtf.format(formattedDate).replace(/[\u200e\u200f]/g, '');
  }
}

import { Injectable } from '@angular/core';
import TransactionManager from '@auth0/auth0-spa-js/src/transaction-manager';
import * as assert from 'assert';

const transactionManager = new TransactionManager();

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  /**
   * The mode is determined on load, potentially from state stored before we call Auth0. That way, the mode travels over Auth0 callbacks.
   */
  mode: string;

  /**
   * The canonical URI of this app, as currently used. This always ends with `…/index.html`.
   * This is used in all cases, including as callback from external services.
   * This contains no query part (`?…`) nor hash (`#…`).
   *
   * Determined on load, and never changes,
   */
  canonicalLocation: string;

  ogoneRedirectUrl: string;

  modeExpressions = {
    production: /^https:\/\/app\.pink-march\.pink-ribbon-belgium\.org\/index\.html$/,
    demo: /^https:\/\/demo\.pink-march\.pink-ribbon-belgium\.org\/index\.html$/,
    june2020: /^https:\/\/june2020\.pink-march\.pink-ribbon-belgium\.org\/index\.html$/,
    acceptance: /^https:\/\/app\.pink-march\.pink-ribbon-belgium\.org\/ui\/bookmarkable\/\d{5}\/index\.html$/,
    qa: /^https:\/\/app\.pink-march\.pink-ribbon-belgium\.org\/ui\/immutable\/\d{5}\/index\.html$/
  };

  get isProductionOrDemoMode(): boolean {
    return this.productionModes.includes(this.mode);
  }

  get isProductionMode(): boolean {
    return this.mode === 'production';
  }

  private readonly productionModes = ['production', 'demo', 'june2020'];

  constructor() {
    this.mode = this.getMode();
    this.canonicalLocation = this.getCanonicalLocation();
    // MUDO: This works in production, but gives problems when used with ?mode=…
    //       On return from Ogone, the mode is lost.
    //       We either need to store the mode in local storage, and communicate a state. We can use the ORDERID for this.
    //       Or, we put the mode in the redirect URL, if appropriate.
    //       *
    //       Suggestion:
    this.ogoneRedirectUrl = this.getOgoneRedirectUrl();
    // tslint:disable-next-line:no-console
    console.log(
      `%c
       ___  ____  ____  ______  ______   ___  ____
      / _ \\/ __ \\/_  / / __/  |/  / _ | / _ \\/ __/
     / , _/ /_/ / / /_/ _// /|_/ / __ |/ , _/\\ \\
    /_/|_|\\____/ /___/___/_/  /_/_/ |_/_/|_/___/

    API Stage: ${window.env.apiStage}
    Immutable version: ${window.env.immutableBuild}
    Bookmarkable version: ${window.env.bookmarkableBuild}
    Mode: ${this.getMode()}`,
      'color: #ba2e8d;'
    );
  }

  /**
   * Determine the canonical path of this app, as currently used. This always ends with `…/index.html`.
   * This is used in all cases, including as callback from external services.
   * This contains no query part (`?…`) nor hash (`#…`).
   * The result always starts with '/'
   *
   * Determined on load, and never changes,
   */
  getCanonicalPath(): string {
    const cleanParts = this.getPath()
      .split('/')
      .filter(p => !!p);
    if (cleanParts[cleanParts.length - 1] !== 'index.html') {
      cleanParts.push('index.html');
    }

    return `/${cleanParts.join('/')}`;
  }

  getCanonicalLocation(): string {
    return `${window.location.origin}${this.getCanonicalPath()}`;
  }

  getMode(): string {
    const queryParams = this.getQueryParams();
    if (queryParams.has('code') && queryParams.has('state')) {
      // we are handling a callback from Auth0. The mode is in the state stored in the Auth0 transaction manager
      const state = queryParams.get('state');
      const transaction = transactionManager.get(state);
      if (transaction && transaction.appState && transaction.appState.mode) {
        return transaction.appState.mode;
      }
      // else, do your thing
    }

    const queryMode = this.getModeQueryParam();

    assert(queryMode !== 'production');

    return queryMode ? queryMode : this.getLocationMode();
  }

  getPath(): string {
    return location.pathname;
  }

  getModeQueryParam(): string {
    const queryParams = new URLSearchParams(window.location.search);

    return queryParams.get('mode');
  }

  getQueryParams(): URLSearchParams {
    return new URLSearchParams(window.location.search);
  }

  getOgoneRedirectUrl(): string {
    return `${this.canonicalLocation}${
      this.mode === this.getLocationMode() ? '' : `?mode=${this.mode}`
    }#/register/payment-success`;
  }

  private getLocationMode(): string {
    const canonicalLocation = this.getCanonicalLocation();

    return this.modeExpressions.production.test(canonicalLocation)
      ? 'production'
      : this.modeExpressions.demo.test(canonicalLocation)
      ? 'demo'
      : this.modeExpressions.june2020.test(canonicalLocation)
      ? 'june2020'
      : this.modeExpressions.acceptance.test(canonicalLocation)
      ? `acceptance-${canonicalLocation.split('/')[5]}`
      : this.modeExpressions.qa.test(canonicalLocation)
      ? `qa-${canonicalLocation.split('/')[5]}`
      : 'dev-experiment';
  }
}

import TransactionManager from '@auth0/auth0-spa-js/src/transaction-manager';
import { ConfigService } from './config.service';

describe('ConfigService', () => {
  interface Fixture {
    config: ConfigService;
  }

  beforeEach(function(this: Fixture): void {
    this.config = new ConfigService();
  });

  it('should set mode to production', function(this: Fixture): void {
    spyOn(this.config, 'getCanonicalLocation').and.returnValue(
      'https://app.pink-march.pink-ribbon-belgium.org/index.html'
    );
    const mode = this.config.getMode();
    expect(mode).toBe('production');
  });

  it('should set mode to acceptance', function(this: Fixture): void {
    spyOn(this.config, 'getCanonicalLocation').and.returnValue(
      'https://app.pink-march.pink-ribbon-belgium.org/ui/bookmarkable/00019/index.html'
    );
    const mode = this.config.getMode();
    expect(mode).toBe('acceptance-00019');
  });

  it('should set mode to qa', function(this: Fixture): void {
    spyOn(this.config, 'getCanonicalLocation').and.returnValue(
      'https://app.pink-march.pink-ribbon-belgium.org/ui/immutable/00019/index.html'
    );
    const mode = this.config.getMode();
    expect(mode).toBe('qa-00019');
  });

  it('should set mode to dev-experiment for localhost', function(this: Fixture): void {
    spyOn(this.config, 'getCanonicalLocation').and.returnValue('https://localhost:4200/la/la/index.html');
    const mode = this.config.getMode();
    expect(mode).toBe('dev-experiment');
  });

  it('should set mode to whatever is stored in Auth0 state', function(this: Fixture): void {
    const myMode = 'this is my mode';
    const myState = 'this is my state';
    spyOn(TransactionManager.prototype, 'get').and.returnValue({
      nonce: 'irrelevant',
      scope: 'irrelevant',
      audience: 'irrelevant',
      code_verifier: 'irrelevant',
      appState: { mode: myMode },
      redirect_uri: ''
    });
    spyOn(this.config, 'getQueryParams').and.returnValue(new URLSearchParams(`?code=a_code&state=${myState}`));
    const mode = this.config.getMode();
    expect(mode).toBe(myMode);
  });

  it('should set mode to the normal stuff if there is no mode in Auth0 state', function(this: Fixture): void {
    const myMode = 'this is my mode';
    const myState = 'this is my state';
    spyOn(TransactionManager.prototype, 'get').and.returnValue({
      nonce: 'irrelevant',
      scope: 'irrelevant',
      audience: 'irrelevant',
      code_verifier: 'irrelevant',
      appState: { noMode: 'no mode here' },
      redirect_uri: ''
    });
    spyOn(this.config, 'getQueryParams').and.returnValue(new URLSearchParams(`?code=a_code&state=${myState}`));
    const mode = this.config.getMode();
    expect(mode).toBe('dev-experiment');
  });

  it('should set mode in redirectUrl when it is different from locationMode', function(this: Fixture): void {
    this.config.mode = 'qa-00015';
    const redirectUrl = this.config.getOgoneRedirectUrl();
    expect(redirectUrl).toContain('?mode=qa-00015');
  });

  it('should set mode to dev-experiment for anything else', function(this: Fixture): void {
    spyOn(this.config, 'getCanonicalLocation').and.returnValue('https://example.org/la/la/index.html');
    const mode = this.config.getMode();
    expect(mode).toBe('dev-experiment');
  });

  it('should return queryParam when available', function(this: Fixture): void {
    spyOn(this.config, 'getModeQueryParam').and.returnValue('demo');
    const mode = this.config.getMode();
    expect(mode).toBe('demo');
  });

  it('should return a path with index.html when there is none', function(this: Fixture): void {
    spyOn(this.config, 'getPath').and.returnValue('/');
    const canonicalPath = this.config.getCanonicalPath();
    expect(canonicalPath).toBe('/index.html');
  });

  it('should return a path with index.html when there is none, deeper, and ends with /', function(this: Fixture): void {
    spyOn(this.config, 'getPath').and.returnValue('/la/la/la/');
    const canonicalPath = this.config.getCanonicalPath();
    expect(canonicalPath).toBe('/la/la/la/index.html');
  });

  it('should return a path with index.html when there is none, deeper, and does not end with /', function(this: Fixture): void {
    spyOn(this.config, 'getPath').and.returnValue('/la/la/la');
    const canonicalPath = this.config.getCanonicalPath();
    expect(canonicalPath).toBe('/la/la/la/index.html');
  });

  it('should return a path with index.html when there already is one', function(this: Fixture): void {
    spyOn(this.config, 'getPath').and.returnValue('/index.html');
    const canonicalPath = this.config.getCanonicalPath();
    expect(canonicalPath).toBe('/index.html');
  });

  it('should return a path with index.html when there already is one, deeper', function(this: Fixture): void {
    spyOn(this.config, 'getPath').and.returnValue('la/la/la/index.html');
    const canonicalPath = this.config.getCanonicalPath();
    expect(canonicalPath).toBe('/la/la/la/index.html');
  });
});

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

export const trackersConstants = {
  dev: {
    fitbit: {
      clientId: '22BG64',
      clientSecret: '2b27010897e912f10e5f914f012bc49a',
      authorizeUrl: 'https://www.fitbit.com/oauth2/authorize'
    },
    googlefit: {
      clientId: '543514349135-rftj2hmq5ahogt02scbmsge0n231l72r.apps.googleusercontent.com',
      clientSecret: 'g3qg-QcMq7V7VzdXhbBT5Y3j',
      authorizeUrl: 'https://accounts.google.com/o/oauth2/v2/auth'
    },
    polar: {
      clientId: '1b556bb4-61dc-48d4-9291-d7b6ad39aafd',
      clientSecret: '28b875ee-9b26-4ab4-818e-369b57ca1373',
      authorizeUrl: 'https://flow.polar.com/oauth2/authorization'
    }
  },
  prod: {
    fitbit: {
      clientId: '22BBNF',
      clientSecret: '45642d84cdbbdc24bb276319468afd05',
      authorizeUrl: 'https://www.fitbit.com/oauth2/authorize'
    },
    googlefit: {
      clientId: '45183502406-qpl94o86gjo9kjle2dg96f9pgdhjoq1f.apps.googleusercontent.com',
      clientSecret: 'hYCXankN6TZSqxgkvg8b7wzp',
      authorizeUrl: 'https://accounts.google.com/o/oauth2/v2/auth'
    },
    polar: {
      production: {
        clientId: 'ac9ff0b9-1321-4b22-82e7-95769a4fbffe',
        clientSecret: '741e0e73-0ff6-40ca-8a60-5a51497b1d3f',
        authorizeUrl: 'https://flow.polar.com/oauth2/authorization'
      },
      demo: {
        clientId: '81701369-4360-41c0-802c-8875f8ea08e9',
        clientSecret: '50709cce-382a-4f6f-b2e8-12ccbc1eed35',
        authorizeUrl: 'https://flow.polar.com/oauth2/authorization'
      },
      june2020: {
        clientId: '0a6fac4c-47c8-4207-86a2-3d8bc2c4b440',
        clientSecret: 'e05877eb-25df-45c4-91aa-043b9b95d8b7',
        authorizeUrl: 'https://flow.polar.com/oauth2/authorization'
      }
    }
  }
};

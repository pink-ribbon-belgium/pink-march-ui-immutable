//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { distinctUntilChanged } from 'rxjs/operators';
import { AccountService } from '../../../api/account/account.service';
import { AccountAggregateService } from '../../../api/aggregates/account-aggregate.service';
import { AuthService } from '../../../auth/auth.service';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { Activity } from '../../../types/activity/activity';
import { AccountAggregate } from '../../../types/aggregates/account-aggregate';

// tslint:disable:template-cyclomatic-complexity //

@Component({
  selector: 'app-individual-ranking',
  templateUrl: './individual-ranking.component.html',
  styleUrls: ['./individual-ranking.component.scss']
})
export class IndividualRankingComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @Input() readonly accountAggregate: AccountAggregate;
  @Output() readonly loading = new EventEmitter<boolean>();
  totalDistance: number;
  currentDisplayLevel: string;
  nextDisplayLevel: string;
  isHandSet: boolean;
  activities: Array<Activity>;
  numberOfDays: number;
  isBusy = true;

  barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
      enabled: false,
      caretPadding: 100
    }
  };
  barChartLegend;
  barChartLabels: Array<Label>;
  barChartType: ChartType;
  barChartData: Array<ChartDataSets>;
  label: string;
  labelX: string;
  goalLabel: string;

  constructor(
    private readonly aggregateService: AccountAggregateService,
    private readonly accountService: AccountService,
    private readonly auth: AuthService,
    private readonly breakpointObserver: BreakpointObserver,
    private readonly translate: TranslateService
  ) {
    super();
  }

  ngOnInit(): void {
    this.isBusy = true;
    this.getTranslation();
    this.numberOfDays = this.getDaysInMonth(new Date());
    this.totalDistance = this.calcDistance(this.accountAggregate);
    this.currentDisplayLevel = this.getDisplayLevel(this.accountAggregate.level);
    this.nextDisplayLevel = this.getDisplayLevel(this.accountAggregate.level + 1);
    this.setGraph();
    this.subscriptions.add(
      this.breakpointObserver
        .observe(Breakpoints.Handset)
        .pipe(distinctUntilChanged())
        .subscribe(state => {
          this.isHandSet = state.matches;
        })
    );
    this.setActivities();
  }

  /***
   * Helper function for visualising the level
   * @param level level to display
   */
  getDisplayLevel(level: number): string {
    if (level > 10) {
      return '10+';
    }

    return level.toString();
  }

  getTranslation(): void {
    this.translate.get('RANKING.STEPS').subscribe(message => {
      this.label = message;
    });
    this.translate.get('RANKING.DATE').subscribe(message => {
      this.labelX = message;
    });
    this.translate.get('RANKING.GOAL').subscribe(message => {
      this.goalLabel = message;
    });
  }

  setActivities(): void {
    if (this.accountAggregate) {
      this.subscriptions.add(
        this.accountService.getActivities(this.accountAggregate.accountId).subscribe(
          result => {
            this.setGraphValues(result);
          },
          err => {
            this.setGraphValues();
          }
        )
      );
    }
  }

  getDaysInMonth(date): number {
    // tslint:disable-next-line
    const month = date.getMonth() + 1;
    const year = date.getYear();

    return new Date(year, month, 0).getDate();
  }

  setLabels(numberOfDays: number): void {
    const days = numberOfDays + 1;
    const arr = [];
    for (let i = 1; i < days; i++) {
      arr.push(i.toString());
    }
    this.barChartLabels = arr;
  }

  setGraph(): void {
    this.setLabels(this.numberOfDays);
    this.barChartOptions = {
      responsive: true,
      tooltips: {
        callbacks: {
          title(item, data): string | Array<string> {
            return '';
          }
        }
      },
      scales: {
        yAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: this.label
            },
            ticks: {
              beginAtZero: true
            }
          }
        ],
        xAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: this.labelX
            }
          }
        ]
      }
    };
    this.barChartType = 'bar';
    this.barChartLegend = true;
  }

  setGraphValues(activities?): void {
    const data = [];
    const goal = [];
    data.length = this.numberOfDays;
    if (activities) {
      activities.forEach(activity => {
        const date = activity.activityDate.slice(8, 10);
        data[date - 1] = activity.numberOfSteps;
      });
      for (let i = 0; i < this.numberOfDays; i++) {
        goal[i] = 10000;
      }
      this.barChartData = [
        { data, label: this.label },
        { data: goal, label: this.goalLabel, type: 'line', fill: false, pointRadius: 0, borderColor: '#a31d70' }
      ];
    }
    this.isBusy = false;
  }

  calcDistance(aggregate): number {
    const distance = aggregate.totalDistance / 1000;

    return Math.round(distance * 10) / 10;
  }
}

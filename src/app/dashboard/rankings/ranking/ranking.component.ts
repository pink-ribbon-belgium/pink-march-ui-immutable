//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AccountService } from '../../../api/account/account.service';
import { AccountAggregateService } from '../../../api/aggregates/account-aggregate.service';
import { TeamAggregateService } from '../../../api/aggregates/team-aggregate.service';
import { AuthService } from '../../../auth/auth.service';
import { selectGroupId, selectSubgroupId } from '../../../navigation/store/reducers/nav.reducer';
import { NavState } from '../../../navigation/store/state/nav.state';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../../shared/constant/auth0.constants';
import { generalConstant } from '../../../shared/constant/general.constants';
import { AccountAggregate } from '../../../types/aggregates/account-aggregate';
import { TeamAggregate } from '../../../types/aggregates/team-aggregate';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('button', { static: false }) toggleButton;
  isLoading = true;
  accountId: string;
  groupId: string;
  groupType: string;
  accountAggregate: AccountAggregate;
  teamAggregate: TeamAggregate;
  name: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly auth: AuthService,
    private readonly aggregateService: AccountAggregateService,
    private readonly teamAggregateService: TeamAggregateService,
    private readonly store: Store<NavState>
  ) {
    super();
  }

  ngOnInit(): void {
    this.route.params.subscribe(url => {
      this.groupId = url[generalConstant.groupId];
      this.groupType = url[generalConstant.groupType];
    });
    if (this.groupType === 'team') {
      this.subscriptions.add(
        this.teamAggregateService.getTeamAggregate(this.groupId).subscribe(result => {
          this.teamAggregate = result;
        })
      );
    } else {
      this.store.select(selectSubgroupId).subscribe(result => {
        if (result) {
          this.teamAggregateService.getTeamAggregate(result).subscribe(value => {
            this.teamAggregate = value;
          });
        }
      });
    }
    this.subscriptions.add(
      this.auth.userProfile$.subscribe(user => {
        this.accountId = user[auth0Constant.accountId];
      })
    );
    this.subscriptions.add(
      this.aggregateService
        .getAccountAggregate(this.accountId)
        .pipe(
          finalize(() => {
            this.isLoading = false;
          })
        )
        .subscribe(result => {
          this.accountAggregate = result;
          this.name = this.accountAggregate.name;
          this.toggleButton.checked = true;
        })
    );
  }
}

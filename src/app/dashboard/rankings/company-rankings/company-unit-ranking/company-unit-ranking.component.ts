import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { CompanyAggregateService } from '../../../../api/aggregates/company-aggregate.service';
import { CompanyService } from '../../../../api/company/company.service';
import { UnsubscribeOnDestroyAdapter } from '../../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { CompanyAggregate } from '../../../../types/aggregates/company-aggregate';

@Component({
  selector: 'app-company-unit-ranking',
  templateUrl: './company-unit-ranking.component.html',
  styleUrls: ['./company-unit-ranking.component.scss']
})
export class CompanyUnitRankingComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;
  @Input() companyId: string;
  @Input() companyAggregate: CompanyAggregate;

  url: string;
  isLoading = true;
  companyAggregates: Array<CompanyAggregate>;
  companyDataSource = new MatTableDataSource<CompanyAggregate>();
  displayedColumns: Array<string> = ['rank', 'name', 'steps'];

  constructor(
    private readonly companyAggregateService: CompanyAggregateService,
    private readonly companyService: CompanyService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.companyAggregateService
        .getCompanyAggregatesForUnit(this.companyAggregate.unit)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(result => {
          this.setRank(result);
          this.companyAggregates = result;
          this.companyDataSource.data = result;
          this.companyDataSource.paginator = this.paginator;
          this.setLogos(this.paginator.pageIndex, this.paginator.pageSize);
        })
    );
  }

  setRank(aggregates: Array<CompanyAggregate>): void {
    let i = 1;
    aggregates.map(aggregate => (aggregate.rank = i++));
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.companyDataSource.filter = filterValue.trim().toLowerCase();

    if (this.companyDataSource.paginator) {
      this.companyDataSource.paginator.firstPage();
    }
  }

  openInfoLink(): void {
    if (this.url) {
      window.open(this.url);
    }
  }

  setLogos(pageIndex: number, pageSize: number): void {
    const startingPoint = pageIndex * pageSize;
    const end = startingPoint + pageSize;
    const aggregatesToCheck = this.companyAggregates.slice(startingPoint, end);
    const aggregatesWithLogo = aggregatesToCheck.filter(item => item.hasLogo);
    if (aggregatesWithLogo.length) {
      aggregatesWithLogo.map(aggregate => {
        if (!aggregate.logo) {
          this.companyService.getCompanyProfileById(aggregate.companyId).subscribe(profile => {
            aggregate.logo = `data:image/jpeg;base64,${profile.logo}`;
          });
        }
      });
    }
  }

  getLogos(event): void {
    this.setLogos(event.pageIndex, event.pageSize);
  }
}

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { CompanyAggregateService } from '../../../../api/aggregates/company-aggregate.service';
import { CompanyService } from '../../../../api/company/company.service';
import { UnsubscribeOnDestroyAdapter } from '../../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { CompanyAggregate } from '../../../../types/aggregates/company-aggregate';

@Component({
  selector: 'app-global-company-ranking',
  templateUrl: './global-company-ranking.component.html',
  styleUrls: ['./global-company-ranking.component.scss']
})
export class GlobalCompanyRankingComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;
  @Input() companyId: string;
  @Input() companyAggregate: CompanyAggregate;

  isLoading = true;
  companyAggregates: Array<CompanyAggregate>;
  companyDataSource = new MatTableDataSource<CompanyAggregate>();
  displayedColumns: Array<string> = ['rank', 'name', 'steps', 'actions'];

  constructor(
    private readonly companyAggregateService: CompanyAggregateService,
    private readonly companyService: CompanyService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.companyAggregateService
        .getAllCompanyAggregates()
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(result => {
          this.setRank(result);
          this.companyAggregates = result;
          this.companyDataSource.data = result;
          this.companyDataSource.paginator = this.paginator;
          this.showPage();
          this.setLogos(this.paginator.pageIndex, this.paginator.pageSize);
        })
    );
  }

  setRank(aggregates: Array<CompanyAggregate>): void {
    let i = 1;
    aggregates.map(aggregate => (aggregate.rank = i++));
  }

  setPageIndex(aggregate): number {
    if (aggregate) {
      const index = this.companyAggregates.findIndex(
        companyAggregate => companyAggregate.companyId === aggregate.companyId
      );

      return Math.floor(index / 10);
    }
  }

  showPage(): void {
    this.paginator.pageIndex = this.setPageIndex(this.companyAggregate);
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  setLogos(pageIndex: number, pageSize: number): void {
    const startingPoint = pageIndex * pageSize;
    const end = startingPoint + pageSize;
    const aggregatesToCheck = this.companyAggregates.slice(startingPoint, end);
    const aggregatesWithLogo = aggregatesToCheck.filter(item => item.hasLogo);
    if (aggregatesWithLogo.length) {
      aggregatesWithLogo.map(aggregate => {
        if (!aggregate.logo) {
          this.companyService.getCompanyProfileById(aggregate.companyId).subscribe(profile => {
            aggregate.logo = `data:image/jpeg;base64,${profile.logo}`;
          });
        }
      });
    }
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.companyDataSource.filter = filterValue.trim().toLowerCase();

    if (this.companyDataSource.paginator) {
      this.companyDataSource.paginator.firstPage();
    }
  }

  getLogos(event): void {
    this.setLogos(event.pageIndex, event.pageSize);
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { CompanyAggregate } from '../../../../types/aggregates/company-aggregate';

@Component({
  selector: 'app-individual-company-ranking',
  templateUrl: './individual-company-ranking.component.html',
  styleUrls: ['./individual-company-ranking.component.scss']
})
export class IndividualCompanyRankingComponent implements OnInit {
  @Input() readonly companyAggregate: CompanyAggregate;
  totalDistance: number;

  ngOnInit(): void {
    if (this.companyAggregate) {
      this.totalDistance = this.calcDistance(this.companyAggregate);
    }
  }

  calcDistance(aggregate): number {
    const distance = aggregate.totalDistance / 1000;

    return Math.round(distance * 10) / 10;
  }
}

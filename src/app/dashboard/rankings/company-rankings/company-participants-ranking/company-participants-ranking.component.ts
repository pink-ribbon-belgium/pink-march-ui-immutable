import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AccountAggregateService } from '../../../../api/aggregates/account-aggregate.service';
import { AuthService } from '../../../../auth/auth.service';
import { UnsubscribeOnDestroyAdapter } from '../../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../../../shared/constant/auth0.constants';
import { AccountAggregate } from '../../../../types/aggregates/account-aggregate';

@Component({
  selector: 'app-company-participants-ranking',
  templateUrl: './company-participants-ranking.component.html',
  styleUrls: ['./company-participants-ranking.component.scss']
})
export class CompanyParticipantsRankingComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;
  @Input() companyId: string;

  accountId: string;
  accountAggregates: Array<AccountAggregate>;
  accountAggregate: AccountAggregate;
  isLoading = true;
  accountDataSource = new MatTableDataSource<AccountAggregate>();
  displayedColumns: Array<string> = ['rank', 'name', 'steps'];

  constructor(private readonly accountAggregateService: AccountAggregateService, private readonly auth: AuthService) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.auth.userProfile$.subscribe(user => {
        this.accountId = user[auth0Constant.accountId];
      })
    );

    this.subscriptions.add(
      this.accountAggregateService.getAccountAggregate(this.accountId).subscribe(result => {
        this.accountAggregate = result;
      })
    );

    this.subscriptions.add(
      this.accountAggregateService
        .getAccountAggregatesForGroup(this.companyId)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(result => {
          this.setRank(result);
          this.accountAggregates = result;
          this.accountDataSource.data = result;
          this.accountDataSource.paginator = this.paginator;
          this.paginator.pageIndex = this.setPageIndex(this.accountAggregate);
        })
    );
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.accountDataSource.filter = filterValue.trim().toLowerCase();

    if (this.accountDataSource.paginator) {
      this.accountDataSource.paginator.firstPage();
    }
  }

  setRank(aggregates: Array<AccountAggregate>): void {
    let i = 1;
    aggregates.map(aggregate => (aggregate.rank = i++));
  }

  setPageIndex(aggregate): number {
    if (aggregate) {
      const index = this.accountAggregates.findIndex(
        accountAggregate => accountAggregate.accountId === aggregate.accountId
      );

      return Math.floor(index / 10);
    }
  }
}

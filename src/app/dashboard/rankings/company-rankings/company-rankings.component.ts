import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { finalize } from 'rxjs/operators';
import { CompanyAggregateService } from '../../../api/aggregates/company-aggregate.service';
import { TeamAggregateService } from '../../../api/aggregates/team-aggregate.service';
import { selectSubgroupId } from '../../../navigation/store/reducers/nav.reducer';
import { NavState } from '../../../navigation/store/state/nav.state';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { generalConstant } from '../../../shared/constant/general.constants';
import { CompanyAggregate } from '../../../types/aggregates/company-aggregate';
import { TeamAggregate } from '../../../types/aggregates/team-aggregate';

// tslint:disable:template-cyclomatic-complexity //

@Component({
  selector: 'app-company-rankings',
  templateUrl: './company-rankings.component.html',
  styleUrls: ['./company-rankings.component.scss']
})
export class CompanyRankingsComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('button', { static: false }) toggleButton;
  groupId: string;
  groupType: string;
  companyAggregate: CompanyAggregate;
  teamAggregate: TeamAggregate;
  isLoading = true;
  isLoadingTeam: boolean;
  unit: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly companyAggregateService: CompanyAggregateService,
    private readonly teamAggregateService: TeamAggregateService,
    private readonly store: Store<NavState>
  ) {
    super();
  }

  ngOnInit(): void {
    this.route.params.subscribe(url => {
      this.groupId = url[generalConstant.groupId];
    });
    this.subscriptions.add(
      this.companyAggregateService
        .getCompanyAggregate(this.groupId)
        .pipe(
          finalize(() => {
            this.isLoading = false;
            this.toggleButton.checked = true;
          })
        )
        .subscribe(result => {
          this.companyAggregate = result;
          this.unit = result.unit;
        })
    );
    this.store.select(selectSubgroupId).subscribe(result => {
      if (result) {
        this.isLoadingTeam = true;
        this.teamAggregateService
          .getTeamAggregate(result)
          .pipe(finalize(() => (this.isLoadingTeam = false)))
          .subscribe(value => {
            this.teamAggregate = value;
          });
      }
    });
  }
}

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AccountAggregateService } from '../../../api/aggregates/account-aggregate.service';
import { TeamAggregateService } from '../../../api/aggregates/team-aggregate.service';
import { SubgroupService } from '../../../api/subgroup/subgroup.service';
import { TeamService } from '../../../api/team/team.service';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { AccountAggregate } from '../../../types/aggregates/account-aggregate';
import { TeamAggregate } from '../../../types/aggregates/team-aggregate';

// tslint:disable:template-cyclomatic-complexity //

@Component({
  selector: 'app-team-ranking',
  templateUrl: './team-ranking.component.html',
  styleUrls: ['./team-ranking.component.scss']
})
export class TeamRankingComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('accountPaginator', { static: true }) accountPaginator: MatPaginator;
  @ViewChild('teamPaginator', { static: true }) teamPaginator: MatPaginator;
  @Input() companyId: string;
  @Input() teamAggregate: TeamAggregate;
  @Input() accountAggregate: AccountAggregate;

  accountAggregates: Array<AccountAggregate>;
  teamAggregates: Array<TeamAggregate>;
  accountDataSource = new MatTableDataSource<AccountAggregate>();
  teamDataSource = new MatTableDataSource<TeamAggregate>();
  displayedColumns: Array<string> = ['rank', 'name', 'steps'];
  displayedColumnsTeam: Array<string> = ['rank', 'name', 'steps', 'actions'];
  isLoading = true;
  url: string;

  constructor(
    private readonly teamAggregateService: TeamAggregateService,
    private readonly accountAggregateService: AccountAggregateService,
    private readonly teamService: TeamService,
    private readonly subgroupService: SubgroupService
  ) {
    super();
  }

  ngOnInit(): void {
    if (this.teamAggregate.companyId) {
      this.setCompanyAggregates();
    } else {
      this.setTeamAggregates();
    }
  }

  applyFilter(event: Event, dataSource): void {
    const filterValue = (event.target as HTMLInputElement).value;
    dataSource.filter = filterValue.trim().toLowerCase();

    if (dataSource.paginator) {
      dataSource.paginator.firstPage();
    }
  }

  setTeamAggregates(): void {
    this.subscriptions.add(
      forkJoin(
        this.accountAggregateService.getAccountAggregatesForGroup(this.teamAggregate.teamAggregateId),
        this.teamAggregateService.getAllTeamAggregates()
      )
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(([accountAggregates, teamAggregates]) => {
          this.setAccountAggregatesRank(accountAggregates);
          this.setTeamAggregatesRank(teamAggregates);
          this.assignDataSources(accountAggregates, teamAggregates);
        })
    );
  }

  setCompanyAggregates(): void {
    this.subscriptions.add(
      forkJoin(
        this.accountAggregateService.getAccountAggregatesForSubgroup(this.teamAggregate.teamAggregateId),
        this.teamAggregateService.getAllTeamAggregates()
      )
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(([accountAggregates, teamAggregates]) => {
          this.setAccountAggregatesRank(accountAggregates);
          this.setTeamAggregatesRank(teamAggregates);
          this.assignDataSources(accountAggregates, teamAggregates);
        })
    );
  }

  assignDataSources(accountAggregates, teamAggregates): void {
    this.accountAggregates = accountAggregates;
    this.accountDataSource.data = accountAggregates;
    this.accountPaginator.length = accountAggregates.length;
    this.accountDataSource.paginator = this.accountPaginator;
    this.accountPaginator.pageIndex = this.setPageIndex(this.accountAggregate, 'account');
    this.accountPaginator._changePageSize(this.accountPaginator.pageSize);

    this.teamAggregates = teamAggregates;
    this.teamDataSource.data = teamAggregates;
    this.teamPaginator.length = teamAggregates.length;
    this.teamDataSource.paginator = this.teamPaginator;
    this.teamPaginator.pageIndex = this.setPageIndex(this.teamAggregate, 'team');
    this.teamPaginator._changePageSize(this.teamPaginator.pageSize);
  }

  setName(teamAggregate: TeamAggregate): string {
    if (teamAggregate.extendedName) {
      return teamAggregate.extendedName;
    }

    return teamAggregate.name;
  }

  setAccountAggregatesRank(aggregates: Array<AccountAggregate>): void {
    let i = 1;
    aggregates.map(aggregate => (aggregate.rank = i++));
  }

  setTeamAggregatesRank(aggregates: Array<TeamAggregate>): void {
    let i = 1;
    aggregates.map(aggregate => (aggregate.rank = i++));
  }

  showPage(): void {
    this.teamPaginator.pageIndex = this.setPageIndex(this.teamAggregate, 'team');
    this.teamPaginator._changePageSize(this.teamPaginator.pageSize);
  }

  setPageIndex(aggregate, aggregateType): number {
    if (aggregateType === 'team') {
      const index = this.teamAggregates.findIndex(
        teamAggregate => teamAggregate.teamAggregateId === aggregate.teamAggregateId
      );

      return Math.floor(index / 10);
    }
    if (aggregateType === 'account') {
      const index = this.accountAggregates.findIndex(
        accountAggregate => accountAggregate.accountId === aggregate.accountId
      );

      return Math.floor(index / 5);
    }
  }

  openInfoLink(): void {
    if (this.url) {
      window.open(this.url);
    }
  }

  setLogos(pageIndex: number, pageSize: number): void {
    const startingPoint = pageIndex * pageSize;
    const end = startingPoint + pageSize;
    const aggregatesToCheck = this.teamAggregates.slice(startingPoint, end);
    const aggregatesWithLogo = aggregatesToCheck.filter(item => item.hasLogo);
    if (aggregatesWithLogo.length) {
      aggregatesWithLogo.map(aggregate => {
        if (aggregate.groupType === 'Team' && !aggregate.logo) {
          this.teamService.getTeamProfileById(aggregate.teamAggregateId).subscribe(profile => {
            aggregate.logo = `data:image/jpeg;base64,${profile.logo}`;
          });
        } else if (aggregate.groupType === 'Company' && !aggregate.logo) {
          this.subgroupService.getSubgroupById(aggregate.companyId, aggregate.teamAggregateId).subscribe(subgroup => {
            aggregate.logo = `data:image/jpeg;base64,${subgroup.logo}`;
          });
        }
      });
    }
  }

  getLogos(event): void {
    this.setLogos(event.pageIndex, event.pageSize);
  }
}

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { TeamAggregateService } from '../../../api/aggregates/team-aggregate.service';
import { SubgroupService } from '../../../api/subgroup/subgroup.service';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { TeamAggregate } from '../../../types/aggregates/team-aggregate';

@Component({
  selector: 'app-company-team-ranking',
  templateUrl: './company-team-ranking.component.html',
  styleUrls: ['./company-team-ranking.component.scss']
})
export class CompanyTeamRankingComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('teamPaginator', { static: true }) teamPaginator: MatPaginator;
  @Input() readonly companyId: string;
  @Input() teamAggregate: TeamAggregate;
  @Input() hideIcon: boolean;

  isLoading = true;
  teamAggregates: Array<TeamAggregate>;
  url: string;
  companyName: string;
  teamDataSource = new MatTableDataSource<TeamAggregate>();
  displayedColumns: Array<string> = ['rank', 'name', 'steps'];

  constructor(
    private readonly teamAggregateService: TeamAggregateService,
    private readonly subgroupService: SubgroupService
  ) {
    super();
  }

  ngOnInit(): void {
    this.companyName = this.teamAggregate.extendedName
      .split('(')
      .pop()
      .split(')')[0];

    this.subscriptions.add(
      this.teamAggregateService
        .getTeamAggregatesForSubgroups(this.companyId)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(result => {
          this.setRank(result);
          this.teamAggregates = result;
          this.teamDataSource.data = result;
          this.teamPaginator.length = result.length;
          this.teamDataSource.paginator = this.teamPaginator;
          this.teamPaginator.pageIndex = this.setPageIndex(this.teamAggregate);
          this.teamPaginator._changePageSize(this.teamPaginator.pageSize);
        })
    );
  }

  setPageIndex(aggregate): number {
    if (aggregate) {
      const index = this.teamAggregates.findIndex(
        teamAggregate => teamAggregate.teamAggregateId === aggregate.teamAggregateId
      );

      return Math.floor(index / 10);
    }
  }

  setRank(aggregates: Array<TeamAggregate>): void {
    let i = 1;
    aggregates.map(aggregate => (aggregate.rank = i++));
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.teamDataSource.filter = filterValue.trim().toLowerCase();

    if (this.teamDataSource.paginator) {
      this.teamDataSource.paginator.firstPage();
    }
  }

  openInfoLink(): void {
    if (this.url) {
      window.open(this.url);
    }
  }

  setLogos(pageIndex: number, pageSize: number): void {
    const startingPoint = pageIndex * pageSize;
    const end = startingPoint + pageSize;
    const aggregatesToCheck = this.teamAggregates.slice(startingPoint, end);
    const aggregatesWithLogo = aggregatesToCheck.filter(item => item.hasLogo);
    if (aggregatesWithLogo.length) {
      aggregatesWithLogo.map(aggregate => {
        if (!aggregate.logo) {
          this.subgroupService.getSubgroupById(aggregate.companyId, aggregate.teamAggregateId).subscribe(subgroup => {
            aggregate.logo = `data:image/jpeg;base64,${subgroup.logo}`;
          });
        }
      });
    }
  }

  getLogos(event): void {
    this.setLogos(event.pageIndex, event.pageSize);
  }
}

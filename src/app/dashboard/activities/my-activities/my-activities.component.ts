//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { distinctUntilChanged, finalize, first } from 'rxjs/operators';
import { AccountService } from '../../../api/account/account.service';
import { AuthService } from '../../../auth/auth.service';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../../shared/constant/auth0.constants';
import { DefaultSnackbarService } from '../../../shared/services/default-snackbar/default-snackbar.service';
import { Activity } from '../../../types/activity/activity';
import { ActivityDialogComponent } from '../activity-dialog/activity-dialog.component';

@Component({
  selector: 'app-my-activities',
  templateUrl: './my-activities.component.html',
  styleUrls: ['./my-activities.component.scss']
})
export class MyActivitiesComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  activities: Array<Activity> = [];
  accountId: string;
  allColumns = ['date', 'steps', 'distance', 'device', 'actions'];
  displayedColumns: Array<string>;
  dataSource = new MatTableDataSource<Activity>();
  isLoading = true;
  hasActivities = false;
  takenDates: Array<string> = [];
  isHandset: boolean;

  constructor(
    private readonly accountService: AccountService,
    private readonly auth: AuthService,
    private readonly dialog: MatDialog,
    private readonly breakpointObserver: BreakpointObserver,
    private readonly translateService: TranslateService,
    private readonly snackBar: DefaultSnackbarService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.auth.userProfile$.subscribe(user => {
        this.accountId = user[auth0Constant.accountId];
      })
    );
    this.subscriptions.add(
      this.accountService.getActivities(this.accountId).subscribe(
        result => {
          this.dataSource.data = result;
          this.dataSource.paginator = this.paginator;
          this.activities = result;
          this.filterManualActivites(result);
          this.isLoading = false;
          this.hasActivities = true;
        },
        () => {
          this.isLoading = false;
        }
      )
    );
    this.subscriptions.add(
      this.breakpointObserver
        .observe(Breakpoints.Handset)
        .pipe(distinctUntilChanged())
        .subscribe(state => {
          this.displayedColumns = this.allColumns.filter(c => (state.matches ? c !== 'distance' : true));
          this.isHandset = state.matches;
        })
    );
  }

  filterManualActivites(activities: Array<Activity>): void {
    const filteredActivities = activities.filter(activity => activity.trackerType === 'manual');
    this.takenDates = filteredActivities.map(activity => activity.activityDate);
  }

  sortDatasource(): Array<Activity> {
    return this.activities.sort((a, b) => Date.parse(b.activityDate) - Date.parse(a.activityDate));
  }

  activityAction(action: string, activity?: Activity): void {
    const dialogRef = this.dialog.open(ActivityDialogComponent, {
      width: '400px',
      disableClose: true,
      data: {
        accountId: this.accountId,
        activity,
        action,
        takenDates: this.takenDates,
        isHandset: this.isHandset
      }
    });
    this.subscriptions.add(
      dialogRef.afterClosed().subscribe((result: Activity) => {
        if (result) {
          this.isLoading = true;
          this.subscriptions.add(
            this.accountService
              .updateActivity(this.accountId, result)
              .pipe(finalize(() => (this.isLoading = false)))
              .subscribe(
                val => {
                  if (result.deleted) {
                    this.activities.splice(this.activities.indexOf(activity), 1);
                    this.takenDates.splice(this.takenDates.indexOf(result.activityDate), 1);
                    if (!this.activities.length) {
                      this.hasActivities = false;
                    }
                  } else {
                    const index = this.activities.indexOf(activity);
                    const index2 = this.activities.findIndex(
                      act => act.activityDate.slice(0, 10) === result.activityDate
                    );
                    if (index !== -1) {
                      this.activities[index] = result;
                    } else if (index2 !== -1) {
                      this.activities[index2] = result;
                      this.takenDates.push(result.activityDate);
                    } else {
                      this.hasActivities = true;
                      this.activities.push(result);
                      this.takenDates.push(result.activityDate);
                    }
                  }
                  this.dataSource.data = this.sortDatasource();
                },
                err => {
                  if (err.error && err.error.statusCode === 403) {
                    this.translateService
                      .get('ACTIVITIES.ERROR_MESSAGE')
                      .pipe(first())
                      .subscribe(translation => {
                        this.snackBar.openSnackBar(translation);
                      });
                  }
                }
              )
          );
        }
      })
    );
  }

  calcDistance(activity: Activity): number {
    const distance = activity.distance / 1000;

    return Math.round(distance * 10) / 10;
  }
}

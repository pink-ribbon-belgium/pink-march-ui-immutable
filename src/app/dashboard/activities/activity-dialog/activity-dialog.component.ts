//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { select, Store } from '@ngrx/store';
import * as moment from 'moment';
import { debounceTime } from 'rxjs/operators';
import { AccountService } from '../../../api/account/account.service';
import { selectAccountProfile } from '../../../navigation/store/reducers/nav.reducer';
import { NavState } from '../../../navigation/store/state/nav.state';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { AccountProfile } from '../../../types/account/account-profile';
import { Activity } from '../../../types/activity/activity';

@Component({
  selector: 'app-activity-dialog',
  templateUrl: './activity-dialog.component.html',
  styleUrls: ['./activity-dialog.component.scss']
})
export class ActivityDialogComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  formGroup: FormGroup;
  activity: Activity;
  action: string;
  accountProfile: AccountProfile;
  disableDatePicker = false;
  today = new Date();
  maxDate = new Date();
  hasChanges = false;
  initialSteps: number;
  initialDistance: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly fb: FormBuilder,
    private readonly dialogRef: MatDialogRef<ActivityDialogComponent>,
    private readonly accountService: AccountService,
    private readonly store: Store<NavState>
  ) {
    super();
  }

  filter = (d: Date): boolean => {
    const date = moment(d);
    date.add(2, 'hours');
    const dateString = date.toISOString().slice(0, 10);

    return !this.data.takenDates.some(takenDate => takenDate === dateString);
  };

  ngOnInit(): void {
    this.setMaxDate();
    this.activity = this.data.activity;
    this.action = this.data.action;
    this.subscriptions.add(
      this.store.pipe(select(selectAccountProfile)).subscribe(val => {
        this.accountProfile = val;
      })
    );
    this.createForm();
    this.checkAction();
    this.setInitialValues();

    this.subscriptions.add(
      this.formGroup
        .get('steps')
        .valueChanges.pipe(debounceTime(500))
        .subscribe(result => {
          if (result) {
            this.accountService
              .calculateDistance({ gender: this.accountProfile.gender, steps: result, distance: 0 })
              .subscribe(val => {
                this.hasChanges = result !== this.initialSteps;
                this.formGroup.get('distance').setValue(val.distance, { emitEvent: false });
              });
          }
        })
    );
    this.subscriptions.add(
      this.formGroup
        .get('distance')
        .valueChanges.pipe(debounceTime(500))
        .subscribe(result => {
          if (result) {
            this.accountService
              .calculateDistance({ gender: this.accountProfile.gender, steps: 0, distance: result })
              .subscribe(val => {
                this.hasChanges = result !== this.initialDistance;
                this.formGroup.get('steps').setValue(val.steps, { emitEvent: false });
              });
          }
        })
    );
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      activityDate: [{ value: this.activity ? this.activity.activityDate : null, disabled: true }, Validators.required],
      steps: [this.activity ? this.activity.numberOfSteps : null, Validators.required],
      distance: [this.activity ? this.activity.distance : null, Validators.required]
    });
    if (this.activity) {
      this.disableDatePicker = true;
    }
  }

  checkAction(): void {
    if (this.action === 'delete') {
      this.formGroup.disable();
    }
  }

  setInitialValues(): void {
    this.initialSteps = this.activity ? this.activity.numberOfSteps : null;
    this.initialDistance = this.activity ? this.activity.distance : null;
  }

  saveActivity(): void {
    this.dialogRef.close(this.mapSaveableActivity());
  }

  mapSaveableActivity(): Activity {
    const activityDate = moment(this.formGroup.get('activityDate').value);
    activityDate.add(2, 'hours');
    const updatedActivity = {
      accountId: this.data.accountId,
      structureVersion: 1,
      trackerType: 'manual',
      activityDate: activityDate.toISOString().slice(0, 10),
      numberOfSteps: this.formGroup.get('steps').value,
      distance: this.formGroup.get('distance').value,
      deleted: this.action === 'delete'
    };

    return updatedActivity;
  }

  setMaxDate(): void {
    const endDate = new Date(2021, 4, 31);
    if (this.today.getTime() > endDate.getTime()) {
      this.maxDate = endDate;
    }
  }
}

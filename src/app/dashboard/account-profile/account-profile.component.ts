import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { finalize, flatMap } from 'rxjs/operators';
import { AccountProfileService } from '../../api/account-profile/account-profile.service';
import { AuthState } from '../../auth/store/reducers/auth.reducer';
import * as navActions from '../../navigation/store/actions/nav.action';
import { selectAccountProfile } from '../../navigation/store/reducers/nav.reducer';
import { NavState } from '../../navigation/store/state/nav.state';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';
import { AccountProfile } from '../../types/account/account-profile';
import { UserProfile } from '../../types/auth/user-profile';

@Component({
  selector: 'app-account-profile',
  templateUrl: './account-profile.component.html',
  styleUrls: ['./account-profile.component.scss']
})
export class AccountProfileComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  formGroup: FormGroup;
  accountId: string;
  accountProfile: AccountProfile;
  userProfile: UserProfile;
  isLoading: boolean;
  isSaving: boolean;

  constructor(
    private readonly fb: FormBuilder,
    private readonly store: Store<AuthState>,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly accountProfileService: AccountProfileService,
    private readonly translateService: TranslateService,
    private readonly snackBar: DefaultSnackbarService,
    private readonly navStore: Store<NavState>
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.getAccountProfile();
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      zip: ['', [Validators.required, Validators.pattern('^\\d{4}$')]]
    });
  }

  getAccountProfile(): void {
    this.isLoading = true;
    this.subscriptions.add(
      this.navStore.pipe(select(selectAccountProfile)).subscribe(result => {
        if (result) {
          this.accountProfile = result;
          this.setFormValues(result);
          this.isLoading = false;
        }
      })
    );
  }

  setFormValues(accountProfile: AccountProfile): void {
    this.formGroup.setValue({
      firstName: accountProfile.firstName,
      lastName: accountProfile.lastName,
      gender: accountProfile.gender,
      dateOfBirth: accountProfile.dateOfBirth,
      zip: accountProfile.zip
    });
  }

  saveProfile(): void {
    this.isLoading = true;
    const savableData = { ...this.accountProfile, ...this.formGroup.getRawValue() };
    delete savableData.links;
    this.subscriptions.add(
      this.accountProfileService
        .saveAccountProfile(savableData)
        .pipe(
          flatMap(() => this.translateService.get('ACCOUNT_PROFILE.SNACK_BAR.SAVED')),
          finalize(() => (this.isLoading = false))
        )
        .subscribe(successMessage => {
          this.navStore.dispatch(navActions.saveProfileSuccess({ profileLoaded: true, accountProfile: savableData }));
          this.snackBar.openSnackBar(successMessage);
        })
    );
  }
}

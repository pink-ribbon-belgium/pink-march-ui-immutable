import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { AccountModule } from '../../account/account.module';
import { AccountProfileService } from '../../api/account-profile/account-profile.service';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { InfoTitleComponent } from '../../info-title/info-title.component';
import { initialNavState, NavState } from '../../navigation/store/state/nav.state';
import { sharedModules } from '../../shared/constant/shared-modules';
import { AccountProfileComponent } from './account-profile.component';

describe('AccountProfileComponent', () => {
  let component: AccountProfileComponent;
  let fixture: ComponentFixture<AccountProfileComponent>;
  let accountProfileService: AccountProfileService;
  let router: Router;
  let store: MockStore<NavState>;
  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  const accountId = 'account-id';
  const accountProfile = {
    accountId: 'account-id',
    firstName: 'Eric',
    lastName: 'VanRustigen',
    gender: 'x',
    dateOfBirth: '1992-10-24T23:00:00.000Z',
    zip: '2500',
    structureVersion: 1
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountProfileComponent, InfoTitleComponent],
      imports: [RouterTestingModule.withRoutes([]), ...appModules, ...sharedModules, AccountModule],
      providers: [
        provideMockStore({ initialState: { nav: initialNavState } }),
        {
          provide: Router,
          useValue: mockRouter
        },
        {
          provide: AuthService,
          useValue: {}
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ accountId })
          }
        }
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountProfileComponent);
    component = fixture.componentInstance;
    accountProfileService = TestBed.get<AccountProfileService>(AccountProfileService);
    router = TestBed.get<Router>(Router);
    store = TestBed.get<Store<NavState>>(Store);
    spyOn(accountProfileService, 'saveAccountProfile').and.returnValue(of(null));
    spyOn(accountProfileService, 'getAccountProfileByAccountId').and.returnValue(of(accountProfile));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call saveAccountProfile when save button is clicked', async () => {
    fixture.detectChanges();
    await fixture.whenStable();
    component.saveProfile();
    expect(accountProfileService.saveAccountProfile).toHaveBeenCalled();
  });
});

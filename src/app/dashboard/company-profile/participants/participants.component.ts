import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { GroupSlots } from '../../../types/group/group-slots';

@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.scss']
})
export class ParticipantsComponent implements OnChanges {
  @Input() slotsLink: string;
  @Output() readonly isBusy = new EventEmitter<boolean>();

  slots: GroupSlots;
  isLoading: boolean;

  constructor(private readonly http: HttpClient) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.slotsLink && changes.slotsLink.currentValue) {
      this.isLoading = true;
      this.isBusy.emit(true);
      this.http
        .get<GroupSlots>(this.slotsLink)
        .pipe(
          finalize(() => {
            this.isLoading = false;
            this.isBusy.emit(false);
          })
        )
        .subscribe(res => (this.slots = res));
    }
  }
}

import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { appModules } from '../../../app-modules.constant';
import { AuthService } from '../../../auth/auth.service';
import { sharedModules } from '../../../shared/constant/shared-modules';
import { ParticipantsComponent } from './participants.component';

describe('ParticipantsComponent', () => {
  let component: ParticipantsComponent;
  let fixture: ComponentFixture<ParticipantsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ParticipantsComponent],
      imports: [...appModules, ...sharedModules],
      providers: [{ provide: AuthService, useValue: {} }],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantsComponent);
    component = fixture.componentInstance;
  });

  it('should work', () => {
    expect(component).toBeTruthy();
  });

  it('should get slots when slotsLink changes', async () => {
    const slots = {
      totalSlots: 1,
      takenSlots: 1,
      availableSlots: 0
    };
    const http = TestBed.get<HttpClient>(HttpClient);
    spyOn(http, 'get').and.returnValue(of(slots));
    component.ngOnChanges({ slotsLink: new SimpleChange(null, 'linkje', true) });
    await fixture.whenStable();
    expect(component.slots).toEqual(slots);
  });

  it('should leave slots undefined when changes happens without slotsLink', () => {
    component.ngOnChanges({});
    expect(component.slots).toBeUndefined();
  });
});

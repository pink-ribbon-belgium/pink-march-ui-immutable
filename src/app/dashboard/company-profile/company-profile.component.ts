import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { finalize, flatMap, tap } from 'rxjs/operators';
import { CompanyService } from '../../api/company/company.service';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { generalConstant } from '../../shared/constant/general.constants';
import { localStorageConstant } from '../../shared/constant/local-storage.constants';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';
import { Company } from '../../types/company/company';
import { GroupProfile } from '../../types/group/group-profile';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.scss']
})
export class CompanyProfileComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  companyId: string;
  company: Company;
  companyProfile: GroupProfile;
  formGroup: FormGroup;
  isSaving: boolean;
  groupType = generalConstant.companyGroupType;
  isLoadingSlots: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly companyService: CompanyService,
    private readonly fb: FormBuilder,
    private readonly snackBar: DefaultSnackbarService,
    private readonly translate: TranslateService,
    private readonly router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.route.params
        .pipe(
          tap(params => {
            this.companyId = params.companyId;
          }),
          flatMap(() =>
            forkJoin(
              this.companyService.getCompanyById(this.companyId),
              this.companyService.getCompanyProfileById(this.companyId)
            )
          )
        )
        .subscribe(([company, companyProfile]) => {
          this.company = company;
          this.companyProfile = companyProfile;
          this.saveToLocalStorage();
          this.createFormGroup();
        })
    );
  }

  createFormGroup(): void {
    this.formGroup = this.fb.group({
      name: [this.companyProfile.name, Validators.required],
      address: [this.company.address, Validators.required],
      zip: [this.company.zip, [Validators.required, Validators.pattern('^\\d{4}$')]],
      city: [this.company.city, Validators.required],
      vat: [this.company.vat, [Validators.required, Validators.pattern('^BE\\d{10}$')]],
      reference: [this.company.reference],
      logo: [this.companyProfile.logo],
      contactFirstName: [this.company.contactFirstName, Validators.required],
      contactLastName: [this.company.contactLastName, Validators.required],
      contactTelephone: [this.company.contactTelephone, [Validators.required, Validators.pattern('^\\+\\d{10,20}$')]],
      contactEmail: [
        { value: this.company.contactEmail, disabled: !!this.company.contactEmail },
        [Validators.required, Validators.pattern('^\\b[+\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b$')]
      ]
    });
  }

  saveProfile(): void {
    this.isSaving = true;
    const formValue = this.formGroup.getRawValue();
    const savableProfile = {
      ...this.companyProfile,
      name: formValue.name,
      logo: formValue.logo
    };
    delete savableProfile.links;
    const savableCompany = {
      ...this.company,
      address: formValue.address,
      zip: formValue.zip,
      city: formValue.city,
      vat: formValue.vat,
      reference: formValue.reference,
      contactFirstName: formValue.contactFirstName,
      contactLastName: formValue.contactLastName,
      contactTelephone: formValue.contactTelephone,
      contactEmail: formValue.contactEmail
    };
    delete savableCompany.links;
    delete savableCompany.joinLink;
    this.subscriptions.add(
      forkJoin(
        this.companyService.updateCompany(savableCompany),
        this.companyService.saveCompanyProfile(savableProfile)
      )
        .pipe(
          flatMap(() => this.translate.get('COMPANY.SNACK_BAR.PROFILE_UPDATED')),
          finalize(() => (this.isSaving = false))
        )
        .subscribe(successMessage => {
          this.snackBar.openSnackBar(successMessage);
          this.formGroup.reset(formValue);
        })
    );
  }

  buyAdditional(): void {
    void this.router.navigate(['/register/payment-preparation']);
  }

  saveToLocalStorage(): void {
    localStorage.setItem(localStorageConstant.profileType, 'Company');
    localStorage.setItem(localStorageConstant.groupId, this.companyId);
    localStorage.setItem(localStorageConstant.flow, 'additional');
  }

  setIsLoadingSLots(event): void {
    this.isLoadingSlots = event;
  }

  goToMembersOverview(): void {
    void this.router.navigate([`/dashboard/company/${this.companyId}/overview`]);
  }

  goToSubgroupOverview(): void {
    void this.router.navigate([`/dashboard/company/${this.companyId}/company-teams`]);
  }
}

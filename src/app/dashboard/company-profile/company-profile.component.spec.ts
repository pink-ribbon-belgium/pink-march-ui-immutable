import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { CompanyService } from '../../api/company/company.service';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { CompanyContactComponent } from '../../company/company-contact/company-contact.component';
import { CompanyDetailsComponent } from '../../company/company-details/company-details.component';
import { JoinLinkComponent } from '../../group/join-link/join-link.component';
import { generalConstant } from '../../shared/constant/general.constants';
import { sharedModules } from '../../shared/constant/shared-modules';
import { CompanyProfileComponent } from './company-profile.component';

describe('CompanyProfileComponent', () => {
  let component: CompanyProfileComponent;
  let fixture: ComponentFixture<CompanyProfileComponent>;
  let companyService: CompanyService;
  let router: Router;
  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  const companyId = 'company-id';
  const company = {
    id: '72bd9328-3e3e-46da-93da-bff6a66d275d',
    address: 'this is an address 12',
    zip: '2500',
    city: 'Lier',
    vat: 'BE1234567890',
    logo: '',
    contactFirstName: 'Jos',
    contactLastName: 'Pros',
    contactEmail: 'email@example.com',
    contactTelephone: '+32498765432',
    code: 'CODE',
    groupType: generalConstant.companyGroupType,
    structureVersion: 1,
    links: {
      administrators: '/I/company/aabdc43d-6427-428b-9e4d-3830ec6ba5b5/administrators',
      slots: '/I/company/aabdc43d-6427-428b-9e4d-3830ec6ba5b5/slots',
      payments: '/I/company/aabdc43d-6427-428b-9e4d-3830ec6ba5b5/payments',
      publicProfile: '/I/company/aabdc43d-6427-428b-9e4d-3830ec6ba5b5/publicProfile'
    }
  };
  const companyProfile = {
    id: '72bd9328-3e3e-46da-93da-bff6a66d275d',
    name: 'My Company',
    structureVersion: 1
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyProfileComponent, CompanyDetailsComponent, CompanyContactComponent, JoinLinkComponent],
      imports: [...appModules, ...sharedModules],
      providers: [
        { provide: Router, useValue: mockRouter },
        { provide: AuthService, useValue: {} },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ companyId })
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyProfileComponent);
    component = fixture.componentInstance;
    companyService = TestBed.get<CompanyService>(CompanyService);
    router = TestBed.get<Router>(Router);
    spyOn(companyService, 'getCompanyById').and.returnValue(of(company));
    spyOn(companyService, 'getCompanyProfileById').and.returnValue(of(companyProfile));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call companyService methods with route param', async () => {
    fixture.detectChanges();
    await fixture.whenStable();
    expect(companyService.getCompanyById).toHaveBeenCalledWith(companyId);
  });

  it('should call createFormGroup when data is loaded', async () => {
    spyOn(component, 'createFormGroup').and.callThrough();
    fixture.detectChanges();
    await fixture.whenStable();
    expect(component.createFormGroup).toHaveBeenCalled();
  });

  it('should reset formGroup after saving', async () => {
    fixture.detectChanges();
    await fixture.whenStable();
    spyOn(companyService, 'updateCompany').and.returnValue(of(null));
    spyOn(companyService, 'saveCompanyProfile').and.returnValue(of(null));
    spyOn(component.formGroup, 'reset');
    component.saveProfile();
    await fixture.whenStable();
    expect(component.formGroup.reset).toHaveBeenCalled();
  });

  it('should navigate back when buyadditionalslots is called ', async () => {
    fixture.detectChanges();
    component.buyAdditional();
    await fixture.whenStable();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/register/payment-preparation']);
  });

  it('should have called setIsLoadingsSlots', async () => {
    fixture.detectChanges();
    component.setIsLoadingSLots(true);
    await fixture.whenStable();
    expect(component.isLoadingSlots).toEqual(true);
  });
});

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, flatMap, tap } from 'rxjs/operators';
import { CompanyService } from '../../api/company/company.service';
import { AuthService } from '../../auth/auth.service';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../shared/constant/auth0.constants';
import { Subgroup } from '../../types/company/subgroup';

@Component({
  selector: 'app-company-overview',
  templateUrl: './company-team-overview.component.html',
  styleUrls: ['./company-team-overview.component.scss']
})
export class CompanyTeamOverviewComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;
  @ViewChild('button', { static: false }) toggleButton;

  isLoading = true;
  formGroup: FormGroup;
  companyId: string;
  accountId: string;
  companyTeams: Array<Subgroup>;
  dataSource = new MatTableDataSource<any>(); // Add dto
  displayedColumns: Array<string> = ['name', 'actions'];

  constructor(
    private readonly fb: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly companyService: CompanyService,
    private readonly router: Router,
    public authService: AuthService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.authService.userProfile$.subscribe(userProfile => (this.accountId = userProfile[auth0Constant.accountId]))
    );
    this.subscriptions.add(
      this.route.params
        .pipe(
          tap(params => {
            this.companyId = params.groupId;
          }),
          flatMap(() =>
            this.companyService.getCompanyTeams(this.companyId).pipe(
              finalize(() => {
                this.isLoading = false;
              })
            )
          )
        )
        .subscribe(companyTeams => {
          this.companyTeams = companyTeams;
          this.dataSource.data = companyTeams;
          this.paginator.length = companyTeams.length;
          this.dataSource.paginator = this.paginator;
        })
    );
  }

  goToCompanyTeamMembersOverview(subgroup: Subgroup): void {
    void this.router.navigate([`/dashboard/company/${this.companyId}/company-team/${subgroup.id}/overview`]);
  }

  back(): void {
    void this.router.navigate([`/dashboard/company/${this.companyId}`]);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { appModules } from '../../../app-modules.constant';
import { AuthService } from '../../../auth/auth.service';
import { UploadImageComponent } from '../../../group/upload-image/upload-image.component';
import { sharedModules } from '../../../shared/constant/shared-modules';
import { TeamFormComponent } from './team-form.component';

describe('TeamFormComponent', () => {
  let component: TeamFormComponent;
  let fixture: ComponentFixture<TeamFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TeamFormComponent, UploadImageComponent],
      imports: [...appModules, ...sharedModules],
      providers: [
        {
          provide: AuthService,
          useValue: {}
        }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamFormComponent);
    component = fixture.componentInstance;
    component.formGroup = new FormGroup({
      teamName: new FormControl('', Validators.required),
      logo: new FormControl('')
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

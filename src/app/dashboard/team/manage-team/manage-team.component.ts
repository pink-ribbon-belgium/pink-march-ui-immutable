import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { finalize, flatMap, tap } from 'rxjs/operators';
import { TeamService } from '../../../api/team/team.service';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/adapter/unsubscribe-on-destroy-adapter';
import { generalConstant } from '../../../shared/constant/general.constants';
import { localStorageConstant } from '../../../shared/constant/local-storage.constants';
import { DefaultSnackbarService } from '../../../shared/services/default-snackbar/default-snackbar.service';
import { GroupProfile } from '../../../types/group/group-profile';
import { Team } from '../../../types/team/team';

@Component({
  selector: 'app-manage-team',
  templateUrl: './manage-team.component.html',
  styleUrls: ['./manage-team.component.scss']
})
export class ManageTeamComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  formGroup: FormGroup;
  teamId: string;
  team: Team;
  teamProfile: GroupProfile;
  isSaving: boolean;
  groupType = generalConstant.teamGroupType;
  isLoadingSlots: boolean;
  members;
  memberList: string;

  constructor(
    private readonly fb: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly teamService: TeamService,
    private readonly snackBar: DefaultSnackbarService,
    private readonly translate: TranslateService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.route.params
        .pipe(
          tap(params => {
            this.teamId = params.teamId;
          }),
          flatMap(() =>
            forkJoin(this.teamService.getTeamById(this.teamId), this.teamService.getTeamProfileById(this.teamId))
          )
        )
        .subscribe(([team, teamProfile]) => {
          this.team = team;
          this.teamProfile = teamProfile;
          this.saveToLocalStorage();
          this.createForm();
        })
    );

    this.subscriptions.add(
      this.teamService.getMembers(this.teamId).subscribe(members => {
        this.members = members;
        this.showMembers(members);
      })
    );
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      teamName: [this.teamProfile.name, Validators.required],
      logo: [this.teamProfile.logo]
    });
  }

  saveTeam(): void {
    this.isSaving = true;
    const formValue = this.formGroup.getRawValue();
    const savableProfile = {
      ...this.teamProfile,
      name: formValue.teamName,
      logo: formValue.logo
    };
    delete savableProfile.links;
    this.subscriptions.add(
      this.teamService
        .saveTeamProfile(savableProfile)
        .pipe(
          flatMap(() => this.translate.get('COMPANY.SNACK_BAR.PROFILE_UPDATED')),
          finalize(() => (this.isSaving = false))
        )
        .subscribe(successMessage => {
          this.snackBar.openSnackBar(successMessage);
          this.formGroup.reset(formValue);
        })
    );
  }

  buyAdditional(): void {
    void this.router.navigate(['/register/payment-preparation']);
  }

  saveToLocalStorage(): void {
    localStorage.setItem(localStorageConstant.profileType, 'Team');
    localStorage.setItem(localStorageConstant.groupId, this.teamId);
    localStorage.setItem(localStorageConstant.flow, 'additional');
  }

  setIsLoadingSLots(event): void {
    this.isLoadingSlots = event;
  }

  showMembers(members): void {
    const memberList = members.map(m => m.name);
    this.memberList = memberList.join(', ');
  }

  goToMembersOverview(): void {
    void this.router.navigate([`/dashboard/team/${this.teamId}/overview`]);
  }
}

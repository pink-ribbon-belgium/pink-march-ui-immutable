import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { TeamService } from '../../../api/team/team.service';
import { appModules } from '../../../app-modules.constant';
import { AuthService } from '../../../auth/auth.service';
import { JoinLinkComponent } from '../../../group/join-link/join-link.component';
import { generalConstant } from '../../../shared/constant/general.constants';
import { sharedModules } from '../../../shared/constant/shared-modules';
import { TeamFormComponent } from '../team-form/team-form.component';
import { ManageTeamComponent } from './manage-team.component';

describe('ManageTeamComponent', () => {
  let component: ManageTeamComponent;
  let fixture: ComponentFixture<ManageTeamComponent>;
  let teamService: TeamService;
  let router: Router;
  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  const teamId = 'team-id';
  const team = {
    id: teamId,
    groupType: generalConstant.companyGroupType,
    code: 'CODE',
    structureVersion: 1,
    links: {
      administrators: '/I/team/29a41fda-3234-4639-8c97-dc61d4309410/administrators',
      slots: '/I/team/29a41fda-3234-4639-8c97-dc61d4309410/slots',
      payments: '/I/team/29a41fda-3234-4639-8c97-dc61d4309410/payments',
      publicProfile: '/I/team/29a41fda-3234-4639-8c97-dc61d4309410/publicProfile'
    }
  };

  const teamProfile = {
    id: teamId,
    name: 'Test-team',
    structureVersion: 1
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ManageTeamComponent, TeamFormComponent, JoinLinkComponent],
      imports: [...appModules, ...sharedModules],
      providers: [
        { provide: Router, useValue: mockRouter },
        { provide: AuthService, useValue: {} },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ teamId })
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTeamComponent);
    component = fixture.componentInstance;
    teamService = TestBed.get<TeamService>(TeamService);
    router = TestBed.get<Router>(Router);
    spyOn(teamService, 'getTeamById').and.returnValue(of(team));
    spyOn(teamService, 'getTeamProfileById').and.returnValue(of(teamProfile));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  //
  // it('should call teamService with route param', async () => {
  //   fixture.detectChanges();
  //   await fixture.whenStable();
  //   expect(teamService.getTeamById).toHaveBeenCalledWith(teamId);
  // });
  //
  // it('should call createForm', async () => {
  //   spyOn(component, 'createForm').and.callThrough();
  //   fixture.detectChanges();
  //   await fixture.whenStable();
  //   expect(component.createForm).toHaveBeenCalled();
  // });

  // it('should call saveTeam', async () => {
  //   fixture.detectChanges();
  //   spyOn(teamService, 'saveTeamProfile').and.returnValue(of(null));
  //   component.saveTeam();
  //   await fixture.whenStable();
  //   expect(teamService.saveTeamProfile).toHaveBeenCalled();
  // });

  // it('should navigate back when buyadditionalslots is called ', async () => {
  //   fixture.detectChanges();
  //   component.buyAdditional();
  //   await fixture.whenStable();
  //   expect(mockRouter.navigate).toHaveBeenCalledWith(['/register/payment-preparation']);
  // });

  // it('should have called setIsLoadingsSlots', async () => {
  //   fixture.detectChanges();
  //   component.setIsLoadingSLots(true);
  //   await fixture.whenStable();
  //   expect(component.isLoadingSlots).toEqual(true);
  // });
});

//     Copyright (C) 2020 PeopleWare NV
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

// tslint:disable:template-cyclomatic-complexity //

import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, debounceTime, finalize, flatMap, switchMap, tap } from 'rxjs/operators';
import { AccountService } from '../../api/account/account.service';
import { SubgroupService } from '../../api/subgroup/subgroup.service';
import { AuthService } from '../../auth/auth.service';
import * as navActions from '../../navigation/store/actions/nav.action';
import { selectSlot } from '../../navigation/store/reducers/nav.reducer';
import { NavState } from '../../navigation/store/state/nav.state';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../shared/constant/auth0.constants';
import { generalConstant } from '../../shared/constant/general.constants';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';
import { Subgroup } from '../../types/company/subgroup';
import { Slot } from '../../types/slot/slot';

@Component({
  selector: 'app-company-team',
  templateUrl: './company-team.component.html',
  styleUrls: ['./company-team.component.scss']
})
export class CompanyTeamComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  formGroup: FormGroup;
  form: FormGroup;
  isBusy: boolean;
  subgroupId: string;
  groupId: string;
  notFound = false;
  accountId: string;
  slot;
  subgroup: Subgroup;
  members;
  memberList: string;
  isSearching: boolean;
  canJoinSubgroup = false;

  constructor(
    private readonly fb: FormBuilder,
    private readonly subgroupService: SubgroupService,
    private readonly auth: AuthService,
    private readonly route: ActivatedRoute,
    private readonly location: Location,
    private readonly accountService: AccountService,
    private readonly translate: TranslateService,
    private readonly snackbar: DefaultSnackbarService,
    private readonly router: Router,
    private readonly store: Store<NavState>
  ) {
    super();
  }

  ngOnInit(): void {
    this.route.params.subscribe(url => {
      this.groupId = url[generalConstant.groupId];
      this.subgroupId = url[generalConstant.subgroupId];
    });
    this.subscriptions.add(
      this.auth.userProfile$.subscribe((userData: any) => {
        if (userData) {
          this.accountId = userData[auth0Constant.accountId];
        }
      })
    );
    if (this.subgroupId) {
      this.subscriptions.add(
        forkJoin(
          this.subgroupService.getSubgroupById(this.groupId, this.subgroupId),
          this.subgroupService.getSubgroupMembers(this.groupId, this.subgroupId)
        ).subscribe(([subgroup, members]) => {
          this.subgroup = subgroup;
          this.members = members;
          this.showMembers(members);
          this.createFormGroup();
        })
      );
    } else {
      this.createForm();
      this.createFormGroup();
      this.subscriptions.add(
        this.store.pipe(select(selectSlot)).subscribe((result: Slot) => {
          if (result) {
            this.slot = result;
            this.groupId = result.groupId;
            this.isBusy = false;
          }
        })
      );

      this.subscriptions.add(
        this.form
          .get('subgroupId')
          .valueChanges.pipe(
            tap(() => (this.isSearching = true)),
            debounceTime(500),
            switchMap(() => this.checkExistingSubgroup())
          )
          .subscribe(result => {
            if (result) {
              if (result.groupId === this.slot.groupId) {
                this.canJoinSubgroup = true;
              } else {
                this.canJoinSubgroup = false;
                this.translate.get('SUBGROUP.SNACK_BAR.NOT_JOINABLE').subscribe(message => {
                  this.snackbar.openSnackBar(message);
                });
              }
            } else if (!result && this.canJoinSubgroup) {
              this.canJoinSubgroup = false;
            }
          })
      );
    }
  }

  createFormGroup(): void {
    this.formGroup = this.fb.group({
      teamName: [this.subgroup ? this.subgroup.name : null, Validators.required],
      logo: [this.subgroup ? this.subgroup.logo : null]
    });
  }

  createForm(): void {
    this.form = this.fb.group({
      subgroupId: [
        '',
        [
          Validators.required,
          Validators.pattern(/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i)
        ]
      ],
      subgroupName: ['']
    });
  }

  setFormValues(subgroup: Subgroup): void {
    this.formGroup.get('teamName').setValue(subgroup.name);
    this.formGroup.get('logo').setValue(subgroup.logo);
  }

  checkExistingSubgroup(): Observable<Subgroup> {
    this.notFound = false;
    const formControl = this.form.get('subgroupId');
    if (formControl.valid) {
      return this.subgroupService.getSubgroupById(this.groupId, formControl.value).pipe(
        catchError(() => {
          this.notFound = true;
          this.translate.get('SUBGROUP.SNACK_BAR.NOT_FOUND').subscribe(message => {
            this.snackbar.openSnackBar(message);
          });

          return of(null);
        }),
        finalize(() => (this.isSearching = false))
      );
      // tslint:disable-next-line:unnecessary-else
    } else {
      return of(null);
    }
  }

  createSubgroup(): void {
    this.isBusy = true;
    const subGroup = {
      groupId: this.groupId,
      name: this.formGroup.get('teamName').value,
      logo: this.formGroup.get('logo').value,
      structureVersion: 1,
      groupType: 'Company',
      accountId: this.accountId,
      code: 'FKFG-KDKK-KSKK-KSWL' // make code optional?
    };
    this.subscriptions.add(
      this.subgroupService.createSubgroup(subGroup).subscribe(
        result => {
          this.translate.get('SUBGROUP.SNACK_BAR.CREATE_SUCCESS').subscribe(message => {
            this.snackbar.openSnackBar(message);
          });
          this.store.dispatch(navActions.saveSubgroupSuccess({ subgroupId: result.subgroupId }));
          void this.router.navigate([`/dashboard/company/${this.groupId}/company-team/${result.subgroupId}`]);
        },
        err => {
          this.isBusy = false;
          this.translate.get('SUBGROUP.SNACK_BAR.CREATE_ERROR').subscribe(message => {
            this.snackbar.openSnackBar(message);
            this.formGroup.reset();
          });
        }
      )
    );
  }

  joinSubgroup(): void {
    this.isBusy = true;
    const subgroupId = this.form.get('subgroupId').value;
    if (subgroupId) {
      const savableSlot = {
        ...this.slot,
        subgroupId
      };
      delete savableSlot.links;
      this.subscriptions.add(
        this.accountService
          .putSlot(savableSlot)
          .pipe(flatMap(() => this.translate.get('SUBGROUP.SNACK_BAR.JOIN_SUCCESS')))
          .subscribe(
            successMessage => {
              this.snackbar.openSnackBar(successMessage);
              this.form.reset();
              this.store.dispatch(navActions.joinSubgroupSuccess({ subgroupId }));
              void this.router.navigate([`/dashboard/company/${this.groupId}/company-team/${subgroupId}`]);
            },
            err => {
              this.isBusy = false;
              this.translate.get('SUBGROUP.SNACK_BAR.JOIN_ERROR').subscribe(message => {
                this.snackbar.openSnackBar(message);
                this.form.reset();
              });
            }
          )
      );
    }
  }

  saveSubgroup(): void {
    this.isBusy = true;
    const formValue = this.formGroup.getRawValue();
    const savableSubgroup = {
      ...this.subgroup,
      name: formValue.teamName,
      logo: formValue.logo
    };
    delete savableSubgroup.links;
    delete savableSubgroup.joinLink;
    this.subscriptions.add(
      this.subgroupService
        .saveSubgroup(savableSubgroup, this.groupId)
        .pipe(
          flatMap(() => this.translate.get('SUBGROUP.SNACK_BAR.UPDATED')),
          finalize(() => (this.isBusy = false))
        )
        .subscribe(successMessage => {
          this.snackbar.openSnackBar(successMessage);
          this.formGroup.reset(formValue);
        })
    );
  }

  copyJoinLink(): void {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.subgroupId;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    this.subscriptions.add(
      this.translate.get('SUBGROUP.SNACK_BAR.ID_COPIED').subscribe(message => {
        this.snackbar.openSnackBar(message);
      })
    );
  }

  showMembers(members): void {
    const memberList = members.map(m => m.name);
    this.memberList = memberList.join(', ');
  }
}

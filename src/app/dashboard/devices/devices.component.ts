import { HttpClient } from '@angular/common/http';
import { Component, Inject, InjectionToken, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { create } from 'pkce';
import { finalize, flatMap } from 'rxjs/operators';
import { DevicesService } from '../../api/devices/devices.service';
import { AuthService } from '../../auth/auth.service';
import { UnsubscribeOnDestroyAdapter } from '../../shared/adapter/unsubscribe-on-destroy-adapter';
import { auth0Constant } from '../../shared/constant/auth0.constants';
import { trackersConstants } from '../../shared/constant/trackers.constants';
import { ConfigService } from '../../shared/services/config/config.service';
import { DefaultSnackbarService } from '../../shared/services/default-snackbar/default-snackbar.service';
import { TrackerConstants } from '../../types/auth/tracker-constants';

export const LOCATION_TOKEN = new InjectionToken<Location>('Window location object');

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss'],
  providers: [{ provide: LOCATION_TOKEN, useValue: window.location }]
})
export class DevicesComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  formGroup: FormGroup;
  initialFormValue = {
    fitbit: false,
    polar: false,
    garmin: false,
    googlefit: false
  };
  trackersConstants: TrackerConstants;
  accountId: string;
  isLoading = true;
  showFitbitError: boolean;
  showGoogleFitError: boolean;
  showPolarError: boolean;

  constructor(
    private readonly fb: FormBuilder,
    private readonly http: HttpClient,
    private readonly config: ConfigService,
    private readonly auth: AuthService,
    private readonly devices: DevicesService,
    private readonly snackbar: DefaultSnackbarService,
    private readonly translate: TranslateService,
    @Inject(LOCATION_TOKEN) private readonly location: Location
  ) {
    super();
  }

  ngOnInit(): void {
    this.trackersConstants = this.config.isProductionOrDemoMode ? trackersConstants.prod : trackersConstants.dev;
    this.createForm();
    this.subscriptions.add(
      this.auth.userProfile$.subscribe(user => {
        this.accountId = user[auth0Constant.accountId];
        this.checkAuthCallback();
      })
    );
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      fitbit: false,
      polar: false,
      garmin: false,
      googlefit: false
    });
  }

  deviceToggled(event, device): void {
    this.formGroup.setValue({
      ...this.initialFormValue,
      [device]: event.checked
    });
    if (event.checked && device === 'fitbit') {
      this.authorizeFitbit();
    }
    if (event.checked && device === 'googlefit') {
      this.authorizeGoogle();
    }
    if (event.checked && device === 'polar') {
      this.authorizePolar();
    }
    if (!event.checked) {
      this.deleteTrackerConnection();
    }
  }

  authorizeFitbit(): void {
    const pkce = create();
    const redirectUri = encodeURIComponent(`${this.location.origin}/fitbit.html`);
    const clientId = this.trackersConstants.fitbit.clientId;
    const scope = 'activity';
    let basePathAuth = `${this.trackersConstants.fitbit.authorizeUrl}?`;
    basePathAuth += `client_id=${clientId}&`;
    basePathAuth += 'response_type=code&';
    basePathAuth += `scope=${scope}&`;
    basePathAuth += `redirect_uri=${redirectUri}&`;
    basePathAuth += `code_challenge=${pkce.codeChallenge}&`;
    basePathAuth += `code_challenge_method=S256&`;
    basePathAuth += `state=${encodeURIComponent(btoa(pkce.codeVerifier))}`;
    this.location.assign(basePathAuth);
  }

  authorizeGoogle(): void {
    const pkce = create();
    const redirectUri = encodeURIComponent(`${this.location.origin}/googlefit.html`);
    const clientId = this.trackersConstants.googlefit.clientId;
    const scope = encodeURIComponent('https://www.googleapis.com/auth/fitness.activity.read');
    let basePathAuth = `${this.trackersConstants.googlefit.authorizeUrl}?`;
    basePathAuth += `client_id=${clientId}&`;
    basePathAuth += 'access_type=offline&';
    basePathAuth += 'response_type=code&';
    basePathAuth += `scope=${scope}&`;
    basePathAuth += `redirect_uri=${redirectUri}&`;
    basePathAuth += `code_challenge=${pkce.codeChallenge}&`;
    basePathAuth += `code_challenge_method=S256&`;
    basePathAuth += `state=${encodeURIComponent(btoa(pkce.codeVerifier))}`;
    this.location.assign(basePathAuth);
  }

  authorizePolar(): void {
    const pkce = create();
    const redirectUri = encodeURIComponent(`${this.location.origin}/polar.html`);
    const clientId = this.config.isProductionOrDemoMode
      ? this.trackersConstants.polar[this.config.getMode()].clientId
      : trackersConstants.dev.polar.clientId;
    const authorizedUrl = this.config.isProductionOrDemoMode
      ? this.trackersConstants.polar[this.config.getMode()].authorizeUrl
      : trackersConstants.dev.polar.authorizeUrl;
    let basePathAuth = `${authorizedUrl}?`;
    basePathAuth += 'response_type=code&';
    basePathAuth += `client_id=${clientId}&`;
    basePathAuth += `redirect_uri=${redirectUri}&`;
    basePathAuth += `code_challenge=${pkce.codeChallenge}&`;
    basePathAuth += `code_challenge_method=S256&`;
    basePathAuth += `state=${encodeURIComponent(btoa(pkce.codeVerifier))}`;
    this.location.assign(basePathAuth);
  }

  resetErrors(): void {
    this.showFitbitError = false;
    this.showGoogleFitError = false;
  }

  resetLocalStorage(): void {
    localStorage.removeItem('auth.state');
    localStorage.removeItem('auth.tracker');
    localStorage.removeItem('auth.code');
  }

  private checkAuthCallback(): void {
    const authCode = localStorage.getItem('auth.code');
    if (authCode && authCode !== 'null') {
      this.subscriptions.add(
        this.devices
          .putTrackerConnection(this.accountId, {
            trackerType: localStorage.getItem('auth.tracker'),
            code: localStorage.getItem('auth.code'),
            code_verifier: atob(localStorage.getItem('auth.state')),
            redirectUrl: `${this.location.origin}/${localStorage.getItem('auth.tracker')}.html`
          })
          .pipe(flatMap(() => this.translate.get('DEVICES.SNACK_BAR.CONNECTED')))
          .subscribe((connectedMessage: string) => {
            this.resetLocalStorage();
            this.snackbar.openSnackBar(connectedMessage);
            this.getTrackerConnection();
          })
      );
    } else {
      if (authCode) {
        this.resetLocalStorage();
      }
      this.getTrackerConnection();
    }
  }

  private getTrackerConnection(): void {
    this.subscriptions.add(
      this.devices
        .getTrackerConnection(this.accountId)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(trackerConnection => {
          if (trackerConnection.trackerType) {
            this.formGroup.get(trackerConnection.trackerType).setValue(true);
            if (trackerConnection.hasError) {
              if (trackerConnection.trackerType === 'fitbit') {
                this.showFitbitError = true;
              } else {
                this.showGoogleFitError = true;
              }
            }
          }
        })
    );
  }

  private deleteTrackerConnection(): void {
    this.resetErrors();
    this.subscriptions.add(
      this.devices
        .deleteTrackerConnection(this.accountId)
        .pipe(flatMap(() => this.translate.get('DEVICES.SNACK_BAR.DISCONNECTED')))
        .subscribe((disconnectedMessage: string) => {
          this.snackbar.openSnackBar(disconnectedMessage);
        })
    );
  }
}

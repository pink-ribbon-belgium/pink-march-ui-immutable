import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EMPTY, of } from 'rxjs';
import { DevicesService } from '../../api/devices/devices.service';
import { appModules } from '../../app-modules.constant';
import { AuthService } from '../../auth/auth.service';
import { InfoTitleComponent } from '../../info-title/info-title.component';
import { sharedModules } from '../../shared/constant/shared-modules';
import { DevicesComponent } from './devices.component';

describe('DevicesComponent', () => {
  let component: DevicesComponent;
  let fixture: ComponentFixture<DevicesComponent>;
  let authService: AuthService;
  let devicesService: DevicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DevicesComponent, InfoTitleComponent],
      imports: [...appModules, ...sharedModules],
      providers: [{ provide: AuthService, useValue: { userProfile$: of({}) } }]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicesComponent);
    component = fixture.componentInstance;
    authService = TestBed.get<AuthService>(AuthService);
    devicesService = TestBed.get<DevicesService>(DevicesService);
    spyOn(devicesService, 'getTrackerConnection').and.returnValue(EMPTY);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set formValue on toggle', async () => {
    fixture.detectChanges();
    await fixture.whenStable();
    spyOn(component, 'authorizeFitbit');
    component.deviceToggled({ checked: true }, 'fitbit');
    expect(component.formGroup.get('fitbit').value).toBe(true);
  });
});
